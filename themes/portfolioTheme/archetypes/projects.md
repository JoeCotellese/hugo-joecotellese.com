---
title: {{ replace .Name "-" " " | title }}
date: {{ dateFormat "2006-01-02" .Date }}
draft: true
summary: Conducted business model workships, hired and coached the product team
end_date: 202107
tags: [
    "analytics",
    "coaching",
    "product-discovery",
    "product-requirements",
    "product-roadmap",
    "project-delivery",
    "strategy",
    "team-building",
    "ui-ux"
]
---