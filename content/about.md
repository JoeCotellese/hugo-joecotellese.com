---
title: Who is Joe Cotellese?
date: 2022-02-11 10:44:42
draft: false
lastmod: 2022-09-16T12:15:52.792Z
description: "Discover expert product management and strategy tips for SaaS, iOS, and Android development, led by an active industry practitioner."
---

Hi! Thanks for stopping by.

As a [product management and product strategy consultant](/services), I help companies bring their products to life by guiding them through the product development process. With a focus on SaaS, iOS, and Android product development, I'm well-versed in every stage of product lifecycle management, from product roadmaps to user stories and interaction design.

But I'm not just a consultant who teaches. As the co-founder of AppJawn, the makers of the recipe app [ClipDish](/projects/clipdish), I'm actively putting into practice the same product management strategies that I teach my clients. By leading ClipDish through the product development process, I gain valuable insights into what works and what doesn't in the real world. This experience enables me to provide my clients with practical advice and actionable solutions that produce real results.

Whether I'm working with pre-investment startups or large corporations, I'm always driven by a passion for creating innovative solutions that make a real difference in people's lives. So, if you're looking for a product management strategy consultant who not only teaches but also practices what they preach, reach out and we'll talk.
