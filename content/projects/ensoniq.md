---
title: ENSONIQ
date: Fri, 20 Apr 2018 13:28:42 +0000
draft: false
lastmod: 2022-09-06T16:54:36.272Z
summary: Software engineering at a musical instrument and sound card company
type: page
excludeFromTopNav: true
end_date: 200201
---

My formative years in the tech industry were spent at [ENSONIQ](https://en.wikipedia.org/wiki/Ensoniq), an innovative company that developed soundcards and music synthesizers. Working alongside a group of passionate engineers and musicians, I began my journey as the first Quality Assurance (QA) hire on the multimedia team. Specifically, I was responsible for testing the [Soundscape](https://en.wikipedia.org/wiki/Ensoniq_Soundscape_S-2000) family of soundcards, which involved hours of video game playing to ensure Sound Blaster compatibility.

Although I enjoyed my role, I soon realized my true passion was software engineering. I started by coding installation software using InstallSHIELD, Borland C++, and Windows DLLs. With ENSONIQ's soundcards being sold to a variety of Original Equipment Manufacturers (OEMs), including HP, Dell, and Gateway, each had unique requirements. To eliminate tedious manual processes, I created a set of deployment scripts, first using DOS Batch files, and later discovering the power of Python.

Later, I moved into driver development maintaining the Windows NT drivers for the DEC Alpha processor as well as contributing code to the AudioPCI support in the Linux kernel.

I eventually transitioned into a role as a Technical Program Manager, which, at the time, was known as a product manager. In this role, my primary focus was ensuring that we were building the right product for our key clients, such as Dell Computers and Gateway.

Overall, my time at ENSONIQ was an incredible experience that shaped the direction of my career in software engineering. It provided me with a strong foundation in quality assurance, software development, and project management as well as a work ethic that has served me well throughout my professional career.
