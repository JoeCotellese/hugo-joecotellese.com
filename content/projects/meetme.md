---
title: "Parsip/MeetGroup"
date: 2023-04-06T09:20:37-04:00
draft: false
end_date : 201308
tags: [
    "product-requirements",
    "product-discovery",
    "product-roadmap",
    "strategy"
]
---

Business Development and product management with a focus on user growth, site security and adtech.
