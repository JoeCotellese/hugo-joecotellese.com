---
title: Music Choice
date: 2023-04-10
draft: false
summary: Led the broadcast engineering team for music channels, VOD and SWRV the world's first interactive music video network.
end_date: 200906
tags: [
    "coaching",
    "project-delivery",
    "team-building",
]
---

Music Choice is a popular television music service that operates in the United States. The service offers a range of programmed music channels that are distributed through cable companies. As the director of broadcast television, I had the opportunity to lead the team responsible for putting these channels on air. My role involved overseeing a team of engineers, project managers, and quality assurance professionals to ensure the seamless delivery of content to viewers.

One of the most exciting areas of development during my tenure was the creation of an interactive broadcast video network. This project involved combining music videos with interactive graphics to create a more engaging and immersive viewing experience for audiences. As the project lead, I oversaw the development of the initial broadcast pipeline, which was a critical component of the interactive network's success.

My focus on innovation and technology was also evident in my decision to transition the broadcast team from a traditional waterfall-based project management approach to Agile Scrum. It was a disruptive change that required alot of work from the team. I'm proud of the results we achieved during my time there.
