---
title: Philanthropi
date: 2023-04-10
draft: false
summary: Interim CPO engagement. I worked on all aspects of the PDLC to develop the Philanthropi MVP.
end_date: 202110
tags:
  - analytics
  - coaching
  - product-discovery
  - product-requirements
  - product-roadmap
  - project-delivery
  - strategy
  - team-building
  - ui-ux
lastmod: 2023-04-29T15:04:28.631Z
---

Philanthropi is an all in one giving solution that enables companies, individuals and nonprofits choose where they create and sustain their philanthropic goals.

I was brought into Philanthropi as an interim head of product to help the team identify and build the MVP for their giving platform.

This work involved all aspects of the product development lifecycle. 

