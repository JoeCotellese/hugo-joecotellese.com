---
title: "ClipDish"
date: 2023-04-06T09:20:37-04:00
draft: false
summary: \"Genius\" cooking app for iOS devices that helps customers find and organize recipes.
end_date: 202312
tags: [
    "analytics",
    "strategy",
    "product-discovery",
    "product-roadmap",
    "product-requirements",
    "ui-ux",
    
]
---

I'm the co-founder and CEO of [ClipDish](https://getclipdish.com), it's a passion project for me that lets me get my hands dirty writing code plus work in all other facets of the business. 

Called a "[genius app](https://www.phillymag.com/news/2020/01/25/best-thing-clipdish-app/)" by Philly Magazine, we think it's the best way to save and organize recipes you find on the web.



