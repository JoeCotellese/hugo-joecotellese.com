---
title: MariaDB
description: ""
date: 2023-12-10T20:13:33.651Z
summary: VP of Product overseeing one of the world's most popular database systems.
draft: false
end_date: 202402
tags:
    - analytics
    - strategy
    - product-discovery
    - product-roadmap
    - product-requirements
lastmod: 2023-12-10T20:39:46.309Z
---

MariaDB is an open-source relational database management system (DBMS) that is a fork of MySQL, one of the world's most popular database systems. It was developed by the original developers of MySQL after concerns over its acquisition by Oracle Corporation. MariaDB is known for its high performance, robustness, and ease of use. It is used by a wide array of organizations for various applications, ranging from websites and big data applications to enterprise and cloud applications.

As the Vice President of Product Management for MariaDB, my role is crucial in steering the development and management of MariaDB's product offerings. This involves overseeing the product team as well as the entire product lifecycle, from conceptualization and development to launch and ongoing enhancement.
