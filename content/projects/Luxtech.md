---
title: "Luxtech"
date: 2023-04-06T09:20:37-04:00
draft: false
summary: Product strategy and roadmap workshops, product management coaching.
end_date: 201802
tags: [
    "strategy",
    "coaching"
]
---

Luxtech is a digital lighting company. I was brought in to conduct lean product workshops. Additionally, I coached new product managers in basic product management principals, and frameworks. 

