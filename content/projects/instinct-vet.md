---
title: "Instinct Vet"
date: 2023-04-06T09:20:37-04:00
draft: false
end_date : 202210
summary: Embedded product manager helped develop requirements and wireframes for outsourced product features.
tags: [
    product-discovery,
    product-requirements
]
---

Instinct Vet develops a veterinary management system used at veterinary clinics and animal hospitals.

In this engagement, I acted as an embedded product manager helping the team develop requirements and designs for the financial transaction portions of their product. 

