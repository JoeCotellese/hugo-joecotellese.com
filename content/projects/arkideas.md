---
title: "Ark Ideas"
date: 2023-04-06T09:20:37-04:00
draft: false
summary: Conducted business model workships, hired and coached the product team
end_date: 202107
tags: [
    "coaching",
    "team-building",
    "strategy",
]
---

Ark Ideas is a marketing agency in the pre-paid mobile space. 

In this engagement, I worked with the executive team to recruit and coach their product manager. 

Additionally, I conducted product strategy workshops to help the team with business models and product market fit.