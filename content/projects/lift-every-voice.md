---
title: "Lift Every Voice"
date: 2023-04-06T09:20:37-04:00
draft: false
summary: Tool deployed at UPenn Hospital to document experiences of racism in health care settings.
end_date: 202212
tags: ["product-discovery", "product-requirements", "ui-ux"]
---

In this engagement, I acted as the lead product manager for the UPenn Hospital Lift Every Voice initiative. 

Lift Every Voice is an anonymous, easy-to-use digital platform for documenting employees’ experiences or observations of racism in the workplace.

In my role, I worked on developing the initial MVP for Lift Every Voice, I had my hands in design, product discovery, requirements and product launch.