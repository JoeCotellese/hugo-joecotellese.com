---
title: Wodify
date: 2023-04-06T09:20:37-04:00
draft: false
summary: Technical due-diligence of remote engineering team, product management coaching.
end_date: 201904
tags:
    - strategy
    - product-roadmap
    - coaching
lastmod: 2024-10-08T21:20:37.274Z
---

Wodify is a software company that develops applications for the fitness industry.

I was brought into Wodify as part of a team conducting technical due diligence of their overseas engineering team.

Additionally, I was tasked with identifying the competitive landscape for potential new verticals that the company was interested in exploring.