---
title: "BoltOn Technologies"
date: 2023-04-06T09:20:37-04:00
draft: false
summary: Brought in to help coach the product and dev team and act as interim Chief Product Officer during company acquisition
end_date: 202201
tags: [
    "coaching",
    "product-requirements",
    "project-delivery"
]
---

Bolt On Technology is a market leader in vehicle service software used by over 8,000 service centers.

I was brought into Bolt On to coach project managers transitioning into product management.

While there, I was asked to step into the role as interim Chief Product Officer as the company moved through an acquisition.