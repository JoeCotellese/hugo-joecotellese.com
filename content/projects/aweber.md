---
title: AWeber
date: Wed, 13 Jan 2016 15:33:00 +0000
draft: false
lastmod: 2022-09-06T17:02:40.280Z
summary: Head of product at email service provider AWeber
type: page
excludeFromTopNav: true
end_date: 201705
tags: [
    "analytics",
    "coaching",
    "product-discovery",
    "product-requirements",
    "product-roadmap",
    "project-delivery",
    "strategy",
    "team-building",
]
---

AWeber is one of the earliest email service providers. Initially founded in 1998 Aweber helps thousands of small businesses communicate with their customers.

When Aweber began their key differentiator was an automated drip campaign. This is a time based series that delivers regular email messages to an audience. As more competing products came into the marketplace and as our customers became more sophisticated with their email campaigns we needed to update the product.

At AWeber, I was the head of product. I lead a team of product managers and UX designers. One of our projects was Campaigns - an email marketing automation platform that was event driven and allows for more sophisticated campaigns.

{{< youtube uQJfmH8oaqA >}}
