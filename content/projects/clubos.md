---
title: "ClubOS"
date: 2023-04-06T09:20:37-04:00
draft: false
summary: I was brought into ClubOS to help them hire their first product manager and head of engineering.
end_date : 201901
tags: [
    "team-building",
    "coaching"
]
---

Club OS combines prospect management, follow-up calling and texts, email campaigns, scheduling, billing, trainer to client collaboration, workout tracking and food journaling in a single platform that is custom branded for the health club.

I was brought into ClubOS to help them hire their first product manager and head of engineering. I developed a hiring plan, conducted the initial screening interviews and facilitated panel interviews with the candidates and team.

Post hire, I worked with the hirees coaching them through their first 90 days.

