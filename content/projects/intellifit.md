---
title: Intellifit
date: 2023-04-10
draft: false
summary: VP of Engineering leading the team building the world's first 3D bodyscanner for the consumer apparel industry.
end_date: 200810
tags: [
    "analytics",
    "product-discovery",
    "product-requirements",
    "product-roadmap",
    "project-delivery",
    "strategy",
    "team-building",
]
---

Intellifit was a 3D body scanning technology that used low power radio waves to construct a 3D model of a person. That model was used to extract body measurements. The body measurements were used to help customers find great fitting clothing. While at Intellifit I was the VP of Engineering, responsible for all of the software for the website, kiosk and 3D scanner. Check out some photos and videos below to see it in action. One of our business models was to create mall kiosks throughout the country and partner with stores to drive better qualified shoppers who had a list of products in their sizes.

{{< youtube "G1IceHjADbQ" >}}

We worked with Levis in the US and Japan to help their customers find better fitting jeans.

{{< figure src="/img/13216320100910101436.jpg" caption="I spent some time in Japan helping Levis deploy the Intellifit Scanner">}}

We also designed a concept store that was like "Build-a-Bear" for women's jeans. You could come in, get measured and then design your own custom fitting jeans.

![Intellifit Body Scanner](/img/intellifit.jpg)