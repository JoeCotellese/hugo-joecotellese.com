---
title: Always Be Testing
slug: always-be-testing
description: Make sure that you use checklists when you are testing anything that is sent to a user.
summary: |
  It's easy to gloss over testing. When you ship a feature, you're often
  pressed for time. Don't skip testing. It can damage your brand.
author: Joe Cotellese
date: 2022-02-25
draft: false
tags: []
categories: []
lastmod: 2024-01-04T12:44:52.259Z
images:
  - AlwaysBeTesting.jpg
---

In the fast-paced world of software development, QA (Quality Assurance) testing often becomes an afterthought, especially when deadlines loom and the rush to release takes over.

I started my career in QA and most Friday afternoons you found me running around frantically with a floppy disk looking for systems to use for "QA."  

Back then, in my eagerness to get to Happy Hour, I sometimes skimmed over crucial testing steps. This lack of thoroughness led to several problematic releases that I would then have to deal with on Monday.

Such experiences drove home an important point: never underestimate the power of detailed, systematic testing.

{{<imgresize "images/AlwaysBeTesting.jpg" >}}

### The Undeniable Importance of a QA Testing Checklist

The cornerstone of effective QA testing is a robust checklist. This checklist serves as your roadmap, ensuring that every critical aspect of the software is examined and validated. It's a tool that fosters consistency and completeness in your testing approach. When faced with new testing scenarios or unexpected bugs, update your checklist. This iterative process not only improves the current testing cycle but also sets a higher standard for future releases.

### The Move Towards QA Automation

I once told a hiring manager that I wanted to automate anything that I'd have to do more than once. There are tons of tools that can help automate the QA process.

Automated testing tools can perform repetitive, detailed tasks with precision, far beyond the capabilities of manual testing. They save time, reduce human error, and can run tirelessly, providing broader coverage. If you can automate part of your testing process, do it.

This doesn't mean replacing human testers, but rather augmenting their capabilities and allowing them to focus on more complex and creative testing strategies.

### A Case Study: The Devil is in the Details

Let me share a recent incident that highlights the nuances of QA testing. I came across a problem on a photo-sharing website, where the email notification system failed in two subtle but significant ways. First, the subject line of the email lacked personalization, displaying a blank space where my name should have been. Second, the email's images were blocked due to the new privacy settings in macOS Mail, which I had enabled.

{{<imgresize "images/email-that-wasnt-reviewed.png">}}

These issues might seem minor, but they significantly affected my perception of the brand. The missing name in the subject line felt impersonal and indicated a lack of attention to detail. The image loading issue, while a consequence of external privacy settings, suggested a lack of foresight in email design and testing.

### The Bottom Line: Always Be Testing

This example underscores a critical axiom in the world of software development: Always be testing. From the smallest UI element to the most complex backend process, every component of your software must undergo rigorous testing. It's not just about finding bugs but also about ensuring a seamless, engaging user experience. Your software is a reflection of your brand, and every interaction a user has with it shapes their perception.

In conclusion, QA testing is not a step to be rushed or overlooked. It's an integral part of the software development lifecycle that demands diligence, precision, and a forward-thinking approach. By embracing thorough checklists and leveraging automation where possible, you can elevate the quality of your software and, by extension, the reputation of your brand. Remember, in the digital world, every detail counts, and every test matters.