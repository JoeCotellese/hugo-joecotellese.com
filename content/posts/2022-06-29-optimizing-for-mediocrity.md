---
title: Optimizing for Mediocrity
description: | 
    When you lose your best team members because of poor management you’re optimizing for mediocrity. 

    Want to optimize for high performance?
date: 2022-06-29T18:54:03.158Z
preview: ""
draft: false
tags: ""
categories: ""
lastmod: 2022-06-29T19:02:04.938Z
---

When you lose your best team members because of poor management you’re optimizing for mediocrity.

Want to optimize for high performance? 

Invest in management training. Especially for people who are moving from the role of individual contributor to people management.

This is an investment that will level up your team and keep your best people from leaving.

If you are a new manager or a manager of new managers and want to get started check out the book [The Three Signs of a Miserable Job: A Fable for Managers (and their employees)](https://amzn.to/3HYS6lf) by Patrick Lencioni. 

The author does a great job of teaching management principals through a fable about a former CEO who goes to work at a pizza shop. 