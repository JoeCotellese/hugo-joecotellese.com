---
title: Product Management Butterfly Effect
description: Product management decisions can have unintended consequences.
date: 2023-12-10T15:49:36.648Z
preview: ""
draft: false
tags: []
categories: []
lastmod: 2023-12-10T15:57:26.052Z
---
Elon Musk let Alex Jones back on Twitter. That guy is a shit and I'm so glad I'm off the platform.

What finally made me give up Twitter? A bad product management decision.

![An animated image of a man saying "and those are your consequences."](consequences.gif)

It took me a long time to ween myself off Twitter. I deleted my account but kept going back to my favorite tweeters. You could say I was addicted.

A few months ago Elon changed Twitter to only permit logged in users from viewing tweets. I suspect the goal was to stop the hemorrhaging of people leaving the platform.

The unintended consequence? People like me who were still addicted to scrolling Twitter (and generating ad impressions) finally left for good. 

Today, Twitter's engagement and ad revenue are both down below pre-Elon levels.

The next time you're thinking about a product decision, game out as many scenarios as possible. You may not identify every unintended consequence but you might catch the big ones.
