---
title: Automating your Mac and streamline product management
description: Save hours by building automation into your product management workflow.
date: 2023-06-02T14:17:53.864Z
preview: ""
draft: ""
categories: []
slug: apple-automation
lastmod: 2024-03-10T17:45:07.729Z
---

"I'm lazy."

That's how I responded to someone who interviewed me and asked "What's one quality that makes you good for this job?"

After he picked up his jaw from the desk, I went on to explain &mdash; "I hate doing anything multiple times and always look for ways to automate repetitive tasks."

My laziness stems from my aversion to repetitive tasks. I despise doing things multiple times when there could be a more efficient way. This mindset has led me on a quest to automate the mundane aspects of my life, freeing up time and mental energy for more important responsibilities.

In this pursuit of automation, I stumbled upon the incredible Automators podcast hosted by Rosemary Orchard and David Sparks. Their in-depth exploration of apps and processes for automating life using Apple products has been a game-changer for me. The podcast offers a wealth of valuable tools and insights that have transformed the way I work.

Inspired by their discussions, I have curated a comprehensive list of these automation tools mentioned in the Automators podcast. You can find the work-in-progress collection [here](https://gitlab.com/JoeCotellese/awesome-automators).

Among these automation tools, there are a few stand-outs that have become indispensable in my daily life as a product manager. They have significantly enhanced my efficiency and productivity, allowing me to focus on high-value tasks. These tools have become my trusted companions, saving me time and effort.

So, if you're a busy product manager like me, seeking ways to optimize your workflow and maximize your productivity, I encourage you to explore the tools I've highlighted. Embrace automation and let technology do the heavy lifting, freeing you to excel in your role and achieve your goals. With these automation tools by your side, you'll reclaim valuable time, streamline your processes, and make significant strides in your professional journey.

## Text expansion - Stop typing all the things...

[Typinator](https://ergonis.com/typinator), developed by Ergonis Software, is a fantastic productivity application that serves as an excellent entry point for individuals venturing into the world of automation. Specifically, Typinator excels in text expansion, allowing users to significantly enhance their typing efficiency and reduce repetitive tasks.

For those who are just starting to explore automation, text expansion tools like Typinator are an ideal first step. Here are some ways that I use it as a product manager.

1. Templatized user stories
2. Templatize customer interview request emails
3. Templatize product requirement documents so that all documents follow a standard layout.
4. Templatize the body of bug tickets so I ensure I remember to capture everything.
5. Templatize product launch checklists so you never forget a critical step
6. Templatize filenames so you can quickly find and sort files on your hard disk (see Hazel)

By embracing text expansion tools, newcomers to automation can experience immediate benefits in terms of time savings and increased productivity. Typinator enables users to quickly and accurately insert frequently used text, regardless of the application or context. It serves as a foundation for automating routine typing tasks and provides a glimpse into the potential of automation tools in streamlining workflows.

With Typinator as their starting point, users can gain confidence and familiarity with automation concepts. As they become comfortable with text expansion and witness the positive impact it has on their productivity, they can then explore more advanced automation tools and workflows tailored to their specific needs.

## Hazel for automaticaly organizing my Mac folders.

What does your desktop or downloads folder look like? If it's anything like mine at any given moment you might have random screenshots, scanned receipts, market research documents, or design files. 

Trying to organize that stuff manually just never happens. Automation to the rescue! I use the app Hazel.

[Hazel](https://www.noodlesoft.com) has become an essential part of my workflow, helping me save time, reduce clutter, and keep my files organized and easily accessible. Its ability to automate file organization based on rules and conditions has revolutionized the way I manage my hard drive, particularly when it comes to efficiently filing bills and receipts into the appropriate folders within Dropbox.

## Alfred for quickly navigating my Mac without the mouse

[Alfred](https://www.alfredapp.com) is a powerful productivity application for macOS that acts as a versatile launcher and automation tool. With Alfred, you can quickly launch applications, search your files and folders, control system settings, and perform various tasks using custom shortcuts and workflows.

At its core, Alfred replaces the traditional macOS Spotlight search with a more feature-rich and customizable alternative. You can invoke Alfred with a hotkey and start typing to search for applications, files, or even perform web searches. Alfred provides quick suggestions and learns from your usage patterns, making it faster to access frequently used items.

Beyond search, Alfred allows you to create custom workflows using its built-in editor, enabling you to automate repetitive tasks. These workflows can be as simple as opening multiple applications with a single command or as complex as executing a series of actions involving file manipulation, system commands, or online integrations. Alfred also integrates with various web services, allowing you to control your email, calendar, and other applications right from the launcher.

Overall, Alfred empowers macOS users to streamline their workflow, save time, and boost productivity by providing a highly customizable and efficient way to access applications, files, and perform automated tasks.

## Hammerspoon for keeping my windows neat and tidy

[Hammerspoon](https://www.hammerspoon.org), with its Lua scripting language, allows me to take precise control over my open windows. I can effortlessly move, resize, and reposition them across multiple monitors without relying on the mouse. This capability not only enables seamless multitasking but also saves me valuable time by eliminating the need for manual window management. With Hammerspoon, I have full control at my fingertips, making my work environment more efficient and visually organized.

Another powerful feature of Hammerspoon is its ability to automate various activities based on changes in my Mac environment. One specific use case that has significantly improved my workflow is automating actions when my camera turns on. Using Hammerspoon, I have created a script that automatically enables the "Do Not Disturb" mode on my Mac whenever my camera is in use. This ensures that I am not interrupted by notifications during important video calls or recordings, allowing me to stay focused and undisturbed.

Best of all, this amazing utility is *free.*
## Conclusion

Automating your Mac is a game-changer in terms of productivity, efficiency, and reducing repetitive tasks. By harnessing the power of automation tools like Hammerspoon, Alfred, Hazel, and Typinator, you can streamline your workflow, save time, and take control of your digital environment.

Whether you are a technically minded individual or just dipping your toes into automation, these tools offer a range of options to suit your needs. They provide a glimpse into the power of automation and inspire you to explore further possibilities in streamlining your workflow.

By embracing automation, you can reclaim valuable time, reduce errors, and create a more efficient and organized digital ecosystem. Start small with text expansion, gradually build your automation toolkit, and witness the transformative impact it can have on your Mac experience.

So, why wait? Unlock the potential of automation and embark on a journey to optimize your productivity, elevate your efficiency, and make the most out of your Mac. Embrace automation, and let your Mac work smarter, not harder.