---
title: How to Build Better Products by Asking Yourself One Question
blogTitle: "Beyond Features: Understanding the Emotional Heart of What You're Selling"
description: Discover how to shift from product-focused to customer-emotional needs in product management, enhancing value by understanding what you're really selling.
date: 2021-11-16 08:14:56
LastModified: 2021-11-16 08:11:48
draft: false
lastmod: 2024-03-10T23:37:24.325Z
---

As Product Managers we’re constantly running around in 50 million different directions. It’s easy to lose focus under the barrage of feature requests coming at your from sales, marketing, customer support, customer interviews and internal stakeholders. You end up spending too much time looking at your product as “that thing that you’re selling” when you should be really asking yourself “What am I really selling?”

There is a classic analogy used in sales to [sell the sizzle, not the steak](https://www.newyorker.com/magazine/1938/04/16/the-sizzle). The point is that a successful sales person knows how to tie the emotional needs of their customers to the product. This should be just as true for product development.

In a former life, I worked for an email service provider. To the casual observer, it might look like the company sells an email platform. While that's true, it misses the emotional connection. When our customers came to us, it’s with the dream of starting their own business. They’ve read books like the [_4-Hour Workweek_](https://fourhourworkweek.com/) and imagine escaping their cubicle-centered existences. We’re not just selling an email platform. We’re selling a slice of the American Dream.

Once you tap into the emotional needs of your customer you can better understand what motivates them. Knowing this, you can frame your product development efforts around these needs.

Look at Nike, they aren’t just selling sneakers and athletic wear. They’re selling athletic glory. Nike customers could buy sneakers from anyone but instead want to buy Nike products because they want to be like their favorite athletes.

Things get exciting when you understand what you are selling because the answer can set a direction for your business to expand in new ways. How? Because it opens up opportunities to expand your relationships with your existing customers. That is, you can sell them more stuff.

**Why is this important?**

There are two drastically different approaches to building a business, product focused and market focused. If you are product focused, you grow your business one market at a time. That is, you focus on dominating one vertical and then repeat the process in other verticals. Coca-cola is a great example of this. Market focused businesses on the other hand focus on understanding the needs of a vertical and building other products that service that same need. Think about the Nike example I presented earlier.

The second approach has the advantage of being a less expensive way to grow your business. It’s cheaper to keep an existing customer then it is to acquire a new customer. Knowing the emotional drivers of your customer strengthens the relationship with them and makes it harder for them to pull away – to put it bluntly, you can get more claws into them. The more they have invested in you, the harder it is to leave.

Think of Apple – They’ve built an entire ecosystem of interrelated products and services. If your digital life is tied up in Apple’s ecosystem I don’t care how great the next Samsung smart phone is, switching is nearly impossible.

Start asking yourself the question “What am I really selling?” and tap into the emotional driver of your customer. Understanding it will begin to give your customers a unique personality. More on that in a later post.
