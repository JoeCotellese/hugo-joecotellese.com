---
title: "The Zen of Collaboration: 5 Strategies to Avoid Design by Committee"
description: Explore key strategies to avoid 'design by committee' in product development.
slug: avoiding-design-by-committee
date: 2023-12-20T14:02:04.385Z
preview: ""
draft: false
tags: []
categories: []
images:
    - kraftsamla-ikea-featured.png
lastmod: 2023-12-21T21:23:20.992Z
---


Recently, my wife and I visited the Ikea cafeteria for a quick bite. As I scarfed down their iconic meatballs, my attention was drawn to an intriguing sign on the wall.

{{< imgproc kraftsamla-ikea.png Resize "800x" >}}

The sign introduced the Swedish concept of 'kraftsamla,' an inspiring notion of collective effort towards a shared objective. This idea resonates deeply with me, especially in my role as a product manager. Often, we find ourselves navigating the challenging waters of 'design by committee.' This phrase typically evokes visions of endless meetings and watered-down concepts – a far cry from the streamlined synergy of kraftsamla.

But what if we could change that? What if there are essential elements that can morph the often cumbersome committee-driven approach into a dynamic and effective collaborative process?

In this article, I delve into five critical factors that can revolutionize product development, transforming it from a sluggish, committee-dominated ordeal to a swift, cooperative, and productive journey.

## What is Design by Committee?

### Definition and Explanation

Design by committee is a term often used pejoratively to describe a project or design process overly influenced by an extensive group, rather than driven by individual or specialized vision.

Hopefully you’ve never experienced this but too many product management people I’ve spoken to experience design by committee at some point in their career.

If you are constantly trying to build consensus among multiple stakeholders, each with their own competing agendas congratulations, you’re in design by committee hell. This hell-scape often results in prolonged decision-making, a tendency towards safer, more generic solutions, and a final product that lacks innovation or a distinct vision.

That doesn’t mean you shouldn’t allow group decision-making. Design by committee specifically refers to scenarios where the absence of a clear leadership or decision-making hierarchy leads to inefficient and often ineffective outcomes. Unlike productive teamwork, design by committee often struggles with aligning diverse viewpoints under a unified vision, resulting in a compromised end result.

### Impact on Product Development

In the realm of product development, design by committee can have several negative consequences. The process can become bogged down by endless discussions and revisions, as every member of the committee tries to have their input reflected in the final product. This can lead to significant delays, increased costs, and often, a product that fails to meet the original vision or market needs.

Spend anytime using your banking software and you are *likely* seeing design by committee in action. The app loses its user-centric focus due to conflicting inputs from various departments, resulting in a cluttered user interface or diluted functionality.

### The Difference Between Collaboration and Committee

It's crucial to distinguish between effective, healthy team collaboration and design by committee. Collaborative product development, when done correctly, leverages the strengths and expertise of each team member while still maintaining a clear direction and leadership.

In a well-functioning collaborative environment, diverse ideas are discussed and integrated into the product development process, but these contributions are always aligned with a shared vision and strategic objectives. This approach can lead to innovative solutions and products that are well-received in the market, as they combine the best of various perspectives under a cohesive strategy. 

For example, many successful tech companies use agile development methodologies that encourage team input but still rely on product managers or lead designers to make final decisions, ensuring that the product remains focused and aligned with its intended purpose. 

This balance between collective input and directed leadership is the key differentiator between a collaborative success and a design-by-committee failure.

So, how do you avoid it in your organization or team?

## Actionable Tactics for Avoiding Design by Committee

### Strategy 1: Establish Clear Leadership and Roles

One of the most effective ways to circumvent the pitfalls of design by committee is to establish clear leadership and well-defined roles within the team. This approach involves nominating a decision-maker or a small group of leaders who have the final say in the project's direction. The role of this leader or leadership team is not to stifle collaboration, but to ensure that the project remains focused and true to its vision.

For instance, in the development of the original iPhone, [Steve Jobs played a crucial role as a decisive leader](https://www.smithsonianmag.com/arts-culture/how-steve-jobs-love-of-simplicity-fueled-a-design-revolution-23868877/). His clear vision and ability to make final decisions were instrumental in creating a product that was both innovative and commercially successful. Under his leadership, team members had defined roles and were encouraged to contribute, but it was Jobs who guided the overarching direction and made critical decisions.

Having a clear leader or leadership team helps in preventing endless debates and indecisiveness that can plague committee-led projects. This doesn't mean disregarding the team's input but filtering and aligning these inputs with the project's goals. The leader's role involves synthesizing these contributions into a cohesive plan and making decisions that may sometimes be unpopular but are necessary for the project's success.

Moreover, defined roles within the team contribute to more effective dynamics. Each member understands their responsibilities and how their work contributes to the larger goal. This clarity helps in reducing overlaps, conflicting inputs, and ensures that all aspects of the project are covered by experts in those areas. For example, in software development, clear roles such as UI/UX designers, front-end developers, back-end developers, and product managers ensure that each aspect of the product is handled by someone with the right skills and experience.

### Strategy 2: Set Defined Goals and Objectives

The establishment of clear, measurable goals and objectives is a cornerstone strategy in steering clear of the 'design by committee' trap. Well-defined goals serve as a guiding light for the project, providing a benchmark against which all decisions and contributions can be measured. This clarity is essential in maintaining focus and ensuring that the team's efforts are aligned with the desired outcome of the project.

For instance, Amazon's development of its Kindle e-reader was driven by [clearly defined goals](https://www.aboutamazon.com/news/devices/a-look-back-at-10-years-of-the-amazon-kindle), primarily focusing on creating a device that provided an exceptional reading experience. This clear objective guided every decision in the development process, from the e-ink technology used for the screen to the device's form factor and battery life. As a result, the Kindle emerged as a product that was well-aligned with its intended purpose and resonated with its target audience.

To effectively set and communicate goals and objectives, it's important to involve key stakeholders in the goal-setting process. This inclusion helps ensure that the goals are realistic, achievable, and broadly supported within the team. Methods like SMART (Specific, Measurable, Achievable, Relevant, Time-bound) criteria can be employed to define these goals. Once established, these goals should be communicated clearly to every team member, ensuring that everyone understands what the project aims to achieve and how their work contributes to these objectives.

Regular check-ins and updates on the project's progress towards these goals can also be beneficial. Tools like dashboards or progress tracking software can provide visual representations of how the project is advancing, keeping the team focused and motivated.

### Strategy 3: Encourage Constructive Feedback and Open Communication

The role of open communication in preventing 'design by committee' cannot be overstated. It is vital to create a project environment where feedback is not only encouraged but is also constructive and valued. This approach helps in harnessing the collective expertise of the team while avoiding the pitfalls of unproductive and directionless discussions.

Open communication fosters an environment where team members feel comfortable sharing their ideas and concerns. When team members are assured that their input is valued, they are more likely to contribute meaningful and constructive feedback. Google's famous 'Project Aristotle,' which studied effective teams, found that [psychological safety](https://www.nytimes.com/2016/02/28/magazine/what-google-learned-from-its-quest-to-build-the-perfect-team.html), a belief that one won't be punished for speaking up, was the most important factor in successful teams. This finding underlines the importance of creating an atmosphere where open and honest communication is encouraged.

To cultivate such an environment, it's essential to establish some ground rules for feedback. These might include focusing on the issue rather than the person, avoiding blame, and being specific and actionable in criticism. Additionally, it can be helpful to designate specific times or forums for feedback, such as regular team meetings or dedicated brainstorming sessions, ensuring that the project maintains its momentum.

Moreover, leaders and managers should lead by example, actively seeking feedback on their decisions and actions and responding to it positively. This behavior sets the tone for the entire team and demonstrates that feedback is a valuable part of the process, not just a formality.

In essence, encouraging constructive feedback and open communication is a key strategy in avoiding 'design by committee.' It ensures that while everyone's input is heard, the feedback is structured in a way that is conducive to the project's progress and aligned with its overarching goals and objectives.

### Strategy 4: Limit the Size of the Decision-Making Group

An effective way to avoid the perils of 'design by committee' is to keep the core decision-making team lean. This strategy ensures that while the team benefits from diverse inputs and collaboration, the decision-making process remains agile and focused.

#### Benefits of a Lean Decision-Making Team

- **Enhanced Agility:** A smaller group can make decisions faster, adapting to changes or challenges more swiftly. This agility is crucial in dynamic environments where quick decision-making can be a competitive advantage.
- **Clearer Communication:** With fewer people involved, communication tends to be more direct and less prone to misunderstandings. This clarity is essential for effective collaboration and decision-making.
- **Increased Accountability:** A lean team facilitates clear accountability. When fewer people are making decisions, it's easier to track who is responsible for what, leading to better ownership and follow-through on tasks.
- **Stronger Cohesion:** Smaller groups often develop a stronger sense of team cohesion and understanding, which can enhance the quality of the decision-making process.

#### Involving Larger Teams without Compromising Decision-Making

While it's beneficial to keep the decision-making team small, it's also important to involve larger teams in the process without compromising efficiency. This can be achieved through:

- **Structured Input Mechanisms:** Establish channels through which team members outside the core group can provide input. Create a dedicated Slack channel for product input or use tools like UserVoice to collect product input.
- **Rotational Involvement:** Consider rotating different team members into the decision-making process for different phases or aspects of the project. This approach ensures fresh perspectives while maintaining a manageable group size.
- **Sub-teams for Specific Tasks:** Create sub-teams responsible for different aspects of the project. These teams can work independently on their assigned tasks and funnel their findings or suggestions to the core decision-making team.
- **Regular Updates and Feedback Sessions:** Keep the larger team informed and involved through regular updates and feedback sessions. This ensures that everyone feels included and has a chance to contribute their ideas and feedback, even if they are not part of the core decision group.

By limiting the size of the decision-making group and employing strategies to involve the larger team effectively, organizations can enjoy the benefits of collaboration without falling into the trap of 'design by committee.' This balanced approach leads to more streamlined, effective decision-making processes and ultimately, to more successful project outcomes.


### Strategy 5: Implement a Structured Decision-Making Process

Implementing a structured decision-making process is crucial in avoiding the inefficiencies of 'design by committee.' One great way to do this is to utilize product prioritization frameworks which will help the team understand that decisions don’t happen at the whim of the product manager.

It's also helpful as a way to avoid the [highest-paid person's opinion problem](/posts/protecting-product-strategy-from-executives).

**What are the most common product management frameworks?**

- **RICE Scoring:** This method evaluates features and projects based on four factors: Reach, Impact, Confidence, and Effort. By scoring each aspect, teams can objectively assess the value and feasibility of different initiatives.
- **Kano Model:** This model classifies product features into different categories like basic needs, performance needs, and delighters, helping product managers understand customer preferences and prioritize features accordingly.
- **MoSCoW Method:** Standing for Must-haves, Should-haves, Could-haves, and Won’t-haves, this technique helps teams distinguish between essential and nice-to-have features, enabling more focused development efforts.

If you want to dive deeper into how to implement a product management framework start with the post ["Mastering the Art of Prioritization: Tools for Product Managers."](/posts/product-management-prioritization-guide/)

By implementing structured decision-making processes and utilizing prioritization frameworks, product teams can navigate the complexities of product development more effectively, making decisions that are not only timely but also strategically sound.


## Conclusion

Navigating the complexities of product development requires a balanced approach that steers clear of the 'design by committee' pitfalls. By establishing clear leadership, setting well-defined goals, encouraging constructive feedback, limiting decision-making groups, and implementing structured processes, teams can achieve streamlined and effective collaboration. These strategies are essential in transforming the traditional hurdles of product development into opportunities for innovation and success.

Want to get more insights into how you can build better products? Consider signing up for my newsletter below. Don’t miss out on the opportunity to elevate your product management journey.
