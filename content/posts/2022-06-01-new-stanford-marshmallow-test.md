---
title: New Stanford Marshmallow Test
description: ""
date: 2022-06-01T11:53:21.717Z
preview: ""
draft: ""
tags: ""
categories: ""
lastmod: 2022-06-29T18:53:05.093Z
---

Good scientists (and product managers) constantly test assumptions and change their minds based on new data. 

I read a lot about Behavioral Psychology / Economics which is a fascinating subject area if you are a product manager.

One of the studies I will often find in books is the Marshmallow Test. If you've never heard of it the concept is easy to summarize. 

<!--more-->

Put kids in front of a marshmallow. Tell them they can have one now or wait fifteen minutes and have two.

The study's conclusion was that kids that wait for the marshmallow rather than succumb to instant gratification are more successful later in life.

In 2018 a team of psychologists published a study recreating the experiment. The findings indicate that taking into account a child's background, home environment and cognitive ability at age four, the test shows no significant correlation between waiting for the marshmallow and success later in life.

I've read about this original study in multiple books and have taken it as gospel. This article on Big Think goes deeper into the new study and is a fascinating read.

https://bigthink.com/neuropsych/new-marshmallow-experiment/
