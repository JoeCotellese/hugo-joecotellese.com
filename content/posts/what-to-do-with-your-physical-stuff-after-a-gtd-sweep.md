---
title: What to do with your physical stuff after a GTD sweep?
date: Sun, 18 Nov 2018 16:38:36 +0000
draft: false
contenttypes:
  - BlogPosting
lastmod: 2024-02-02T12:33:13.767Z
---

I've recently jumped back into [Getting Things Done](/posts/getting-things-done-book-summary/) after a year or so lapse. Yesterday I "corralled my stuff" by going through my office and clearing out every self, nook and cranny.

Following David Allen's method, I processed all of that stuff by tossing it, filing it or making a note of it as a Next Action in my system. It was an incredibly freeing process. I feel like at least as far as my office goes, I have a pretty good handle on all of my open loops. I was using a laundry basket as a giant container for the physical stuff that I needed to do something with.

<!--more-->

At the end of the process, I ended up with this. ![laundry basket full of stuff](/img/laundrybasketGTDstuff.jpg)

It seems that I traded one inbox for another. I thought about how to best handle it. Everything that could be addressed in less than two minutes was already taken care of.

What's left here are the things that have associated Next Actions in my system that I don't want to put back on the shelves. I'm eventually going to need the laundry basket again so I need to do something with it.

Here's how I'm handling it.

I'm going to put them in my basement or attic so they are out of sight. However, I'm going to indicate in the Next Action associated with each item the location of that item so I can recall it later when I'm ready to process it.

It solves my problem by allowing me to avoid putting the things back on their shelves.
