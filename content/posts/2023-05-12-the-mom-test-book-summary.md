---
title: The Mom Test Book Summary
description: |
  Read my notes and book summary from the excellent product
  management book The Mom Test.

date: 2023-05-12T13:50:22.958Z
preview: ""
draft: false
tags:
  - books
categories: []
lastmod: 2023-05-22T12:40:37.854Z
slug: the-mom-test-book-summary
---

As product manager, or entrepreneurs, getting feedback from people is important to do early and often. Early conversatoins about problems help ensure that you are focused on the right solutions.

But interviewing is hard. Especially if you are shy or introverted. 

I found this great book called [The Mom Test](https://amzn.to/3IzLScU) that breaks down a framework for interviewing people. My notes and outline are below.

## Chapter 1 - The Mom Test
Picture this: You've just stumbled upon the most mind-blowing idea for a business or product. Now, your first instinct might be to rush to people and eagerly ask them what they think about it. But hold your horses, my friend!

If you approach someone, say your mom, and go, "Hey, what's your take on my fabulous product?" She'll probably respond with a sweet, "That's nice, dear." And guess what? You'll foolishly interpret it as a green light to charge ahead. Well, let me burst your bubble right here. You're heading for failure because you're basing everything on misguided assumptions.

Remember, your conversations with people become goldmines of insights if you ask about their problems without ever spilling the beans about your idea. Keep it on the DL.

Now, let's dive into the three points of the fabulous Mom Test framework:

1. Talk about their life, not your idea.
2. Ask about specifics that happened in the past.
3. Talk less, listen more.

## Chapter 2 - Avoiding Bad Data

When chatting up potential customers, you must be wary of gathering the right type of data. Bad data is a recipe for disaster, leading to false positives or false negatives. Yikes!

Keep an ear out for lousy data disguised as compliments, fluff, or random ideas from your customers. One way to dodge those compliments is by not blabbering about your idea in the first place. It's like avoiding a trap!

If you do hear compliments, take them with a grain of salt. They're often misleading and unreliable. People tend to be overly nice and tell you how cool your idea is. Well, guess what? That's not helpful at all! You need them to spill the beans about their own experiences in precise detail.

So, when you come back from customer conversations all jazzed up, exclaiming, "Customers love it!" or "It went amazingly well!"—slow down, my friend. Make sure you have actionable insights, not just empty compliments.

Another way to muddle your data is by asking vague questions that elicit fluffy answers. We don't want that!

Avoid starting questions with "Would you ever...?" or "Do you ever...?" They lead to wishy-washy responses. Instead, reel them back in and ask for concrete examples.

Them: "I always do..."
You: "Tell me about a specific time that you did..."

Now, there might be situations where your test subject "gets" your idea and starts bombarding you with feature requests. It's important to understand those requests, but don't blindly follow them like a lost sheep.

Stay laser-focused on the core problems. Sure, the ideas might sound intriguing, but watch out for that dreaded feature creep.

Ask probing questions to uncover the motivations behind those feature requests. This will help you [prioritize and effectively address customer needs like a boss]((/posts/product-management-prioritization-guide/).

Now, let's wrap this chapter up with a couple of essential points: avoid being overly "pitchy" and zip it to listen.

Being pitchy is like desperately seeking compliments and approval by annoyingly persisting with your ideas. If you catch yourself doing it, stop! Apologize and steer the conversation back to the other person's perspective and problems. Remember, annoying people will say your idea is brilliant just to get

## Chapter 3 - Asking Important Questions

It's critically important to figure out and ask questions that could potentially change or disprove your business. Trivial questions do not move your business forward, and thought experiments can help you find critical pieces of information. It's crucial to ask questions that scare you, and bad news is valuable because it brings you closer to the truth. The worst thing you can do is ignore bad news and search for validation. Lukewarm responses are informative and reliable, and it's worth asking follow-up questions to understand the nature of someone's apathy. False positives are not helpful, and you should focus on learning the truth. 

### Look before you zoom
Also, make sure you are keeping the big picture in mind. Don't prematurely zooming in on a specific problem before understanding the bigger picture. The author uses an example of a bad conversation where the interviewer asks about a person's biggest problem with going to the gym, assuming that staying fit is a real problem for them. The conversation goes off track and the interviewer misses the point. 

Start with generic questions to see if the problem is a must-solve problem that people are ready to pay for. It also advises asking cost/value questions to find out if the person is taking the problem seriously. The rule of thumb is to start broad and not zoom in until a strong signal is found.

### Gaze upon the elephant

When it comes to building a business, it's important to address both product and market risks. Sometimes we focus on asking questions that don't actually resolve critical issues or de-risk our business. Ignoring the "elephant in the room" can lead to wasted time and missed opportunities.

Understanding customer and market risks is essential. However, we must also consider product risks and not overlook them. Conversations with customers provide valuable insights, but they can't prove everything, especially when heavy product risk is involved. Building a great product requires taking action and starting earlier, even with less certainty.

Try to strike a balance between addressing product and market risks, ask the right questions, and be proactive in building something remarkable.

### Prepare your list of three
So, you're preparing to talk to different types of people, like customers, investors, or industry experts, and you want to make the most of those conversations. The key is to have a list of three important things you want to learn from each person. This pre-planning saves you from asking biased or trivial questions that won't get you anywhere. Plus, it's easier to face the tough questions when you're sitting calmly with your team rather than during an unplanned conversation.

Remember, your three questions will be different for each type of person you're talking to. Don't stress about finding the "perfect" questions because they'll change over time anyway. Just focus on what's murkiest or most important to you right now. Getting answers from different people will give you a better sense of direction. Maybe customer A answers questions 1-3, customer B gives you answer 4, and customer C covers answers 5-7. It might sound repetitive, but you don't need to repeat the whole set of questions with every person. Your time is valuable, so just pick up where you left off and keep filling in the picture.

Having your list of three big questions also comes in handy when you stumble upon a serendipitous encounter. Instead of just exchanging business cards like everyone else, you can casually throw out your most important question. Trust me, it'll make a lasting impression.

## Chapter 4 - Keep it casual
You need to get in front of people to talk about your product. Steve Blank suggests having three meetings in his book TK. This traditional three-meeting structure for customer development can be difficult and time-consuming to implement. 

Instead, adopting a casual approach and engaging in quick chats with customers proves more effective in learning about their problems. By stripping away formality and focusing on immediate conversations, founders can gather valuable insights and establish connections without the overhead of formal meetings. Keeping it casual allows for faster and lighter interactions, making it possible to have multiple customer conversations at industry events. The key is to prioritize informal chats over formal meetings when learning about customer problems.

### The meeting anti-pattern
I'm a naturally shy person and have always had a hard time just walking up and talking to someone. If I want to setup an interview, I'd much prefer to schedule a meeting. 

Here's the thing though, formality is a crutch we lean on in uncomfortable situations. We end up turning everything into a rigid process, but it costs us valuable time and kills the learning vibes. So, let's skip the suits, ditch the boardroom coffee, and ask the right questions in a way that keeps things interesting. You can chat anywhere, anytime, and save those formal meetings for when you've got something concrete to show. Trust me, these convos can be a blast for both parties involved. You might just be the first person in ages who genuinely cares about the little annoyances of their day. And here's a pro tip: if it feels like they're doing you a favor by talking to you, well, that's a sure sign that things are way too formal.

### How long are meetings
One of the benefits of keeping the meetings casual, as opposed to scheduling them, is they tend to be shorter. 

If you leave your product out of the conversation and focus on problems, you tend to have shorter meetings.

### Putting it together
Casual conversations or scheduled meetings, try to keep the conversation casual so as to avoid introducing biases.

If they don't know you're asking about a problem you're doing it right.

## Chapter 5 - Commitment and Advancement

Chapter 5 introduces the concept of advancing relationships with customers by asking for commitments. It highlights the importance of avoiding ambiguous outcomes and unproductive meetings. The author emphasizes that meetings either succeed or fail and discusses traps to avoid, such as seeking compliments or failing to ask for clear commitments. Rejection is seen as a learning opportunity, while not asking for commitment is considered a real failure. The chapter emphasizes the significance of commitment as a measure of customer interest and stresses the need for clarity regarding next steps after meetings.

### The Currencies of Conversation
Commitment is likened to currency in conversations. Compliments hold no value as they cost nothing, while major currencies are time, reputation risk, and cash. Time commitments can include setting clear goals for the next meeting, providing feedback on wireframes, or using the product trial extensively. Reputation risk commitments involve introductions to peers, decision makers, or giving testimonials. Financial commitments can range from a letter of intent to pre-orders or deposits. Strong commitments often involve a combination of multiple currencies. Compliments are not progress in validating a product, but they can serve as a warning sign that the person may not be genuinely interested. The rule of thumb is that the more someone is willing to give up, the more seriously their words can be taken.

### Good meeting / bad meeting

This section discusses different scenarios of meetings and evaluates whether they are good or bad based on the outcomes. Here's a summary of each scenario:

- "That's so cool. I love it!": Bad meeting because it's just a compliment with no valuable data. Deflect the compliment and get back to business.
- "Looks great. Let me know when it launches.": Bad meeting because it's a compliment combined with a stalling tactic. Look for a commitment today, such as an introduction or agreement to be an alpha user.
- "There are a couple people I can introduce you to when you're ready.": Nearly a good meeting, but the promise is generic and lacks specifics. Convert fuzzy promises into concrete actions and clarify what "ready" means.
- "What are the next steps?": Usually a good meeting as it indicates progress to the next step. However, ensure you know your next steps and avoid non-committal responses.
- "I would definitely buy that.": Bad meeting because false positives are high. Shift from future promises to concrete commitments, such as a letter of intent or pre-purchase.
- "When can we start the trial?": Maybe a good meeting as it shows a step forward. Assess the commitment level by considering what they are giving up for the trial.
- "Can I buy the prototype?": Great meeting as it indicates genuine interest and a willingness to purchase.
- "When can you come back to talk to the rest of the team?": Good meeting, especially in enterprise sales. It signals that multiple stakeholders are involved, which is necessary in B2B sales.

Understanding the outcomes of these scenarios helps in assessing the quality of the meetings and taking appropriate actions.

### How to fix a bad meeting
Get them to give you something concrete, you're only going to get a clear commitment if you ask for something specific

### Don't pitch blind
This section emphasizes the importance of gathering knowledge and learning in sales meetings, rather than blindly pitching your product. It suggests starting with open-ended questions to understand the customer's unique situation, which improves the conversation and increases the chances of closing the deal. Rejection without feedback is not helpful, so it's important to ask learning questions that provide valuable insights. 

The section also highlights the significance of early customers, often referred to as "earlyvangelists" or early evangelists. These customers are passionate about your product and willing to take a risk by being the first to try it. They have the problem, know they have it, possess the budget to solve it, and may have already improvised their own solution. These early customers show deep emotional attachment to your product and are willing to commit, even when it may not make rational sense to do so.

The key takeaways are: 

1. Customers who lack emotional attachment are unlikely to become early customers, so keep them on the list but don't rely on them for initial sales.
2. Customers who display deep emotion and enthusiasm should be cherished and kept close, as they can be your precious fans who support you through difficult times and make your first sale.

In early-stage sales, the primary goal is learning, with revenue being a side-effect. Advancement and commitments from customers help distinguish between dead leads and potential real customers.

## Chapter 6 - Finding Conversations

### Going to them
This is hard, cold calls suck. Goal is to stop doing these as soon as you can.

This method sucks, but even if the rejection rate is high, all it takes is one positive response to get started. The section also encourages staying open to serendipitous opportunities and seizing chances to engage in conversations that provide useful customer insights.

1. Find a good excuse to start a conversatoin. By showing genuine interest in people's lives and problems, without explicitly mentioning your business, you can have engaging conversations. However, it's important to note that transitioning such conversations into product or sales discussions can be challenging without revealing the initial deception and potentially damaging trust.
2. Immersing oneself in relevant communities or events is suggested as a way to meet potential customers and learn about their needs. The example of attending conferences and engaging with speakers and event organizers is given as an effective approach.
3. The use of landing pages and generic launches is also mentioned as methods to collect emails of qualified leads, which can then be used as a basis for initiating conversations.

### Bringing them to you
It's much better to bring people to you rather than seek them out.

Several methods are suggested:

1. Organize meetups: By hosting your own event focused on a specific audience or industry, such as an "HR professionals happy hour," you can easily engage in conversations with attendees and gain insights into their problems. Organizing meetups not only provides rapid customer learning but also boosts your credibility in the industry.
2. Speaking and teaching: Utilize your expertise and opinions to teach others through conferences, workshops, online videos, blogging, or free consulting. Teaching allows you to refine your message, connect with potential customers, and identify which aspects of your offering resonate with them.
3. Industry blogging: If you have a relevant blog audience, writing a post and asking people to get in touch can facilitate conversations with potential customers. Even without an existing audience, blogging about the industry can still be beneficial as it showcases your expertise and makes you an interesting person to talk to.

Get clever: Tailor your approach to your specific business needs and situation. Look for unique hacks or creative solutions that can help you understand customer problems and gain access to decision-makers. An example is organizing a knowledge exchange call between department heads of top universities to absorb credibility and establish direct connections with potential leads.

### Creating warm intros
Warm introductions are valuable in establishing credibility and facilitating conversations with potential customers. 

Here are the key points:

1. Seven degrees of Kevin Bacon: The world is interconnected, and people often have mutual acquaintances. By asking around and leveraging your network, you can find connections to reach the individuals or organizations you're targeting. Simply asking for introductions or reaching out to people who may know someone can lead to valuable connections.

2. Industry advisors: Advisors can play a crucial role in providing credible introductions. By having advisors who are knowledgeable and respected in your industry, you can tap into their network and gain access to potential customers. Establishing regular meetings with advisors allows for a consistent flow of introductions without burdening them with excessive time commitments.

3. Universities: Professors and academic institutions can be excellent sources for introductions. Professors often have connections with industry professionals who are interested in new projects and research. Contacting professors and visiting their offices can help you access their network and gain valuable introductions.

4. Investors: Top-tier investors are particularly beneficial for B2B introductions. They not only have their own extensive network but can also make cold introductions across various industries. Leveraging investor connections can help you secure introductions to key players in your target market.

5. Cash in favors: When people express interest in helping you or staying updated, it's an opportunity to call in those favors. Reach out to individuals who showed initial interest and ask for introductions to relevant contacts. While you may face some rejection, the goal is to initiate conversations and start building relationships.

It's important to exercise caution and avoid overusing these methods, as they can be perceived as annoying or burn bridges if not handled appropriately. However, when you're in a challenging situation and need to get started, leveraging existing connections and favors can provide a necessary push.

### Asking For and Framing the Meeting

You need to make sure that meetings are productive so you're not waisting your time or theirs. Here are the key points to make sure that happens.

1. Avoiding sales meetings: When you need to have a meeting but don't have a specific product or service to sell, it's important to provide the right explanation and framing to set clear expectations and maximize the value of the meeting
2. Symptoms of poor framing: Vague or generic openings like "Um. So..." or "How's it going?" can lead to unproductive sales-oriented meetings. Similarly, framing the meeting as an interview or seeking compliments/approval can create the wrong impression.
3. The framing format: The mnemonic "Very Few Wizards Properly Ask [for help]" represents the five key elements of effective framing: Vision, Framing, Weakness, Pedestal, and Ask.
4. Example email framing: The author provides examples of how to structure the email requesting a meeting by articulating the vision, stating the stage of development, expressing a specific problem or challenge, highlighting the expertise of the person being contacted, and explicitly asking for help.
5. Taking control of the meeting: Once the meeting starts, it's crucial to set the agenda, ask good questions, and keep the conversation focused. Being assertive and prepared with a plan for the meeting helps maintain control and achieve the desired outcomes.
6. Warm intros vs. cold approaches: Warm introductions, facilitated through connections or referrals, tend to yield higher response rates and more productive meetings. Cold approaches, such as cold emails, can be challenging, and it's recommended to focus on generating warm intros instead.

By framing meetings effectively and taking charge of the conversation, entrepreneurs can maximize the value of their interactions and increase the likelihood of receiving helpful insights and assistance.

### To Commute or Call

The author suggests the best thing to do is be in the same room. I don't think this is valid anymore with the proliferation of Zoom and other great video conferencing solutions. You can cast a wider net if you can video conference.

### The Advisory Flip

Rather than approaching meetings with a customer-focused mindset, entrepreneurs should seek industry and customer advisors. This shift in mindset changes the purpose of the meeting from a sales-oriented approach to a learning-oriented one where entrepreneurs evaluate potential advisors through thoughtful questions. It puts entrepreneurs in control of the meeting and changes the dynamic. 

This is a really great approach that is also discussed in the book Running Lean.

### How Many Meetings?

Every meeting has an opportunity cost, taking time away from other business activities. In sales-driven businesses, early conversations have a low opportunity cost as they can serve as both learning opportunities and potential sales leads. 

The [Nielsen Norman Group suggest you only need to interview 5 users](https://www.nngroup.com/articles/why-you-only-need-to-test-with-5-users/). Practically speaking, interview people until you no longer hear anything new.

## Chapter 7 - Choosing Your Customers

If you think your product is good for everyone, it's probably good for no one. 
### Segmentation

The author shares to examples of businesses that realized thaey needed to focus on specific customer segments. Doing so allows you to tailor products and services more effectively and get consistent feedback to drive business decisions.

### Customer Sliciing
If you feel like your audience is too big you need a way to narrow things down. One way is to involves asking questions to narrow down the customer segment into more tangible subsets based on motivations, goals, and demographics. The goal is to create a "who-where" pair, identifying the specific type of customer and where to find them. By slicing the customer segment, it becomes easier to make informed decisions and have targeted customer conversations.

He suggests the following questions:
* Within this group, which type of person would want it most?
* Would everyone within this group buy/use it, or only some?
* Why does that sub-set want it? (e.g. what is their specific problem)
* Does everyone in the group have that motivation or only some?
* What additional motivations are there?
* Which other types of people have these motivations?

Once you narrow the segment down, you can star with customers that are profitable, easy to reach, or the type of people or industry you want to be around.

### Talking to the Wrong People
If you haven't properly segmented your audience, you're going to talk to the wrong people.

If you have multiple customer segments you have to make sure you're talking to all of them. For example, a social network with an advertising business model needs to talk to the users of the social network and the advertisers.

If you're selling enterprise you need to make sure you're talking to the right stakeholders. Is the users the same as the purchaser? If not, you need to talk to both.
## Chapter 8 - Running The Process

### Prepping
You will need to do initial prep work before starting the interview sessions. This is important so you aren't waisting anyone's time.

- Identify three important questions to address in customer interviews and confront challenging questions head-on.
- Determine the desired commitment and next steps following the interviews, based on existing knowledge.
- Create a framework of assumptions about customer interests and needs to guide the conversation.
- Prioritize in-person conversations for gaining insights that cannot be obtained through desk research.
- Conduct basic due diligence on LinkedIn and company websites before meeting with businesses.
- Involve the entire founding team to ensure a comprehensive perspective during preparation.
- Demonstrate the value of customer conversations in saving time and gaining insights to overcome initial resistance.
- Trust your instincts and clearly define learning goals before engaging in conversations.
- Avoid conversations without clear learning objectives.
### Reviewing
After the meeting review your notes with the team. You want to make sure you've captured everything correctly. You also want to imprint the information on your team. 

Disseminate the notes and quotes to the team as quckily as possible.

Do a post-mortem to figure out how to adjust for future interviews.

This is similar to lunch and learns I've done with teams. 
### Who should be there
Try to have some people from your team attending a couple of these meetings. Hearing people talk about problems first hand is always better than reading notes.

If you're shy, try and get someone to help you for your first couple of sessions.
### How to write it down
Create a template.
Use shorthand (emoticons, symbols) to quickly capture information.
Put things in quotes if they are important. You can use these things later for slide decks.
### Where to write it down
The author suggests note taking directly in Google docs, a spreadsheet or other digital medium. 

Might be better to write notes down and transfer them to somewhere permanent later. 

Make sure they are lightweight and you can review them with the team.

### The Process
Customer interviews are a tool, not an obligation. If you don't feel like you're getting anything out of it, don't waste your time.

## Conclusion & Cheatsheet

The last part of the book provides a handy cheatsheet you can use to run the process.

All in all, a pretty solid book. Some of the information is obvious, especially in the latter half of the book but having it all put together in one place makes it a great reference when conducting customer interviews.
