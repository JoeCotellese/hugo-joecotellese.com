---
title: The Now Habit Book Summary
date: 2022-09-06
draft: true
lastmod: 2023-05-21T20:49:53.166Z
tags:
  - books
---

The Now Habit by Neil Fiore is a book about breaking through your problems with procrastination. This book summary was written to help you brush up on the key points after reading or listening to the audio book.
<!--more-->
Introduction
------------

*   Neil introduces the concept of guilt-free play.
*   Procrastinators can get stuff done, they are just more stressed about it
    *   quality of end work goes down

From Procrastinator to Producer
-------------------------------

*   Procrastination is a vicious cycle
*   You can't relax without feeling guilty
*   This book helps you think like a producer

A New Definition of Procrastination
-----------------------------------

*   People procrastinate out of fear.
    *   criticism
    *   failure
    *   own perfectionism
*   Overcome procrastination with positive attitude about human spirit
*   Somewhere in your life there are leisure and forms of work you do without hesitation.
    *   you're not a 24 hour procrastinator
    *   use these experience to stop thinking of yourself as a procrastinator
*   Procrastination is "an attempt to resolve a variety of underlying issues, including low self-esteem, perfectionism, fear of failure and of success, indecisiveness, an imbalance between work and play, ineffective goal-setting, and negative concepts about work and yourself."
*   New definition -- "Procrastination is a mechanism for coping with the anxiety associated with starting or competing any task or decision."

The Now Habit
-------------

*   Hearing try harder, get it done, do it, get organized - doesn't actually help
*   need tools to create a positive inner dialog
*   this book is strategic, not just how to advice
*   has 10 tools to overcome procrastination

1.  Creating Safety
    1.  less fear of failure
    2.  bounce back from mistakes
2.  Reprogram negative attitude with positive self-talk
    1.  identify the negative messages
    2.  see how they affect you
    3.  replace them with positive phrases
3.  Use symptom to trigger cure
    1.  use old habits to form new habits
4.  Guit-free play
    1.  schedule your leisure time
    2.  creates urge to return to work
5.  3D thinking and the reverse calendar
    1.  create step-by-step calendar of path to achievement
6.  Make worry work for you
7.  The Unschedule
    1.  help you track quality time on projects and play
8.  Setting realistic goals
    1.  get rid of guilt producing goals that you can't work on
9.  Working in the flow state
    1.  get focused in < 2 minutes
10.  Controlled setbacks
    1.  prepare for them
    2.  turn them into opportunities

Expect a Miraculous Change
--------------------------

The strategies aren't new but what is new is using them in your own life.

Why We Procrastinate
====================

Warning Signs of Procrastination
--------------------------------

There are six warning signs that will point to potential issues with procrastination. They are in summary

1.  Long list of unmet obligations
2.  Unrealistic views of time
3.  Vague goals or values
4.  Feeling unfulfilled, frustrated or depressed
5.  Indecisiveness
6.  Low self-esteem, lack of assertiveness

If you feel any of these you probably procrastinate

A Positive View of the Human Spirit
-----------------------------------

*   Your body has a resilient healing system.
    *   Humor and positive emotions have healing potential
    *   This book focuses on this to tackle procrastination
*   Procrastination is not a character defect.
    *   Coping mechanism

Our Work Critic: Ourselves
--------------------------

*   Neil presents an example of Claire,
    *   Learn to be the best in everything you do
    *   Criticism if I mess up
    *   praise is withheld as a child
    *   efforts aren't good enough
    *   finish a project - get criticism or constructive feedback
    *   Claire developed negative self-talk
    *   Exercise for Claire
        *   keep track of when and why she procrastinated for a few days
        *   when she was procrastinating realize it was to escape inner conflict and anxiety
    *   Claire started getting past fear of failure
    *   thinking like a producer

Procrastination is Rewarding
----------------------------

*   1 main reason we procrastinate
    *   "it rewards us with _temporary_ relief from stress"
*   Takes us away from something painful
    *   more painful work is, more we seek relief through avoidance
    *   endless work deprives

How We Procrastinate
====================

How To Talk To Yourself
=======================

Guilt-Free Play, Quality Work
=============================

Overcoming Blocks to Action
===========================

The Unschedule
==============

Working in the Flow State
=========================

Fine-Tuning Your Progress
=========================

The Procrastinator in Your Life
===============================