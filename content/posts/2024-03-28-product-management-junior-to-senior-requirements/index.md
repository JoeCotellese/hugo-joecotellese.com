---
title: "Product Management Career Path: How to get a promotion"
blogTitle: How to get a promotion as a product manager
slug: product-management-junior-to-senior-career-chart
description: Unlock your product management career potential with this handy career progression cheatsheet.
date: 2024-03-28T13:34:13.345Z
preview: ""
draft: true
tags: []
categories: []
lastmod: 2024-04-05T12:59:14.584Z
type: default
---

1. Search volume for book - target those terms.
2. make title, translating the PM bible to the realworld
3. I've been doing this a really long time, here's how to use it.
4. This is how I assess levels of competency and level up their careers

Here's the chart I use to hire and assess product managers.

Elevate my writing. Be empathetic, that's great but tout my credentials a little more.

"Poof, you're a product manager!" And just like that, I was supporting a product that we just sold to our company's largest customer *ever*. I had no idea what to do and, back then, there weren't many people, books or even websites to turn to.

Work my way up the trenches, now, I'm in a position to mentor, coach, and lead product teams. In those positions I'm often asked "How do I level up my career?"

I tell people to read this book.

Like many product managers, I eventually came across [Marty Cagan's "Inspired."](/posts/marty-cagan-inspired-detailed-book-summary-study-guide/) It's like the holy grail of product management books for me and always tops [my product management reading list](/posts/best-books-for-product-management/) for anyone looking to sharpen their skills.

Created framework to hire and assess product managers based on the ideas in the book.

Recognizing the challenges that come with stepping into product management, I've taken the essence of "Inspired" and laid it out into the chart below. This chart shows each key area of the book Inspired and maps the expections I have of product managers across various career stages.

Use this chart with your team leader to help lay down the responsibilities you'll need to focus on to develop your product management career.

| Role or Responsibility                   | Junior                                                                            | Mid                                                                                                                                                                      | Senior                                                                                                                                                                                                             |
| -------------------------------------------------------------------- | --------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Customer Expertise            | Develop an understanding of customer needs through research and feedback.         | Become the go-to person for understanding anything about our customers, sharing insights across the company.                                                             | Lead strategic customer insight initiatives, and mentor others in customer research techniques. Develop deep, segment-specific customer expertise that guides long-term product strategy.                          |
| Business and Market Knowledge       | Gain familiarity with the business, market, and industry.                         | Develop a deep understanding of our business, market, and industry, driving products that meet our business needs while satisfying customer desires.                     | Provide thought leadership on market trends, and competitive landscape. Influence product and business strategy based on deep industry insights. Mentor others in understanding market dynamics.                   |
| Data-Driven Decision Making               | Assist in gathering and analyzing data to support product decisions.              | Leverage quantitative and qualitative data to inform product decisions, ensuring what we build is truly worth building.                                                  | Champion a culture of data-driven decision making, setting standards for how data is used for product decision-making. Lead complex analyses and translate data insights into strategic actions.                   |
| Leadership and Collaboration           | Support cross-functional teams under guidance.                                    | Work closely with engineering, design, and business teams to discover and deliver products. Demonstrate leadership and inspire your team, even without direct authority. | Lead cross-functional initiatives, driving collaboration and innovation across teams. Serve as a role model for effective leadership, influencing without authority across the organization.                       |
| Product Lifecycle Management     | Assist with various stages of the product lifecycle under supervision.            | Manage all aspects of a successful product throughout its lifecycle.                                                                                                     | Strategize and oversee the product lifecycle for multiple products or a product line, including market withdrawal. Define best practices and processes for product lifecycle management.                           |
| Problem-solving Skills                 | Contribute to problem-solving with guidance.                                      | Exhibit strong problem-solving skills and take initiative to get the job done.                                                                                           | Lead strategic problem-solving initiatives, identifying and addressing systemic challenges. Mentor teams in advanced problem-solving techniques and foster an environment of continuous improvement.               |
| Communication Skills                  | Develop communication skills, sharing findings and recommendations with the team. | Excel in written and verbal communication, effectively sharing complex ideas with stakeholders.                                                                          | Master the art of communication, influencing decision-making at all levels of the organization. Coach others in effective communication strategies.                                                                |
| Understanding of Technology    | Learn and understand the technical aspects relevant to the product.               | Have a strong grasp of technology with the ability to identify new opportunities for growth and innovation.                                                              | Possess deep technical expertise, driving technological innovation. Mentor others in technological understanding and its impact on product development.                                                            |
| Strategic Vision                         | Support the development of product vision and strategy under supervision.         | Contribute significantly to the product vision and strategy, aligning with business objectives.                                                                          | Define and champion the product vision and strategy, leading long-term strategic planning. Mentor others in strategic thinking and vision development.                                                             |
| Market Positioning and Competitive Analysis  | Assist in the analysis of market position and competition.                        | Independently conduct market positioning and competitive analysis, informing product decisions.                                                                          | Lead strategic market positioning and competitive analysis initiatives, shaping the product strategy to achieve competitive advantage. Teach others how to effectively analyze and respond to competitive threats. |

