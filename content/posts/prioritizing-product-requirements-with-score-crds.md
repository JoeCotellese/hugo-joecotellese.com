---
title: Prioritizing Product Requirements with Scorecards
date: Wed, 17 Feb 2016 12:46:52 +0000
draft: false
lastmod: 2023-05-05T21:21:05.280Z
---

One of the product managers core responsibilities is to effectively [prioritize product features](/posts/product-management-prioritization-guide). To an outside observer this often looks like reading tea leaves. Creating a process in your organization to effectively rank product initiatives will help create transparency and avoid squeaky wheel prioritization. I've collected some research on the topic below. This first article from Karl Weigers suggests using a cost / benefit ratio to prioritize product features. I've used this method in the past and thought it was pretty effective.

> [First Things First: Prioritizing Requirements](https://www.processimpact.com/articles/prioritizing.html) Customers are never thrilled to find out they can't get all the features they want in release 1.0 of a new software product (at least, not if they want the features to work). However, if the development team can not deliver every requirement by the scheduled initial delivery date, the project stakeholders must agree on which subset to implement first. Any project with resource limitations has to establish the relative priorities of the requested features, use cases, or functional requirements. Prioritization helps the project manager resolve conflicts, plan for staged deliveries, and make the necessary trade-off decisions.

This next post is a little different. It advocates using the Kano Model, a method developed in Japan in the 80s to prioritize features. I've read that this is often used by agile teams and it is somethig we might experiment with.

> [Leveraging the Kano Model for Optimal Results | UX Magazine](https://uxmag.com/articles/leveraging-the-kano-model-for-optimal-results) The ‘Kano Model’ is used to measures customer emotional reaction to individual features. At projekt202, we’ve had great success in doing just that. Our success emerged from revisiting Kano’s original research and through trial and error. What we discovered is that it really matters how you design and perform a Kano study. It matters how you analyze and visualize the results.

Finally, @JeffLash argues against using scorecards at all. I don't really agree with this but it's useful to have a counter argument.

> [How do product managers prioritize requirements?: Ask A Good Product Manager: Your product management questions answered](https://ask.goodproductmanager.com/2008/01/21/how-do-product-managers-prioritize-requirements/) "I’ve known people who have tried scorecards, ranking systems, voting (see Product Development is not a democracy), but ultimately these are just ways to avoid the responsibility that product managers have to ultimately make the important decisions."

## Your Turn

Scorecards aren't a way to avoid responsibility, they are a tool used to help you with one of your most important tasks - prioritizing features. How do you go about prioritizing your product features?
