---
title: Product Management is not Project Management
date: 2021-03-29
draft: false
lastmod: 2022-05-20T19:00:18.122Z
---

These two terms are often used interchangeably and yet, other than having the word "management" and sort of sounding the same, they are vastly different things. 

In many organizations senior management will point to a project manager and say "He's our product manager." 
<!--more-->
This isn't the case. A project manager's focus is operational. He helps coordinate resources to get a task moved through to completion. His single minded focus is to deliver a project on time. 

A product manager on the other hand works with internal and external stakeholders to ensure that the thing being built is the right thing for the market. A product manager works with executive team to ensure that the things that are built align with the vision of the company. 

They should understand the market, know their competition and anticipate changes in the market so that the company remains relevant. They work with developers to ensure that the product features they are working on align with what customers are asking for. They should also balance the backlog of feature requests weighing the opportunity cost of one feature over another so that the right thing is built. They work with Marketing and communications to help them understand the product and give them the material they need to create the product collateral and marketing plan. 

Product managers should brought on board early in the life of any organization that wants to grow beyond the founder's vision of a single product.