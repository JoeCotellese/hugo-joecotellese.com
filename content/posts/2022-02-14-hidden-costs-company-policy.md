---
title: What are the hidden costs of a company policy?
slug: hidden-costs-company-policy
description: Company policies have hidden costs. Have you thought through them?
summary: Policies may be necessary for managing exceptions, but they can damage your brand and bottom line. This article draws from experiences with Ritz Carlton, Netflix, and a Wordpress add-on company to argue that employees who are trusted to act in the best interest of the company are more accountable and create a better customer experience. Instead of relying on policies, companies should focus on being amazing and providing excellent service.
author: Joe Cotellese
date: 2022-02-14T17:29:10.954Z
draft: false
tags: []
categories: []
lastmod: 2023-05-05T20:56:51.776Z
---

A number of years ago I heard a talk from a Ritz Carlton executive.

The executive told the audience that Ritz employees are [empowered to spend up to $2,000](https://hbr.org/2010/09/what-happens-when-you-really-m-2) without manager approval when solving a customer problem.

Ritz Carlton trusts their employees to make decisions that are in the best interest of the customer, which in turn, is in the best interest of the Ritz Carlton.

Recently, I read the book [No Rules Rules by Reed Hastings](https://amzn.to/34DKN2U). They have few company policies because they discovered that employees are more accountable when there are fewer rules in place.

Netflix's rationale is that if you have the right people in place then they will act in the best interest of the company. Policies manage the exceptions and get in the way.


## Policies damage your brand and bottom line

I've thought about both of these idea recently after a poor interaction with a Wordpress add-on company.

The company's product is a a Wordpress site builder. As a long time customer, I've spent somewhere around a thousand dollars with them. I have recommennded them to anyone with a problem they solve.

About a year ago my subscription lapsed. It was my mistake, it was one of those things on my todo list that never got done.

When I attempted to renew my subscription I discovered I have to rebuy the product at the full price rather than the annual discounted price. A difference of about fifty dollars.

When I contacted support, I was told that I’m out of the two-week upgrade window that I would have to rebuy the product.

I’m sure this customer service person is thinking she’s doing the right thing because the "policy" is that the renewal price is only valid up to two weeks after renewal.

This policy probably helps their short-term revenue but the long term impact is that I've cancelled my other account with them and no longer recommend them to anyone.

Do you have policies that seem like a good idea but have hidden, damaging, side effects? Make sure that you are thinking about the positive and negative impacts so you aren't surprised by the side effects.

In the end, the only policy you need to have in pace is to "[be amazing](/posts/be-amazing)."
