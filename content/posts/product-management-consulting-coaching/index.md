---
title: "Coaching vs. Directing: Mastering Leadership as Head of Product"
slug: product-management-leadership-coaching-directing-teams
description: Explore the balance between coaching and directing to empower your team and enhance product management. Learn key strategies for effective leadership.
date: 2024-02-03T17:12:36.660Z
draft: false
lastmod: 2024-02-04T14:29:32.437Z
tags:
    - leadership
---

I had my first real experience at coaching years ago when my daughters laced up their cleats for their first soccer season. 

These young athletes, had enthusiasm but lacked experience and they looked to me for guidance.

I set a unique goal for myself: to cultivate a love for soccer within these young players, measuring success not by wins, but by the number of girls eager to return next season.

I think about my time coaching whenever I am working with people on my team. I try to always be the coach but, depending on the individual or the circumstances you may have to give them direct guidance.

In this article I explore the difference between coaching and directing. I want you to know the difference and undersand when to switch strategies. Getting this right is essential to help your team reach their potential.

![](i-want-leadership.webp)

## Mastering the Art of Coaching

Consider a product manager who is struggling to figure out how to properly prioritize the features in their product backlog. If you take a coaching approach the aim isn't to dictate them how to figure it out but to empower them to navigate the challenge independently.

### Strategies for Effective Coaching

- **Initiate with Thought-Provoking Questions:** Start conversations that promote deep thinking and introspection. Ask, "What are some of the factors you are considering as you review your product backlog?" Or, "What [product management frameworks](/posts/product-management-prioritization-guide/) are you using to help drive your decisions?"
- **Embrace Active Listening:** Immerse yourself in their thought process, acknowledging their challenges without immediately jumping to solutions.
- **Promote Self-Discovery:** Guide them towards discovering their own solutions, asking, "What impact might prioritizing Feature A over Feature B have on our users?"
- **Support Autonomy:** Encourage them to take charge of their decision-making, asking, "How do you plan to present your prioritization strategy to the team?" Or "How can you be sure that product stakeholders understand your decisions?"

## Knowing When to Direct

Sometimes, the situation calls for a more authoritative approach. Revisiting the product manager's predicament:

### Directing with Clarity

- **Offer Definite Guidance:** Explain the company's strategic goals and their relation to the product's roadmap, emphasizing, "Our focus this quarter is enhancing user engagement, necessitating priority on features that elevate this goal."
- **Establish Clear Expectations:** Detail the expected results, stating, "I need a prioritized feature list by week's end that aligns with our engagement objectives."
- **Present Specific Solutions:** Offer direct solutions, suggesting, "Feedback indicates Feature X should be our top priority, with Feature Y as a close second." Or "Use the MoSCoW method to decide on your priorities."
- **Implement Checkpoints:** Set benchmarks for evaluating progress and schedule reviews of the prioritization list.

## Deciding Between Coaching and Directing

The choice between coaching and directing depends on the scenario, the individual team member's needs, and the overarching team and organizational goals.

### When to Opt for Directing instead of Coaching

- **Time Constraints:** In urgent situations requiring immediate action, direct instructions save time.
- **Skill Gaps:** If a team member lacks essential skills or knowledge and learning opportunities are limited, direct guidance ensures task completion.
- **Non-Negotiable Outcomes:** For tasks where specific outcomes are critical or standard processes must be followed, direct instructions are necessary.

### Opting for Coaching instead of Directing

- **Growth Opportunities:** When a situation offers a learning opportunity, coaching can unlock potential and foster long-term development.
- **Boosting Engagement:** Coaching enhances engagement by actively involving team members in problem-solving, increasing their sense of value.
- **Fostering Independence:** Coaching cultivates critical thinking and ownership, preparing team members for autonomous decision-making.

### Making the Choice

1. **Evaluate the Circumstances:** Assess urgency, learning potential, and the team member's current capabilities.
2. **Consider the Individual's Needs:** Reflect on their developmental goals and their preference for independence or guidance.
3. **Weigh the Overall Impact:** Think about how your approach affects team dynamics and goal achievement.

### Implementing Your Decision

- **Begin with Inquiry:** Use open-ended questions to understand the team member's perspective, aiding in deciding between coaching or directing.
- **Ensure Objective Clarity:** Whether coaching or directing, align on the desired outcomes to ensure shared expectations.
- **Provide Support:** When coaching, clarify your supportive role, indicating your availability for guidance as they explore solutions.

If you want to dig into this more, check out "[The Coaching Habit](https://amzn.to/3SFKMBP)" and "[The Advice Trap](https://amzn.to/48WQOUz)" by Michael Bungay Stanier. It underscores the importance of fostering a culture of curiosity and reflective action. 

## In Conclusion: Striking the Right Balance

The journey of managing product managers intertwines the essences of coaching—encouraging growth, fostering self-discovery, and advocating autonomy—with the decisiveness of directing, where clear guidance and expectations are paramount. Recognizing when to employ each approach amplifies your team's potential and paves the way for innovation and excellence. Like my soccer coaching endeavor, the ultimate objective in product management leadership is to ignite a fervent pursuit of excellence, nurturing a team's commitment to surpassing its goals.