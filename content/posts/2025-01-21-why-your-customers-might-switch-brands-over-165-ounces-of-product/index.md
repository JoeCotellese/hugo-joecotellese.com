---
title: "The 1.65 Ounce Problem: Hidden Product Design Insights"
description: Discover how a simple lotion bottle reveals crucial lessons about Jobs to be Done, customer loyalty, and product design. Learn why solving tiny pain points could unlock massive customer value.
date: 2025-01-21T17:34:50.781Z
draft: false
slug: the-1-65-ounce-problem-product-design-lessons
tags: []
categories: []
lastmod: 2025-01-21T18:04:03.048Z
images:
    - drains-the-lotion.jpg
---

Product design insights often emerge from unexpected places. While many companies invest heavily in customer research and focus groups, sometimes the most valuable product lessons come from observing everyday frustrations. This story about a simple lotion bottle reveals a powerful lesson about Jobs to be Done, customer loyalty, and why seemingly minor design flaws might be costing companies more than they realize.

## The Lotion Bottle Experiment

My wife is not a product person, but today reminded me that product lessons can come from anywhere – or anyone.

When I came downstairs to get my coffee, the mugs were blocked by this setup. She’d tipped a bottle of lotion into a cup overnight to collect the remains from the bottom of the bottle. I didn’t have to ask why. I know it grinds her gears that the bottles with the pump lids always stop working before the bottle is empty.

![Draining the Suave](featuredImage.png)

“Do you know how much lotion is in that cup?” she asked as I tried to extract a mug without upsetting her science project. 

I did not know.

She did.“1.65 ounces! That’s 5% of the bottle! And that’s an economy size bottle. So if it were a smaller one, I bet it would be an even bigger percentage! But do you think Jergens cares? No. They just want us to have to buy lotion 1.65 ounces earlier!”

I poured my coffee and gently pointed out that this lotion was Suave brand, not Jergens. 

## When Products Become Commodities

“You know what? I never notice what brand it is,” she said. “There are many adequate lotions. It doesn’t matter much to me. But if someone could fix this stupid design flaw, that would be the only one I’d buy. I’d be loyal for life.”

OK, fellow product people. You felt something click in your "product brains," too, right? This is a classic Jobs to be Done moment - you know, that framework where we focus on what customers are actually trying to accomplish rather than just the features they're using.

## Jobs to be Done: Beyond Basic Features

My wife views this product as a commodity because every lotion company is solving the wrong job. They think their job is to "help people moisturize their skin." But that's not the whole story. The real job is to "help people use every bit of moisturizer they paid for." When companies miss the true job, products become interchangeable - she can't even remember what brand she bought. She doesn't feel emotional about it all … until she's thwacking away at the pump cap that refuses to dispense the 1.65 ounces of lotion that she knows is still there.

## The True Cost of Ignored Pain Points

It’s a tiny pain point. It happens so far into the product lifecycle that it seems like it hardly matters. Heck, it might even make the company a few bucks because she has to buy lotion earlier. Cha-ching!

But is there potentially more money to be made by understanding this true job to be done? Would the powers that be at the company take that feedback? Would they tell the product person to just focus on making the lotion better and not worry about the delivery system? Would those internal silos kneecap an important and profitable improvement?

My wife doesn't care about any of that. She's not differentiating between the lotion and the thing the lotion comes in. She just wants to get the job done. And she's ready to devote her loyalty to the first company that truly understands what job she's really hiring their product to do.
