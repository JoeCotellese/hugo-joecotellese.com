---
title: Can't Keep Up? Use This Simple Product Prioritization Framework
description: |
  Learn how to prioritize your product backlog using the Value/Effort ratio for efficient and effective decision-making. Simplify and optimize your backlog management.
date: 2023-05-10T14:26:56.006Z
draft: false
slug: value-effort-ratio-prioritization
lastmod: 2023-05-15T20:28:47.140Z
---

---
**Key Ideas**
* Prioritization frameworks help you make sure you're focusing on the right things
* Ranking items based on value and effort is a good way to start prioritization.
---
When I was a kid picking a show to watch was fairly simple. I only had a few choices.

Today, I subscribe to too many streaming services, giving me more shows and movies that I will be able to watch in my lifetime.

Does this mean I'll always find something interesting to watch. 

Nope. 

Instead, I either spend my precious TV viewing window indecisively thrashing through my list or I fall back on watching a show I've seen before so I don't have to decide. 

![That's me messing with the remote](hamm-remote-control.gif)

As an engineer, of course I have a heuristic to help me decide on a show. I basically ask myself two questions.

1. How much do I really want to watch this based on the show description and 
2. how much time do I want to put into it?

Based on those two criteria, I decide on the best show to watch given the time I have available to me.

What's this have to do with product management? Well my dear friends, when we are deciding on how to prioritize what we should be building, we often have to weigh how valuable this is to the business and also the cost of building it. 

This is the basis of the most basic prioritization framework, one that I teach new product teams. So read on and I'll show you how to clean up your backlog using the Value / Effort ratio. 

---
**Follow along**

If you want to see how I prioritize the ClipDish backlog using this technique, [create an account at Airtable](https://airtable.com/invite/r/Nvjt8ERR) and copy this [shared base.](https://airtable.com/shrmEIVOmWWLXTPNw)

---

Ok so let's jump in. The entire process in a nutshell is this:
1. Rank backlog items based on value
2. Rank backlog items based on level of effort to implement
3. Divide the Value over the Effort to get a score.
4. Sort based on the Score

So what does this look like in practice? We'll walk through it using an example taken from the backlog for my app [ClipDish](https://getclipdish.com).

## Value - What's this worth to the business?
When I'm prioritizing the backlog the first thing I do is go through each of the items one by one and determine what the value is to the business and the customer. 

It's important to understand that these aren't static rankings. I might go back and rerank things based on whether business priorities change. 

In order to figure out the Value of the items on your backlog look at customer data, the market and stakeholders.

I like to identify a series of questions that I can use when evaluating each thing.

--- 
**Questions to help figure out the Value of a backlog item**
- Will this decrease the churn rate of our customers?
- Will this increase the activiation rate of new users?
- Will this drive new user signups?
- Does this impact a lot of users?
- Will this create a competitive moat for our business?
- Does this move us toward feature parity with a competitor?
- Will this move the needle on the KPIs that we are focused on this quarter or year?
- Does this align with our long term product vision?
- Are customers specifically asking for this feature?
---

Based on the answers to those questions, I rank each item on a scale from 1 - 10

{{< imgproc prioritizing-backlog-value.png Resize "800x" >}}

After I take a first pass at ranking them, I'll sort the sheet by the Value column and do a gut check on my rankings.

Remember, Value is specific to your business *at the time you are ranking things*.

## Effort - How much work do we have to put in to see the value?

After determining the value of each backlog item, it's time to assess the effort required to implement them. This is where you'll need to collaborate with your development team to estimate the amount of work involved in each task. 

Keep in mind that effort estimation is not an exact science, and it's essential to be flexible and open to adjustments as you progress. Some teams use story points, while others use time-based estimates like hours or days. Choose the method that works best for your team.

---
**Here are some factors to consider when estimating effort:**

- Complexity of the task
- Dependencies on other tasks or teams
- Availability of resources (team members, tools, etc.)
- Potential risks and uncertainties
Remember, the goal is to find a balance between the value a task brings and the effort required to complete it.
---

In this example, I'm ranking from 1 - 10. The numbers merely represent relative effort. 

As with Value, after everything is scored, I'll sort it by Effort and do a gut check which is basically just me asking the team "Do you think the relative estimates of these are right?"

{{< imgproc prioritizing-backlog-effort.png Resize "800x " >}}
## Calculate the score

Now that you have ranked your backlog items based on value and effort, it's time to calculate the score. To do this, simply divide the Value by the Effort for each item. This will give you a score that represents the return on investment (ROI) for each task.

For example, if a task has a value of 8 and an effort of 4, the score would be 8 / 4 = 2. The higher the score, the more valuable the task is relative to the effort required to complete it.

Once you have calculated the scores for all your backlog items, sort them in descending order. This will give you a prioritized list of tasks, with the highest-scoring items at the top.

{{< imgproc prioritizing-backlog-calculated-results.png Resize "800x" >}}

## Conclusion

Prioritizing your product backlog based on value and effort is a simple yet effective way to ensure that your team is working on the most impactful tasks. By focusing on tasks with a high return on investment, you can maximize the value delivered to your customers and stakeholders while making the best use of your team's time and resources.

Of course, this is just one method of prioritization, and there are [many other frameworks and techniques available](/posts/product-management-prioritization-guide/). However, the Value / Effort ratio is a great starting point for teams looking to bring order to their backlog and make more informed decisions about what to work on next. So give it a try, and watch your product development process become more efficient and effective.