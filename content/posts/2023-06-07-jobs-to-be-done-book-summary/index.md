---
title: Jobs to be Done Book Summary
description: The book Jobs To Be Done is summarized in this blog post
date: 2023-06-07T19:42:03.608Z
preview: ""
draft: true
tags: []
categories: []
lastmod: 2023-06-11T13:08:07.435Z
---
I recently re-read "Jobs to be Done" this time, taking copious notes. Below are my thoughts on the book.

## Introduction

"Jobs to be Done" begins with the author's personal experience of failure when a product he worked on, the IBM PCjr, turned out to be a flop in the marketplace. This failure motivated him to understand how customers judge the value of products and find a way to identify the metrics they use early in the product planning process.

>I remember the PCjr. The thing that really sticks out for me was I remember how terrible the chicklet style keyboard was. Had I been part of a interview process I would have mentioned that!

He studied various tools and methods related to product planning, including voice of the customer, quality function deployment, and conjoint analysis. He conducted extensive research and interviews with customers, worked with statisticians, and helped different teams formulate market and product strategies.

In 1990, the author realized that applying Six Sigma and process control principles to innovation could be effective by studying the process customers go through when using a product. This led him to develop a process called CD-MAP (customer-driven maps) and uncover the metrics that customers use to measure success and value in executing their processes.

He gained recognition from Clayton Christensen, who popularized the concept of "jobs to be done" in his book "The Innovator's Solution."

The author highlights the success of the ODI process, citing a study that showed an 86% success rate for products and services launched using ODI, compared to the average 17% success rate of traditional innovation processes. The focus on understanding the job-to-be-done and customer-defined metrics is credited for the higher predictability and profitability of innovation.

## Why does innovation fail?

The goal of innovation is to find solutions that meet unmet customer needs. There are two main approaches to innovation: the "ideas-first" approach and the "needs-first" approach. In the ideas-first approach, companies come up with product or service ideas and then test them with customers to see if they meet their needs. In the needs-first approach, companies first understand customer needs, identify unmet needs, and then create solutions to address those needs.

The ideas-first approach is flawed because it relies on generating a large number of ideas without knowing all the customer's needs. It is a guessing game based on hope and luck, and it lacks predictability. The needs-first approach, although not inherently flawed, often fails in execution. The evaluation and filtering processes used to select ideas are flawed because they are typically done without knowing the customer's needs. Customers themselves cannot articulate the solutions they want because they are not experts in the field. Relying on customers to come up with solutions is the company's responsibility.

Despite the popularity of the ideas-first approach, it is doomed to failure for three reasons. First, generating more ideas without understanding all the customer's needs does not increase the chances of finding the optimal solution. Second, the evaluation and filtering processes used today often miss great ideas and fail to filter out bad ideas. Third, customers cannot articulate the solutions they want because they are not experts in the field.

The needs-first approach is also flawed in execution because companies struggle to uncover all or most of the customer's needs. There is a lack of common language and agreement on what constitutes a customer need. Different departments within companies have opposing views on what a need is, making it difficult to have a complete and agreed-upon list of customer needs. The structure and content of a need statement are also not well-defined, leading to confusion and variability.

In summary, both the ideas-first and needs-first approaches to innovation have their flaws. The key to successful innovation lies in recognizing and correcting these flaws in execution.

## Jobs-to-be-done needs framework
The chapter introduced the concept of Jobs-to-be-Done Theory and its implications for understanding customer needs. It emphasizes the importance of knowing all the customer's needs and how it can transform decision-making, product development, and marketing strategies. 

The theory suggests that customers don't buy products but rather seek solutions to accomplish specific jobs or tasks. By categorizing and organizing customer needs and tying them to desired outcomes, companies can better identify market opportunities, evaluate concepts, and create customer value. 

It introduces the Jobs-to-be-Done Needs Framework, which encompasses various types of customer needs, such as functional, emotional, social, and financial desired outcomes. Understanding all these needs can be complex, with markets often having over 100 or even 200 needs in more complex industries.


## Jobs-to-be-done growth strategy matrix

## Outcome driven innovation

## Case studies

## Becoming an ODI practioner

## Transforming the organization

## The language of Jobs-to-be-done

## Learn more

