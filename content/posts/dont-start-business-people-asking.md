---
title: Don't start a business until people are asking you to
date: Wed, 13 Apr 2016 16:12:55 +0000
draft: false
contenttypes:
  - BlogPosting
lastmod: 2023-03-09T18:06:15.010Z
---

[Derek Sivers](https://twitter.com/sivers) just posted an article about starting a business that has some really great advice that while obvious is often overlooked. In a nutshell:

> Don't announce anything. Don't choose a name. Don't make a website. Don't build a system. You need to be free to completely change or ditch your idea.

Yet too often we get sucked into doing all of those things? Why? Because it feels like work. It's not though. It's just a form of procrastination. The real work, talking to prospects, is hard. It's also scary. Talking to prospects means coming to grips with the idea that an idea might be terrible. It's much easier to fool yourself into thinking you've started a business because you cobbled together a Wordpress site. Derek's right. Get paid, then you can start your business. Read the entire article over at [Sivers.org](https://sivers.org/asking)
