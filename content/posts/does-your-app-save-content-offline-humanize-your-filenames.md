---
title: Does your app save content offline? Humanize your filenames.
date: Sun, 27 Aug 2017 12:48:03 +0000
draft: false
contenttypes:
  - BlogPosting
lastmod: 2023-12-20T15:38:40.531Z
slug: humanize-your-filenames
images:
  - humanize-your-filenames.png
description: Web applications sometimes download filenames for offline use. Naming those files is an afterthought. Read how your should do it.
---

I am on a quest to help UX designers think about [humanizing their products](/posts/humanize-your-data/). In this post I'd like to talk about humanizing filenames when your application saves an invoice. Any application that bills it's customers should have a human readable file name. Your customers need those invoices when they submit expense reports. It's frustrating to have a folder full of poorly named files when you are trying to find a specific invoice. I'd like to use Buffer to demonstrate my point. Buffer is an awesome app that has a great user interface. It's the contrast between a normally great UI and it's in-human invoice file names that illustrates the problem.

## Download an Invoice

The download UI is below. When you click on either Download Invoice as PDF or Download Receipt as PDF a file is dropped into your download folder. ![UX issue with save file](/img/humanize-your-filenames-1.png)

Anyone see a problem here?

![](/img/humanize-your-filenames-2.png) The receipt filenames are woefully unhelpful. It's very obvious what is going on here. The developer who wrote this code probably had the requirement that each receipt filename should be unique. Come tax time I'm going to have a folder full of these poorly named files. I will either need to rename them all or manually check each file to see if it includes the correct months. It's not such a big deal with Buffer since my invoices are all the same amount. I would rip my hair out in frustration if I had to manually match different invoice amounts with the appropriate file. This is an easy problem to solve by adopting the following naming convention.

## Do this instead

_application - receipt - YEAR-MO - unique ID.pdf_

So, in the example above Buffer should name the files

```
buffer - receipt - 2017-01 - BHz7Z.pdf
buffer - receipt - 2017-02 - AMz7Z.pdf
buffer - receipt - 2017-03 - aLz7Z.pdf
buffer - receipt - 2017-04 - Ajz7Z.pdf
```

Humanizing information removes cognitive friction in your application. If you allow users to save files offline, make sure that the filenames are as useful to them as the rest of your product.
