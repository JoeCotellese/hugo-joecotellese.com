---
title: In sales? Mixmax is your personal assistant
date: Sat, 22 Dec 2018 14:08:46 +0000
draft: true
lastmod: 2023-03-09T18:04:36.052Z
---

In grade school, I was a Boy Scout. A rite of passage for all Boy Scouts is the day you get a pocketknife.

![](https://media.licdn.com/dms/image/C5112AQE1ka1aaGFlKw/article-inline_image-shrink_400_744/0?e=1550707200&v=beta&t=rrnetd0p8bRZkc1Kml-H5qzrYeTTXojmeqb2GyQXCFs)

I loved that pocketknife. Besides doing the obvious, cutting stuff, it also had a bunch of other cool attachments. It had a fork, spoon, toothpick, and a bottle opener because, you know, every ten-year-old needs to open a bottle of wine when he’s out hiking.

I was thinking about that pocketknife today as I was using [Mixmax](https://www.mixmax.com/).

Mixmax is Gmail’s Swiss Army knife. On the surface, Mixmax is a tool you can use to track email engagement. While that may be their leading feature it really does so much more.

I love this tool and use it every day. Rather than focus on every feature, I’m going to share with you two core use cases.

## Scheduling Meetings

As a co-founder of [ClipDish](https://getclipdish.com), I schedule a butt load of meetings. As everyone reading this has probably experienced, setting up a meeting time blows. It might take three or four exchanges to find the best possible time. The problem becomes exponentially worse when you try to schedule time with multiple people.

I don’t have that problem anymore. In fact, scheduling meetings is almost too easy now. If I’m scheduling one on one meetings, I simply block off “meeting times” in my calendar and share a “schedule” link with my recipients. They can pick the time that works best for them. If you’ve ever used the tool [Calend.ly](https://calend.ly/) it’s pretty similar. This has cut the back and forth time to nearly zero. There is the occasional times where my meeting blocks don’t work for someone but it’s pretty rare.

If I’m trying to schedule a meeting with a group I can just send out a meeting poll. I pick a few times that work for me, send it to the group and then recipients also pick the times that work best for them. When everyone votes the meeting is scheduled.

These features alone has saved me over an hour week.

## Cold Emailing

If you’re in sales, Mixmax is a killer tool for email outreach. Who doesn’t fear rejection? How many times have you sent an email and wondered if the recipient never opened or clicked on the message? With Mixmax, wonder no more! Mixmax analytics soothes you like mama after a nightmare.

Each message has a little lightning bolt indicator that lets you know if someone has looked at your email. It will also tell you how many times it’s been opened. This is great for follow-up. Someone with multiple opens might be more interested in a follow-up message from you.

![](https://media.licdn.com/dms/image/C5112AQHl7Yi8j6WFzQ/article-inline_image-shrink_1500_2232/0?e=1550707200&v=beta&t=5IcBlctxuO-i6FubV2wTo0x1g1tGLE_89DNYMg2XOj0)

A more robust dashboard gives you a view of your overall engagement.

![](https://media.licdn.com/dms/image/C5112AQEcLhgwH7S6IQ/article-inline_image-shrink_1500_2232/0?e=1550707200&v=beta&t=b7ryecz3eBW_ek7-6gTLAQGAbcG8Su3KaeZS2qhXSuU)

Email templates can also streamline your outreach process but, like Uncle Ben told Peter Parker – “With great power comes great responsibility.” Templates are a tempting way to just blast emails far and wide. I don’t recommend doing that. I have a couple of outreach templates that I customize for each recipient.

The best salespeople know that they need to follow-up or fail. This was advice I [read in a book years](https://www.amazon.com/dp/B00H6JBFOS/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1) ago. In practice, it is easy for me to forget to send those follow-up emails. Mixmax has my back again with reminders and automatic sequences.

When I send a message with Mixmax I can ask the tool to remind me to send a follow-up message. If I’m sending an important email that requires a response I will ask Mixmax to remind me. At the right time, Mixmax will drop the reminder into my inbox with an optional note. Who needs a personal assistant?

## Enrich Your Email

Mixmax replaces the existing Gmail compose window with their own. One of the cool little-hidden gems here is the “Enhance” pop up. When you press the “Enhance” button you get a pop up of elements you can add into a message. Youtube video previews, PDF previews, and call-to-action buttons really make your email stand out.

![](https://media.licdn.com/dms/image/C5112AQFTYWAbK3Q4Vg/article-inline_image-shrink_1500_2232/0?e=1550707200&v=beta&t=Q0Zg1T0iuqjy9ShBd_30Q3ldJ_WNks3qOCDuNLD_0EQ)

I use the call-to-action to add a “Schedule into my calendar” button in my cold email messages.

Enhancements like Dropbox and Google Drive integration makes sending attachments a breeze.

## Downsides?

The only are a couple of downsides to using Mixmax. It’s implemented as a Chrome Extension so if you use Safari or Firefox you’re out of luck. Also, another side effect of it implemented as a Chrome extension is it interferes with other Gmail Chrome extensions. If you’re using a tool like Full Contact you’ll have to choose which tool you prefer. This is not really Mixmax’s fault but rather a limitation in how you can extend Gmail via Chrome extensions.

Disabling other Chrome extensions is a small price to pay for the power that Mixmax gives you.

## What’s it cost

Mixmax is free if you are a light user. If you want to jump right in and use the more powerful features you’ll pay anywhere from $12 to $65 / month. I’m using the Small Business plan at $29 / month and it covers all of my needs.

They also have a referral program which unlocks some additional capabilities on the free tier if you get your friends to signup for the special link.
