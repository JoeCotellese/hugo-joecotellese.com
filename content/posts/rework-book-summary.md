---
title: Rework Book Summary
date: Thu, 22 May 2014 09:07:27 +0000
draft: false
description: The book Rework talks about the product development process at Basecamp. This is an outline of the book.
lastmod: 2024-03-11T18:19:10.920Z
tags:
  - books
---

I recently finished the excellent book [Rework](https://www.amazon.com/gp/product/B002MUAJ2A/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B002MUAJ2A&linkCode=as2&tag=clearstaticor-20&linkId=LDE2C2LUQXK7QATN) by the guys at 37 Signals. If you've read [**Getting Real**](https://basecamp.com/books/getting-real), most of this is old hat. However, if you are interested to see how their product development principals are applied to running a business. Check it out. Here's my outline.

## Chapter 1 - First

### The New Reality

- Ignore the real-world
  - people will continue to tell you why you can't do something. 37signals shows you that's a bad assumption
- Learning from mistakes is over rated
  - Harvard report showed that success breeds success, failures have same rate of failure as first time entrepreneurs
- planning is guessing
- Why grow?
  - you don't need a giant company
  - ramping up shouldn't be goal.
  - build the right size company for your business
- Workaholism
  - don't kill yourself working. You are less effective.
  - "Workaholics aren’t heroes. They don’t save the day, they just use it up. The real hero is already home because she figured out a faster way to [get things done](/posts/getting-things-done-book-summary/)."

## Chapter 2 - Takedowns

### Ignore the real world

- people in the "real world" are full of pessimism and despair
- assume society isn't ready for change
- don't believe it
- The real world isn’t a place, it’s an excuse. It’s a justification for not trying. It has nothing to do with you.

### Learning from mistakes is overrated

- other people's failures are there own
- their failure has nothing to do with you
- instead, learn from your successes.
  - it's your ammo
- failure isn't a prerequisite for success
  - people who fail have the same follow-up success as first timers
- success is what counts

### Planning is guessing

- Writing a plan makes you feel in control of things you don't control
- THey are really just guesses
- Plans let the past drive the future and can put blinders on you
- They don't let you improvise
- If you write a big plan you'll never look at it anyway.
- Make decisions right before you do something
- Working without a plan may seem scary. But blindly following a plan that has no relationship with reality is even scarier.

### Why grow?

- Maybe the right size for your company is 5 or 40 people
- grow slow
- see what feels right
- small businesses wish they were bigger but ...
- big businesses wish they were more agile
- once you get big it's hard to shrink without damaging morale
- Don’t be insecure about aiming to be a small business. Anyone who runs a business that’s sustainable and profitable, whether it’s big or small, should be proud.

### Workaholism

- Burning the midnight oil is stupid
- workaholics can create more problems then they solve
- they aren't heroes. They don't save the day. The real hero is home early because she figured out a faster way to get things done.

### Enough with "entrepreneurs"

- Everyone should be encouraged to start his own business.
- replace entrepreneur with "starter"
- all you need is an idea, self-confidence and a push

## Chapter 3 - Go

### Make a dent in the universe

- To do great work, you need to feel that you’re making a difference.'
- Don’t sit around and wait for someone else to make the change you want to see.
- If you’re going to do something, do something that matters.

### Scratch your own itch

- The easiest, most straightforward way to create a great product or service is to make something you want to use.
- If you’re solving someone else’s problem, you’re constantly stabbing in the dark.
- When you build what you need, you can also assess the quality of what you make quickly and directly, instead of by proxy.

### Start making something

- What you do is what matters, not what you think or say or plan.
- Until you actually start making something, your brilliant idea is just that, an idea. And everyone’s got one of those.
- the most important thing is to begin
- Ideas are cheap and plentiful
  - the original pitch is tiny.
  - real question is in execution

### No time is on excuse

- When you want something bad enough, you make the time— regardless of your other obligations.

### Draw a line in the sand

- Great businesses have a point of view, not just a product or service.
- If no one’s upset by what you’re saying, you’re probably not pushing hard enough. (And you’re probably boring, too.)
- If you stand for something decisions are obvious
  - if you don't have an ethos everything becomes an argument.
- Examples - whole foods, high quality natural organic products. No debate over Snickers or Coke

### Mission statement impossible

- Big difference between a mission statement and truly standing for something
  - one is a piece of paper
  - one is something you do everyday
  - belive it and live it

### Outside money is Plan Z

- No matter what type of business you are building take as little cash as possible. If you take cash you...
  - Give up control
  - think about cashing out
  - spend out of control
  - generally get screwed
  - stop thinking about customers, instead think about investors
  - are distracted by raising money

### You don't need as much as you think

- work under constraints
- It's ok to be frugal

### Start a business, not a startup

- every business is governed by same market rules
  - revenue in, expenses out, make money or go home
- think about how you are going to make a profit right away
  - don't defer profits for later
    - that's a hobby
- Act like a business, it will help you succeed

### Building to flip is building to flop

- You need a commitment strategy, not an exit strategy. Stop talking about exit strategies
- Don't be the flipper, if you have something good going, keep it going

### Less mass

- embrace the idea of being smaller
- keeping small lets you change easily
- the more expensive it is to change, the less likely you will make the change

## Chapter 4 - Progress

### Embrace Constraints

- get creative with the constraints you have.
- You can make a lot with a little
- 37signals -
  - only one or two people working on project at a time
  - keep features to a minimum
  - keeps down bloat

### Build half a product, not a half-assed product

- you can turn a bunch of great ideas into a crappy product
  - don't try to do it all at once
  - sacrifice things for the greater good
- cut out the stuff that's merely "good"

### Start at the epicenter

- There’s the stuff you could do, the stuff you want to do, and the stuff you have to do. The stuff you have to do is where you should begin. Start at the epicenter.
- How do you find the epicenter? Ask yourself "If I took this think away would I still have something to sell?"

### Ignore the details early on

- details are important
- don't focus on them too early
  - disagreement / meetings / delays
  - get lost in things that don't really matter
- Low rez prototypes

### Making the call is making progress

- Try to swap "Let's think about it." with "Let's do..."
  - force yourself to make decisions
  - don't wait for the perfect solution
- your decisions don't need to last forever
- Long projects zap morale. The longer it takes to develop, the less likely it is to launch. Make the call, make progress, and get something out now— while you’ve got the motivation and momentum to do so.

### Be a curator

- the stuff you leave out is what matters
- look at how you can pare things down
- Stick to what’s truly essential. Pare things down until you’re left with only the most important stuff. Then do it again. You can always add stuff back in later if you need to.

### Throw less at the problem

- Trim down then polish what's left
- stop pushing back deadlines and increasing your budget

### Focus on what won't change

- don't be a company that chases the next big thing
- core of business is built around things that don't change
  - invest in those
- When you focus on permanent features, you’re in bed with things that never go out of style.

### Tone is in your fingers

- don't focus on your tools
- your equipment is a crutch
- In business, too many people obsess over tools , software tricks, scaling issues, fancy office space, lavish furniture, and other frivolities instead of what really matters. And what really matters is how to actually get customers and make money.

### Sell your by-products

- When you make something you end up also making something else.
- everything has a by-product
- try to find these and look for opportunities to monetize them

### Launch now

- Once your product does what it needs to do get it out there.
- If you had to launch your biz in two weeks what would you cut out?
- Don’t mistake this approach for skimping on quality, either. You still want to make something great. This approach just recognizes that the best way to get there is through iterations. Stop imagining what’s going to work. Find out for real.

## Chapter 5 - Productivity

### Illusions of agreement

- don't over document. It's an illusion
- If you need to explain something, get real
  - remove layers of abstraction

### Reasons to Quit

- Ask questions to determine if what you're doing matters
  - Why are you doing this?
  - What problem are you solving
    - make sure you are not solving an imaginary problem
  - Is it actually useful
    - don't mix enthusiasm with usefulness
  - Are you adding value?
    - adding something is easy. adding value is hard
  - Will this change behavior
  - Is there an easier way?
    - problems are usually simple, we just make them hard
  - What could you be doing instead?
  - Is it really worth it?
- sometimes abandoning something is the best thing to do. Don't throw good time after bad work.

### Interruption is the enemy of productivity

- Interruptions are slowing you down
- it's not collaboration, it's just an interruption
- they breakup work day into series of "work moments"
- fight back from interruptions

### Meetings are toxic

- an hour meeting with 5 people is 5 hours of lost productivity
  - is it ever worth it?
- Est. rules if you are having a meeting
  - Set a timer and end meetings on time.
  - limit your invites
  - have a clear agenda
  - begin with a specific problem
  - go to the problem, not to a conference room
  - end with a solution

### Good enough is fine

- Find judo solutions to hard problems.
  - maximum efficiency with minimum effort
- If good enough gets the job done, go for it.
- better than wasting resources
- you can't afford complex solutions
- you can turn good enough to great later

### Quick wins

- Momentum fuels motivation
- Every improvement you make gives you a jolt of momentum
- longer it takes to do something less likely you'll finish it
- stuck on a long term project? Give yourself some time each week to some small victories
- What can we do in two weeks?
  - quicker it's in the hands of the customer the better off you'll be

### Don't be a hero

- Sometimes you start something it takes longer than it should
- you feel bad and lock yourself away and kill yourself to get it done.
- was it really worth it?
  - you originally budgeted X and it took Y what else could you do in Y?
- If anything takes longer than two weeks bring in someone else to take a look
- best answer to problem might be to quit doing something

### Go to sleep

- there are costs to pulling an all-nighter
  - you become stubborn
  - lack creativity
  - low morale
  - irritability
- don't be a masochist, get some sleep

### Your estimates suck

- we always see everything in best case scenarios
- estimates that stretch weeks, months, and years into the future are fantasies.
- break big things into smaller things
- better to have it be a small project that’s a couple weeks over rather than a long one that’s a couple months over.

### Long lists don't get done

- make smaller todo lists
- divide problems into smaller and smaller pieces
  - find what you can deal with quickly
  - prioritize visually, most important things on top

### Make tiny decisions

- big decisions are hard to make and change
- make choices that are small enough that they are temporary
- you can still think big just get there with one tiny decision at a time
- always strive for attainable goals

## Chapter 6 - Competitors

### Don't copy

- copying in business area is dumb
- you can't build a business being a copycat
  - it skips the understanding part
  - that's how you grow
- if you copy, you can't keep up
- you're always playing catch-up
- being influenced is ok
- you don't see the entire picture when you copy stuff
  - why do things work the way they do.

### Decommoditize your product

- you're going to be copied
- make _you_ part of your product or service
  - Zappos example
- You should be felt in all parts of your product
  - sales, service, explaining it, delivering it
  - can't copy you

### Pick a fight

- If your competitor sucks, say so.
  - people will agree with you and rally behind you
  - be the anti-\_\_\_
  - differentiate yourself and attract followers
- get a target in your sights

### Underdo your competition

- Normal thinking is beat competitors by one-uping them
- it keeps you on the defensive
  - you can't think ahead then, only behind
- Do less instead to beat them
- Solve simple problems really well
- leave the hard stuff to the competition
- try one-downing
- try underdoing
- Don’t shy away from the fact that your product or service does less.
- Highlight it. Be proud of it.
- Sell it as aggressively as competitors sell their extensive feature lists.

### Who cares what they are doing?

- Focus on yourself, not your competition
- What's going on in here, more important than out there
- if you worry about others you can't improve yourself
- Don't allow your competition to define your parameters
  - you can't out-Apple Apple

## Chapter 7 - Evolution

### Say no by default

- get into that habit
  - even to some of your best ideas
  - use it to get priorities straight
  - rarely regret saying no
  - often regret saying yes
- don't avoid confrontation
- customer isn't always right
- making a view vocal customers happy isn't worth wrecking product for everyone else
- make sure product stays right for you.
  - you'll love it because I do

### Let your customers outgrow you

- if you stick with your current customers you cut off new ones
- it becomes tailored to the current customer
- stops appealing to fresh blood
- that's how your company dies
- Scaring away new customers is worse than losing old customers
- you'll probably end up with a basic product. That's ok
- small simple needs are constant
- endless supply of customers who need that
- Be true to a type of customer more than an individual customer with changing needs

### Enthusiasm != Priority

- enthusiasm you have for a new idea is not an indicator of its true worth
- Must have's get downgraded to nice to have with the passage of time
- you chase ideas and you never get anywhere
- let your ideas cool off a bit
  - write them down and revisit them
  - evaluate their priority with a calm mind

### Be at home good

- don't be the product that only is good in the store
- be the product that's good at home
  - you're more impressed with it then at the store
  - the more you use it, the more you like it
  - can't paint over bad experience with good advertising or marketing

### Don't write it down

- How should you keep track of what customers want?
  - Don't
  - Listen but then forget what they say
- The only requests that really matter are the ones that come up over and over again
- you won't be able to forget them because they keep reminding you
  - they show you what things you really need to worry about
- If you keep forgetting a request it's not that important

## Chapter 8 - Promotion

### Welcome Obscurity

- obscurity is good
- make mistakes without everyone watching you
- try new things
- who cares if you mess up
- Once you get bigger you'll take fewer risks

### Build an audience

- Speak, write, blog, tweet, make videos.
- Share information that's useful
- it will help build an audience

### Don't outspend / out teach

- Can't compete on marketing dollars
- If you teach someone something you'll build a better bond
- earn loyalty by teaching
- they'll trust you more
- they'll respect you
  - even if they don't use your stuff

### Emulate Chefs

- Share everything you know
- This might go against thinking
- Don't be paranoid and secretive
- Cooks write cookbooks. What can you do?

### Go behind the scenes

- Show people how your business works
- it builds a relationship with them
- better appreciation for what you do

### No one likes plastic flowers

- Don't fear your flaws
- There is beauty to imperfection
  - Wabi-sabi - character and uniqueness over shiny facade
    - scratches and cracks are ok

### Press releases are spam

- Don't do a generic pitch
- Press releases are boring, they don't get people interested in writing about you
- don't do what everyone else does
- pick up phone and call someone
- write them a note
- pitch with passion and life
- be unforgettable

### Fuhgettabout the Wall Street Journal

- Big trades aren't that important
- they are nice but don't result in instant activity
- focus on trade pubs, niche outlets

### Emulate Drug Dealers

- Make your product addictive.
- give them a free taste and make them want to come back with cash
- don't be afraid to give something away.
  - but you need something to sell

### Marketing is not a department

- It's the sum of everything you do
- 24/7/365

### The myth of the overnight sensation

- you won't get rich quick
- no one cares about you
- trade the dream of overnight success for slow growth
- it's hard but be patient
- start building your audience today.
- get people interested in what you have to say

## Chapter 9 - Hiring

### DIY First

- don't hire someone until you've tried to do it yourself first
  - you understand the job better
  - you know what a good job looks like
  - you will be a better manager
- try learning first
- you need to be intimately involved in your entire business

### Hire when it hurts

- don't replace lost employees immediately
- see how long you can get by w/o him
- might not need as many people as you think
- hire when there is more work than you can handle for a sustained period of time

### Pass on great people

- Don't just hire someone because they are great.
  - if you don't need them
- don't have people one staff who aren't doing anything

### Strangers at a cocktail party

- Don't hire too fast
- they don't work well together initially
- like strangers at a cocktail party, they're too polite to each other
  - no one says "This idea sucks"
- you need to be able to tell each other when ideas are full of crap
- safe environment to be honest when things get tough

### Resumes are ridiculous

- resumes are a joke
- exaggerations
- filled with fluff that don't mean anything
- you want specific person who cares about company, product, customers, job
- Look at the cover letter
  - trust your gut in the letter. first paragraph should hook you in.

### Years of irrelevance

- little skill difference between six months of experience and six years.
- real difference is dedication, personality, intelligence
- not how long but how well someone is doing something

### GPAs don't matter

- Not everyone tests well
- too much time in academia can be harmful
- pool of great talent is much bigger than people who completed college with great GPAs
  - dropouts
  - community college
  - just finished high-school

### Everybody works

- small team, everyone works,
- not just delegate work
- everyone's producing

### Hire managers of one

- They come up with own goals and execute on them
- don't need much direction
- free you from oversight
- find someone who is capable of building something from scratch and seeing it through

### Hire great writers

- If you're deciding between candidates, hire the best writer
- being a good writer is more than just writing
- clear writing = clear thinking
- great writers know how to communicate
- make things easy to understand
- they know what to omit

### The best are everywhere

- Embrace remote work
- have time overlap a few hours a day
- meet in person once in a while
- geography doesn't matter, hire the best talent

### Test drive employees

- interviews only tell one picture
- hire them for mini-project
  - twenty / forty hours
  - see how they make decisions
- truth comes out in real work environment.

## Chapter 10 - Damage Control

### Own Bad News

- if something goes wrong people will talk about it
  - should be you.
- if something bad happens tell your customers
- people respect you if you are honest.
- make sure your customers are as informed as possible.
- How to share the story?
  - message from the top
  - spread it far and wide
  - always have a comment
  - say your sorry like a real person
  - be really empathetic

### Speed changes everything

- get back to people quickly. esp in customer service
- try not to use canned answers
- even if you don't know, tell them and get back to them.

### Know how to say you are sorry

- use language that conveys you really understand how much they are inconvenienced.
- no canned sorry message. they don't ring true
- think about how you would feel if the apology was directed at you.
- Would you believe yourself?
- no apology will help you if your customers don't trust you.

### Put everyone on the front lines

- big difference between working in kitchen and dealing with customers
- don't split your front and back house
- everyone should be listening to customers
- try having everyone connect with customers a few times a year
- no one should be shielded from direct criticism
- Look at Craigslist founder

### Take a deep breath

- make a change and people will bitch
- resist urge to panic or make rapid changes
- people are creatures of habit
- make decisions you believe in even if it's unpopular at first.
- negative reactions are always louder than positive ones.
- when people complain, let them know that you are listening.

## Chapter 11 - Culture

### You can't create culture

- Culture isn't foosball or trust falls.
- It's action, not words
- it needs time to develop

### Decisions are temporary

- Things aren't problems until they are real problems
- your decisions aren't set in stone
- if circumstances change revisit your decisions
- optimize for now
- being small makes it easy to change course

### Skip the rockstars

- create a rockstar environment instead
- good work environment breeds great work
- rock star environment comes from trust, autonomy and responsibility.
- people should have
  - privacy
  - workspace
  - the right tools

### They're not 13

- when you treat people like children, you get children's work
- people need diversions
- failing to trust employees is expensive.

### Send people home at 5

- You don't need more hours, you need better hours
- if they have things to do at home they use their time wisely
- they get their shit done
- you want busy people who have a life outside of work

### Don't scar on the first cut

- Keep your policies to a minimum
- Policies are collective punishment for the misdeeds of an individual
- No one sets out to create a bureaucracy. It sneaks up slowly.

### Sound like you

- Small companies try to sound big, it comes off as a joke.
- It's ok to sound small
- Talk to your customers the way you talk to friends
- Read out loud what you write
- Think about one person while you are writing it.

### 4 Letter Words

- Avoid need, must, can't, easy, just, only and fast.
- Need - very few things need to get done.
- Can't - yeah, you probably can
- Easy - don't describe someone's job as easy. You don't ever describe your job that way.
- Try to avoid words that box people into a corner

### ASAP is poison

- stop saying it
- everyone knows the stuff needs to get done as soon as possible
- reserve your use of emergency language for true emergencies

## Chapter 12 - Conclusion

### Inspiration is perishable

- we all have ideas - they last forever
- inspiration doesn't
- if you're inspired on Friday, dive right in
  - you can get a lot done in a little time
- If inspiration grabs you, grab it back and put it to work.

## Your Turn

I hope you enjoyed my book summary and outline of Rework. If you want to be notified when I've created a new book summary, [join our email list](https://mailsrv.getclipdish.com/subscription?f=EeZoV6892LXhcY892763exC763cYdoDgoAf0bUVbz1mDs4XmCTDHGoP7l8EIVsHzShlA8FUG).

## More Book Summaries

You can find more of my book outlines & summaries by [visiting this page.](/tags/books/)