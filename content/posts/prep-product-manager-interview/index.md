---
title: How to Be the Product Manager Every Company Wants to Hire
date: Thu, 21 Jul 2016 21:33:38 +0000
draft: false
lastmod: 2024-02-04T14:38:49.314Z
images:
    - be-prepared.png
---
![](south-park-be-prepared.gif)

My daughter once decided she was going to try out for the school musical. I asked her if she was going to rehearse before hand.

"Nah, I'm good." she replied.

Fast-forward two days later she gets in the car crying because she was asked to sing a song during the audition. Needless to say she didn't get the part. Hopefully she leared the value of preparation.

It's not the school play but landing the product management job of your dreams requires the same level of preparation.

You must find a way to stand out from a crowded field. And in this post, I'm going to show you how I prepare for job interviews.

## The Power of Preparation

Many years ago, I interviewed a software engineer. I asked a question about assembly language and the candidate, clearly nervous, said to me "If you're looking for a world class engineer, I'm not your guy." 

Another time, I had a candidate sit in silence for over five minutes thinking about the answer to a question.

I am constantly shocked at the number of candidates who come in unprepared, lacking basic knowledge about the company or the role. 

If you're aiming for a Product Manager role and are eager to prepare but unsure where to start, I've got you covered. It's not just about rehearsing answers to potential questions; it's about crafting responses that resonate specifically with the company’s needs and ethos. Let’s break down this winning strategy:

Transforming your approach to job interviews, especially for a coveted Product Manager position, requires deep and strategic preparation. Like my daughter, success hinges on how well you prepare beforehand.

Here's how I think about the process, follow along and be better prepared for your interview.

### 1. Discover the Company's Core

**Understanding the company:** Begin by immersing yourself in the company’s history, mission, and culture. Dive into their website, read their blog posts, and study their social media presence.

**Questions to Consider:**

- What values does the company champion? Sometimes you can find a values page on the website.
- How does its mission align with your personal career goals?
- Can you identify any recent achievements that resonate with you?

**Advice:** Try to frame your responses so they highlight how your personal values and achievements align with the company's ethos. This demonstrates not just familiarity but a deep connection with their core principles.

### 2. Know Your Interviewers

**Researching the Panel:** Utilize LinkedIn and other professional networks to learn about your interviewers’ backgrounds. Understand their role within the company, their professional journey, and if available, their interests or work philosophy.

If they write about product management, on LinkedIn or a blog, you should know about it.

**Questions to Consider:**

- What projects have they led or contributed to?
- Do they have a specific management style or professional principle they adhere to?
- Can you find common ground or interests that might help establish rapport?

**Advice:** Prepare thoughtful questions or comments that reference their work or interests. This shows respect for their accomplishments and indicates your proactive nature.

### 3. Deep Dive into the Product

**Understanding the Offering:** Gain a comprehensive understanding of the product(s) you’ll potentially manage. Use the product yourself if possible, read user reviews, and analyze its features and benefits.

You don't even have to spend money on the product. Search Youtube and find videos of other people using it.

**Questions to Consider:**

- What unique value does the product offer its users?
- How does it stand out from competitors?
- Are there any user feedback or features that resonate with you personally?

**Advice:** Be ready to discuss specific aspects of the product that excite you or where you see potential for growth or improvement. Your insights should reflect a balance of user empathy and strategic thinking.

### 4. Analyze the Competition

**Gaining a Competitive Edge:** Understand the broader market and the company's position within it. Identify key competitors, their strategies, and how they compare to the company's offerings.

**Questions to Consider:**

- What trends are shaping the industry?
- How does the company differentiate itself from its competitors?
- Are there any threats or opportunities on the horizon?

**Advice:** Offer thoughtful analysis on how you could leverage or defend against these market dynamics. This demonstrates not just industry awareness but strategic foresight.

### 5. Connect the Dots

**Aligning with the Role:** Reflect on everything you’ve learned and tie it back to the specific responsibilities and requirements of the job description.

**Questions to Consider:**

- How do your skills and experiences uniquely position you to excel in this role?
- Can you provide examples of how you've tackled similar challenges in the past?
- How can your insights into the company, its product, and the market drive success in this role?

**Advice:** Prepare to articulate a compelling narrative that connects your background, your understanding of the company and its product, and your vision for contributing to its success. This approach not only showcases your preparedness but also your enthusiasm and strategic alignment with the role.

## Conclusion

By diving deep into these areas and integrating your findings into your interview strategy, you elevate yourself from a candidate to a contender. It’s this nuanced preparation that can transform your interview into a meaningful dialogue, setting the stage for a successful journey with the company. Ready to stand out? Embrace these strategies and step into your Product Manager interview with confidence and insight.

If you need help preparing for an interview or if you're getting ready to hire product managers and need help, [let me know](/services).
