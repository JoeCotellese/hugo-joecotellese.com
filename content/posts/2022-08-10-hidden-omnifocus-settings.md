---
title: Hidden Omnifocus Settings
description: The definitive list of hidden preferences in Omnifocus.
date: 2022-08-10T13:12:33.903Z
preview: ""
draft: ""
tags: ""
categories: ""
lastmod: 2022-09-15T13:47:54.826Z
keywords:
  - omnifocus
---
I'm a long time Omnifocus user but only recently discovered that there are hidden settings you can tweak to further customize how Omnifocus works.

I could not find a definitive list that wasn't out dated. I did a little spelunking through the Omnifocus application bundle and found them all.
<!--more-->
I don't know how all of these work so you **use them at your own risk.**

If you find out how they work, feel free to shoot me an [email](/contact-me/) describing what you've learned.

If you're not familiar with the layout of a PLIST file, look at <key> for the name of the preference and the field immediately after shows the default value.

## Changing Preferences

You can change these by opening the following 

    omnifocus:///change-preference?KEY=VALUE

For example:

    omnifocus:///change-preference?AutomaticBackupLimit=5

If you want to go back to the default remove the value.

    omnifocus:///change-preference?AutomaticBackupLimit


When you open that URL, you should see a dialog like the following in Omnifocus.

![Omnifocus Settings Modal](/img/omnifocus-change-hidden-preferences.png)


    omnifocus:///change-preference?AutomaticBackupLimit


## Preference list

```

<dict>
    <key>AllowInspectionOfSidebarObjects</key>
    <true/>
    <key>AutoCloseTemporaryPerspectives</key>
    <true/>
    <key>AutomaticBackupFolder</key>
    <string>~/Library/Application Support/OmniFocus/Backups</string>
    <key>AutomaticBackupLimit</key>
    <integer>100</integer>
    <key>AutomaticBackupMinutes</key>
    <integer>120</integer>
    <key>AutomaticallyHideCompletedActionsDelay</key>
    <real>0.25</real>
    <key>BuiltinPerspectiveShortcuts</key>
    <dict/>
    <key>ClippingTaskTitleSummaryLength</key>
    <integer>140</integer>
    <key>ClippingsGoToQuickEntry</key>
    <true/>
    <key>ClippingsIgnoreAttachments</key>
    <false/>
    <key>ClippingsIgnoreEntireEmailMessages</key>
    <true/>
    <key>ClippingsIgnoreTextBackgroundColor</key>
    <true/>
    <key>ClippingsIgnoreTextForegroundColor</key>
    <true/>
    <key>ClippingsSimplifyStyle</key>
    <false/>
    <key>ContentOutlinePreservesVisibleSelection</key>
    <true/>
    <key>ContentOutlinePreservesVisibleSelection:ByID:ProcessForecast.v2</key>
    <false/>
    <key>CopyAsTaskPaper</key>
    <false/>
    <key>CustomColumnsLayoutEstimatedDurationWidth</key>
    <integer>58</integer>
    <key>CustomColumnsLayoutMaximumWidthTags</key>
    <integer>180</integer>
    <key>CustomColumnsLayoutMinimumProjectWidth</key>
    <integer>72</integer>
    <key>CustomColumnsLayoutMinimumTitleWidth</key>
    <integer>110</integer>
    <key>CustomColumnsLayoutMinimumWidthTags</key>
    <integer>80</integer>
    <key>CustomColumnsRatioOfMaximumColumnWidthToMinimum</key>
    <integer>3</integer>
    <key>DataStoreProbesList</key>
    <array/>
    <key>DebugOpenURL</key>
    <false/>
    <key>DelayIntegrationOfSyncedChangesWhenUserIsActive</key>
    <true/>
    <key>ExpandRowsByDefault</key>
    <false/>
    <key>ExtraColumns</key>
    <array/>
    <key>FeedbackAddress</key>
    <string>omnifocus@omnigroup.com</string>
    <key>ForecastBlessedTagPrimaryKey</key>
    <string></string>
    <key>ForecastCalendarCollapsed</key>
    <false/>
    <key>ForecastDragSetsDeferDate</key>
    <false/>
    <key>ForecastEventTimelineIncludeOffHoursEvents</key>
    <true/>
    <key>ForecastIncludesInboxEvenWhenFocused</key>
    <false/>
    <key>ForecastIncludesProjectsOnHold</key>
    <true/>
    <key>ForecastShowDeferredItemsSection</key>
    <false/>
    <key>ForecastShowEventTimeline</key>
    <true/>
    <key>ForecastTimelineEndHour</key>
    <integer>17</integer>
    <key>ForecastTimelineStartHour</key>
    <integer>9</integer>
    <key>ForumPageURL</key>
    <string>http://www.omnigroup.com/forward/omnifocusforums</string>
    <key>HiddenForecastCalendarIdentifiers</key>
    <array/>
    <key>InsideOmniFocusPageURL</key>
    <string>http://inside.omnifocus.com</string>
    <key>KinklessImportPath</key>
    <string></string>
    <key>KinklessImportShouldIncludeArchive</key>
    <true/>
    <key>LastArchiveRequestTimeInterval</key>
    <real>0.0</real>
    <key>LastExportDirectory</key>
    <string>~/Desktop</string>
    <key>LastExportType</key>
    <string>HTML</string>
    <key>LastWindowPosition</key>
    <string></string>
    <key>NSShouldActivateForServiceRequest</key>
    <false/>
    <key>NotificationsDebugLogger</key>
    <integer>0</integer>
    <key>OFIAutomaticDashSubstitutionEnabled</key>
    <true/>
    <key>OFIAutomaticDataDetectionEnabled</key>
    <false/>
    <key>OFIAutomaticLinkDetectionEnabled</key>
    <false/>
    <key>OFIAutomaticQuoteSubstitutionEnabled</key>
    <true/>
    <key>OFIAutomaticSpellingCorrectionEnabled</key>
    <true/>
    <key>OFIAutomaticTextReplacementEnabled</key>
    <true/>
    <key>OFIColorApproximatelyGrayThreshold</key>
    <real>0.10000000000000001</real>
    <key>OFIColorDarkLowContrastThreshold</key>
    <real>0.40000000000000002</real>
    <key>OFIColorLightLowContrastThreshold</key>
    <real>0.80000000000000004</real>
    <key>OFIContinuousSpellCheckingEnabled</key>
    <false/>
    <key>OFIGrammarCheckingEnabled</key>
    <false/>
    <key>OFIMaximumNumberOfTitleLines</key>
    <integer>-1</integer>
    <key>OFINoteLayoutManagerReplacesLowContractColors</key>
    <true/>
    <key>OFISmartInsertDeleteEnabled</key>
    <true/>
    <key>OFMRemindersImporterLogger</key>
    <integer>0</integer>
    <key>OIInspectorDefaultTopLeftPosition</key>
    <string>{900, 53}</string>
    <key>OJSEnabled</key>
    <true/>
    <key>OOReturnInRowInsertsNewline</key>
    <false/>
    <key>OOShouldContinueDeletionAcrossCellBoundariesOnKeyRepeat</key>
    <false/>
    <key>OOShouldEditNotesOnEnterKey</key>
    <false/>
    <key>OOSplitWhenCreatingItem</key>
    <false/>
    <key>OOTabShouldNavigateCells</key>
    <true/>
    <key>OSUseFontLeading</key>
    <false/>
    <key>OZCallbackURLHandler</key>
    <string>com.omnigroup.omnifocus3</string>
    <key>OZShouldSuppressTitleBarWarning</key>
    <true/>
    <key>PaidFeedbackAddress</key>
    <string>omnifocus@omnigroup.com</string>
    <key>PerspectiveControlHidden</key>
    <false/>
    <key>PerspectiveSnapshotSound</key>
    <string>Snapshot</string>
    <key>PrelaunchQuickEntry</key>
    <true/>
    <key>ProductPageURL</key>
    <string>http://www.omnigroup.com/applications/omnifocus/</string>
    <key>QuickEntryAnimationDuration</key>
    <real>0.10000000000000001</real>
    <key>QuickEntryItemsGoToInbox</key>
    <false/>
    <key>QuickEntryLayoutMode</key>
    <string>fluid</string>
    <key>QuickEntryMultiEntryMode</key>
    <false/>
    <key>QuickEntryVisibleColumnNames</key>
    <array>
        <string>projectInfo</string>
        <string>context</string>
        <string>dateToStart</string>
        <string>dateDue</string>
        <string>noteExists</string>
        <string>flagged</string>
    </array>
    <key>QuickOpenAnimationDuration</key>
    <real>0.10000000000000001</real>
    <key>QuickOpenEnabled</key>
    <true/>
    <key>QuiescenceDelayedIntegrationLogger</key>
    <integer>0</integer>
    <key>QuiescenceTimerDuration</key>
    <real>10</real>
    <key>RestorableState</key>
    <dict/>
    <key>ShouldOpenLinksInNewWindows</key>
    <false/>
    <key>ShouldQuickOpenRevealInNewWindows</key>
    <false/>
    <key>SimpifyStyleWhenPastingInNotes</key>
    <false/>
    <key>StrikeCompletedActions</key>
    <true/>
    <key>SyncAutomaticallyOnLaunch</key>
    <true/>
    <key>SyncAutomaticallyOnQuit</key>
    <true/>
    <key>SyncDebug</key>
    <true/>
    <key>SyncFinishedSound</key>
    <string>Purr</string>
    <key>SyncShouldPublishDueDateTimes</key>
    <false/>
    <key>TitleTextFoldingForColumns</key>
    <true/>
    <key>TitleTextFoldingForFluid</key>
    <false/>
    <key>XMLTransactionGraphShouldEmailProblems</key>
    <true/>
</dict>
```