---
title: The Rabbit Hole
date: Mon, 22 Oct 2018 22:35:22 +0000
draft: false
lastmod: 2023-03-09T18:03:49.385Z
---

Distractions are a bitch. I'm writing this as the last step in a hole that I found myself in. It went something like this.

1.  Wanted to work on a checklist for my morning routine.
2.  Had to decide best tool to create my morning routine in.
3.  Realized I already had it in a text file. Opened the text file.
4.  Made some updates to the existing checklist.
5.  Thought “Hmm. Doesn’t Tim Ferriss have a process he follows every morning?”
6.  Dug into [Tools of Titans.](https://amzn.to/2rGyIBy)
7.  Found a quote and thought. Hmm. I should put this quote on my blog.
8.  Went to said blog.
9.  Logged in and realized “Oh, I need to update my version of Wordpress”
10. Realized the rabbit hole I just got myself into and decided to jot it down.

This all happened over the span of 12 minutes. I've been meditating a lot lately using Headspace. The one thing that Andy continues to say is if you feel distracted, make note of it and bring yourself back to the breath. So, noted, breathing, back to work...
