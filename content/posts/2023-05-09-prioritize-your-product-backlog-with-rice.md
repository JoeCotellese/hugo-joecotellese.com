---
title: How to cook up a prioritized product backlog with RICE
description: "The RICE Framework is an efficient method used by product managers to prioritize features and ideas. With it you can objectively evaluate each feature and make data-driven decisions. Make sure your team is working on the right stuff."

date: 2023-05-09T11:16:00.266Z
preview: ""
slug: rice-framework-product-prioritization-how-to
draft: false
tags: []
categories: []
lastmod: 2023-05-09T17:27:17.872Z
---
---
**Key Ideas**

- The RICE framework is a popular prioritization method used by product managers to evaluate features and projects and make data-driven decisions about which ones to prioritize.
- RICE stands for Reach, Impact, Confidence, and Effort. Each factor is given a score and then plugged into a formula to create a final RICE score.
- By using RICE, product managers can objectively evaluate each feature or project and prioritize the ones that will have the most significant impact on the business and users.

---
As a parent of four kids, meal planning is a critical part of my weekly routine. Each of my kids has their own favorite meals, and it's hard to keep everyone happy while still maintaining a healthy and varied diet. One kid could eat mac and cheese every night, while another insists on a steady diet of chicken nuggets. It's like trying to prioritize a product backlog when everyone has their own feature requests! 

![Kid eating oatmeal and spitting it out](/img/christmas_story_eating.gif)

Now, like a chef planning a meal, you have to decide how to prioritize one feature over another. Do you give in to your kid's request for mac and cheese every night or make them try something new? Similarly, as a product manager, you have to choose which features to prioritize in your product backlog. This is where [prioritization frameworks](/posts/product-management-prioritization-guide/) come into play, making the process much easier and efficient. Today, we're going to talk about RICE (the framework, not the grain), a popular method used by many product managers to prioritize features and ideas. With RICE, you'll be able to make informed decisions, ensuring that the features you work on will have the most impact on your product's success. So, let's get cooking!

## What is RICE?

RICE is a popular prioritization framework used by product managers to determine which features or projects should be worked on first. It stands for Reach, Impact, Confidence, and Effort. Each factor is given a score, the scores are then plugged into a simple formula to create a final RICE score. The feature or project with the highest RICE score is the one that should be prioritized.

---
**Follow along**

If you want to see how I prioritize a backlog using real world data from the app ClipDish, [create an account at Airtable](https://airtable.com/invite/r/Nvjt8ERR) and copy this [shared base.](https://airtable.com/shrmEIVOmWWLXTPNw)

{{< airtable url="https://airtable.com/embed/shr99crVeTDx6lPMb" >}}

---

Let's break down each factor in more detail:

- Reach: This factor measures how many people will be affected by the feature or project. The more people it will reach, the higher the score.
- Impact: This factor measures the potential impact the feature or project will have on the users or the business. The higher the impact, the higher the score.
- Confidence: This factor measures how confident the product manager is in their estimates of reach and impact. If there is high confidence in the estimates, the score will be higher.
- Effort: This factor measures the amount of work required to implement the feature or project. The less effort required, the higher the score.

By using RICE, product managers can objectively evaluate each feature or project and make data-driven decisions about which ones to prioritize. This can help ensure that the product team is working on the features that will have the biggest impact on the business and the users.

## What is Reach and how do I calculate it?

How many times has the CEO of some other executive come to you with a feature idea. You want to be accomidating but your gut tells you other features are more important. Reach can help you effectively put the request into perspective.

Use Reach to get the bias out your backlog. For a given feature idea, look at your data and analytics and get a sense of how many people this will impact *over a given time period.* I like to use quarters. 

If you're not sure how many customers a feature might impact, don't worry, just take an educated guess based on your understanding of your customer base and their needs — but make sure that you add the appropriate instrumentation.

Let's look at a couple of examples.

**So that I can quickly find recipes by type, my recipes are automatically categorized**

This will reach everyone who saves a recipe in my app ClipDish, that's about 2000 every quarter.

**So that I know how much I'm saving in my own currency, I can see the cost per month by purchasing a annual subscription.**

We don't get very many non US users each month, I looked at my analytics and see it's only about 50 people.

The goal is to ensure that the features you prioritize have the potential to make a significant impact on your customers and help move the needle for your business. And if you're still uncertain, don't worry, we've got you covered with the Confidence score.

## What is Impact and how do I calculate it?

Ah, the Impact score. This is where we start to separate the peanut butter and jelly sandwiches from the sushi rolls. The Impact score measures how much a particular feature or enhancement is going to affect your business. And let's be real, if you're feeding four kids, you need to make sure the meal you're making is going to have some impact.

Calculating the Impact score is important because it helps you prioritize the features that are going to have the most significant impact on your business. Think about it this way: if you're trying to feed four kids, you're not going to waste your time making something that nobody is going to eat. You're going to focus on the meals that are going to make the biggest impact and keep everyone happy.

Of course, determining the Impact score isn't always easy. It can be challenging to predict exactly how much impact a particular feature is going to have. But don't worry, you don't have to be a Michelin-starred chef to figure it out. The RICE framework identifies a scale to use for scoring:
- 3 for "massive impact," 
- 2 for "high," 
- 1 for "medium," 
- 0.5 for "low," 
- 0.25 for "minimal." 

Taking the same examples from Reach let's examine the Impact.

**So that I can quickly find recipes by type, my recipes are automatically categorized**

I think this is going to have a massive impact on engagement because I've had requests for recipe categories. Also most people have a couple of hundred recipes saved and this will make it easier to find them again.

**So that I know how much I'm saving in my own currency, I can see the cost per month by purchasing a annual subscription.**

Since we don't get a lot of non US customers, I don't think this will have much of an impact. 

One thing to note about this is that if I was intentionally building a strategy around a non US market, with marketing dollars behind the strategy, I would increase this Impact score.

*Remember you aren't scoring things in a vacuum.*

## What is Confidence and how do I calculate it?

Confidence is like trying to predict how your four picky eaters will react to a new meal you've never tried before. In product prioritization, it's the level of certainty you have in the impact and effort scores you've assigned to a feature.

Why is confidence important? Well, you don't want to just cross your fingers and hope for the best. You want to make sure that the features you prioritize will actually have the impact you think they will. That's where the Confidence score comes in.

To score the Confidence level, use another multiple-choice scale to avoid decision paralysis. Consider the following:

- 100% - "high confidence"
- 80% - "medium"
- 50% - "low"
- 10% - "Moonshot"

To be honest with yourself, consider how much support you really have for your estimates. Gut check your Confidence score by asking yourself questions like "Have I seen this work before?" or "Do I have data to back up my assumptions?" This can help you avoid overconfidence or underestimating the complexity of a feature.

Let's revisit our examples again.

**So that I can quickly find recipes by type, my recipes are automatically categorized**

I've talked through how this feature will work with the team, they believe that their estimates are correct. I also have pretty good belief in my customer data so I'm going to give this the highest confidence score.

**So that I know how much I'm saving in my own currency, I can see the cost per month by purchasing a annual subscription.**

Again, this is pretty straightforward to do and I am confident in my data. 

Of course, there are challenges when calculating Confidence. It can be difficult to gauge the level of buy-in and support you have for a feature from stakeholders, or to accurately predict how customers will react to a new product offering. But by being honest and using a clear scoring system, you can better prioritize features and make more informed decisions for your business.

## What is Effort? How do I calculate it?

Effort - How long are you stuck in the kitchen?
The Effort score in the RICE framework helps you estimate how long it will take to build a feature relative to the other features in your list. It's important to consider this score when prioritizing features because some may require significantly more time and resources than others.

When calculating the effort score, you can choose to do so in sprints, weeks, or months. This will depend on the nature of the feature and the development process. It's important to involve the engineering team when estimating effort, as they can provide valuable insight into the technical requirements and challenges.

It's not uncommon for engineers to avoid ambiguity and be hesitant to commit to an effort score. As a product manager, it's important to stress that this score is not a firm commitment or deadline, but rather a relative level of effort compared to the other items in a list. This score is used for prioritization purposes and to help the team make informed decisions about where to allocate resources.

**So that I can quickly find recipes by type, my recipes are automatically categorized**

We talked through the feature with the team, they think that it can be done in two weeks based on the information they have.

**So that I know how much I'm saving in my own currency, I can see the cost per month by purchasing a annual subscription.**

We talked through the feature with the team, they think that it can done in a week.

These estimates are just used for prioritization. Once we've picked which backlog items to work on more detailed requirements might impact the Effort.
## How to calculate the RICE score
Ok, it's time to get cooking. Once you have scored the various items in the backlog you can calculate the score.

This is easy if you are using a spreadsheet. Just plug in the following formula.

![RICE formula Reach times Impact times Confidence divided by Effort](/img/RICE%20formula-fs8.png)

## What should I do with the scored backlog items?
Start with the items that have the highest RICE score and work your way down the list. This will help ensure that you are focusing your efforts on the features that have the greatest potential impact, reach, confidence, and effort.

You shouldn't just follow this blindly don't forget about your stakeholders, especially those picky eaters who represent your customers! Be sure to involve them in the process and keep them informed of your progress. This will help ensure that you are building the features that they truly need and want.

Overall, the RICE framework can be a powerful tool for product managers looking to prioritize their backlog items and make data-driven decisions about what to build next. So go ahead, use RICE to score your backlog, prioritize your features, and keep those picky eaters (I mean stakeholders) happy!

If you feel like you can't keep all of your hungry stakeholders happy, [let's talk about how I can help](https://get-in-touch-46.zapier.app).

## Frequently asked questions about RICE scoring

### How do I handle features with similar RICE scores? Which one should be prioritized first?

When two or more features have similar RICE scores, consider additional factors such as strategic alignment, customer demand, or technical debt reduction. Prioritize the feature that aligns closely with your product roadmap and long-term goals, satisfies a critical customer need, or helps improve the codebase for future developments. Remember, RICE is a guide, that should help you, not a hard and fast rule.

### Can the RICE framework be used for bug fixes and technical tasks?

Yes, the RICE framework can be adapted for prioritizing bug fixes and technical tasks. For these, the Reach might refer to how many users are affected by the bug or how the technical task impacts other features. The Impact would relate to the improvement in user experience or system stability. Confidence levels can assess how certain you are about the estimated effect of the fix or task, and Effort remains a measure of the time and resources required.

### How often should the RICE scores be re-evaluated?

RICE scores should be re-evaluated regularly, especially after significant product changes, user feedback cycles, or market shifts. A good practice is to reassess RICE scores at the beginning of each planning cycle or quarterly, ensuring that priorities align with current business goals and user needs.

### How do I adjust the RICE framework for a small or newly formed team?

For small or new teams, it might be helpful to simplify the RICE framework. Consider using broader estimates for Reach and Impact or merging Confidence and Effort into a single "Feasibility" score. The key is to maintain the framework's spirit—balancing potential impact against effort—while making it manageable for your team's size and maturity level.

You might even want to start with a simple ratio of value vs. effort as I've described in [Can't Keep Up? Use This Simple Product Prioritization Framework](https://www.joecotellese.com/posts/value-effort-ratio-prioritization/)

### Can RICE scoring be integrated with agile or scrum methodologies?

Absolutely. RICE scoring can be integrated into agile or scrum by using it during the backlog grooming or sprint planning phases. It can help prioritize the product backlog items (PBIs) or user stories to be tackled in upcoming sprints. RICE scores offer a data-driven way to argue for the priority of certain tasks during sprint planning meetings.

### How do I account for dependencies between features when using RICE?

When features are dependent on one another, evaluate the RICE score of the combined features as a single entity or project. Alternatively, prioritize the dependent features based on their sequence in the development pipeline, ensuring the foundational features with the highest combined RICE score are tackled first. Documenting and mapping out dependencies can help in visualizing the optimal order of implementation.

### Is it possible to use RICE scoring for non-product related decisions?

While RICE scoring is primarily designed for product management, its principles can be applied to various decision-making scenarios where weighing the impact against effort is crucial. Examples include marketing campaign planning, sales strategies, and even operational improvements. However, the specific criteria might need to be adjusted to fit the context of the decision being made.