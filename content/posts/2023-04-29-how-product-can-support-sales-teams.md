---
title: "The Product v. Sales Matchup: Why Focusing on APIs is Key to Enterprise Growth"
description: "Discover how focusing on API features can help you support sales, build partnerships, and expose your app's functionality to third-party services. "
date: 2023-04-29T14:33:48.714Z
preview: ""
draft: false
tags: []
categories: []
slug: product-support-sales-teams
lastmod: 2023-04-29T14:41:29.341Z
---

In the Product v. Sales matchup, who should have the final say on what gets built? It’s a spicy question, right? Product wants to support Sales, but heeding every request for “just one more change” puts you on a path to creating a Franken-product that’s eventually going to become unmanageable and unsellable. 

Worse, this issue usually arises at a critical moment for the product team: when the company shifts focus to the enterprise after mainly targeting SMBs. So how can product teams navigate this “awkward phase” of a company's growth?

Here’s the TL;DR: Shift your approach from product to platform. 


Rich Mironov recently wrote about the [friction between sales and product teams](https://www.mironov.com/cake/) due to mismatched incentives between sales and product.

He suggests partnerships as a way out of the problem. Which is excellent advice. Rather than build one off features, focus on the core product and let 3rd parties customize your app to close a sale. Too often though, in this transition to the enterprise we’ve neglected one thing: the API. 

So then, how do you best support sales?

As soon as you get a whiff that your organization is moving to the enterprise, you need to change your thinking from product features to API features. The API is the product.

Depending on the engineering architecture, this transformation is a sliding scale of difficulty.

On the one side, your product is built with a robust API used by the internal team. Your existing product is technically the first customer of the API. 

On the other, the product is a monolithic application with no clear separation of concerns. This is going to be much harder. Resist the cries of your engineering team to "rewrite our application using microservices." They may want to do this but any rearchitecting needs to be loosely coupled to product delivery.

Despite the difficulty, you need to expose your app functionality to third- party services. Where do you begin?

Focus on authentication first –- any third party developer is going to need to authenticate against your API. Get this in place as soon as you reasonably can, knowing that you are going to have requests for access to your data.

Then split your apps into core areas of functionality – no, I'm not talking about physically splitting your app. I'm talking about creating a model of your app’s functionality, (i.e, reporting, customer database, core feature X, core feature Y)

Then, taking the list of sales requests, see which of the product feature buckets they fall into. If there is a product area with a lot of requests, great, start there first.

If you are being proactive and don't have a list of feature requests from sales yet, woohoo. Instead, focus on areas of the app that have the most usage.

With a prioritized list in hand, task the engineering team with building CRUD (create, read, update, delete) functionality for each of those product areas. 

Finally, ensure that there is adequate documentation you can hand off to a third party developer. 

Obviously, this isn’t a one-size-fits-all approach and I’ve glossed over a lot of things the engineering team will point out to you. The point is,the sooner you start thinking about transitioning from a product to a platform, the easier it will be to transition to the enterprise.

