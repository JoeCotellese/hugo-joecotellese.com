---
title: Focus On Customer Experience like Nike and Disney
date: Tue, 15 Nov 2016 19:51:53 +0000
draft: false
contenttypes:
  - BlogPosting
lastmod: 2023-03-09T18:03:23.498Z
---

![](https://designyourthinking.com/wp-content/uploads/2016/11/episode015.png) I just did an interview on the [Design Your thinking podcast](https://designyourthinking.com/dyt-015-focus-on-customer-experience-like-nike-and-disney-with-joe-cotellese/?ref=producthunt) we covered a lot of ground related to product development. Some highlights include:

- Why you should focus on the customer and not the product.
- I give some tips on how I keep myself organized (hint - hire a great team)
- What tools I use to accomplish my day to day tasks.

Check it out.
