---
title: Blue Ocean Strategy Book Summary
description: Book notes and study guide for a classic in new product go to market strategy.
date: 2024-07-25T19:04:24.441Z
preview: ""
draft: false
tags:
    - books
categories: []
lastmod: 2024-07-25T19:47:47.201Z
type: default
---

## Chapter 1 - Creating Blue Oceans

Chapter Overview:

This chapter introduces the concept of "Blue Ocean Strategy" by discussing how Cirque du Soleil revolutionized the circus industry. It explains the difference between red oceans, where industries compete in established markets, and blue oceans, where new markets are created. The chapter argues that to achieve significant growth and profitability, businesses must innovate and create uncontested market spaces, rather than merely outcompeting rivals in existing markets.

### Key Points

- **Cirque du Soleil's Success**: Achieved rapid growth in a declining industry by targeting a new customer base (adults and corporate clients) and offering a unique entertainment experience, rather than competing directly with traditional circuses.
- **Red Oceans vs. Blue Oceans**:
  - *Red Oceans*: Represent all existing industries where companies compete for market share. Competitive rules are known, leading to fierce competition and diminished profits.
  - *Blue Oceans*: Represent untapped market spaces, created by redefining industry boundaries. Competition is irrelevant because the rules are yet to be defined.
- **Value Innovation**: The core of Blue Ocean Strategy, focusing on making competition irrelevant by offering superior value to customers at lower costs. Combines differentiation and low cost simultaneously.
- **Historical Examples**: Many major industries today did not exist 120 or even 40 years ago, showing the continuous creation of blue oceans (e.g., automobiles, aviation, e-commerce, cell phones).
- **Impact on Business Growth**: Studies show that business launches aimed at creating blue oceans yield higher profits and significant growth compared to those competing in red oceans.
- **Strategic Moves**: The basic unit of analysis should be the strategic move, not the company or industry. Successful companies execute strategic moves that create new market spaces, rather than sticking to the existing competitive landscape.
- **Formulating Blue Ocean Strategy**: Involves systematic approaches and frameworks to minimize risks and maximize opportunities, focusing on innovation that aligns with customer utility, price, and cost.

### Chapter Questions

1. What is the key difference between red oceans and blue oceans in terms of market competition?
2. How did Cirque du Soleil create a blue ocean in the circus industry?
3. What is value innovation, and why is it crucial for creating blue oceans?
4. Can you provide examples of industries that did not exist 40 years ago but are significant today?
5. Why is the strategic move considered the basic unit of analysis for blue ocean strategy?
6. What are some of the driving forces behind the rising imperative to create blue oceans?
7. How does focusing on value innovation challenge the traditional value-cost trade-off?
8. Why are companies often reluctant to pursue blue ocean strategies despite their potential benefits?

## Chapter 2 - Analytical Tools and Frameworks

Chapter Overview:

This chapter delves into the analytical tools and frameworks essential for formulating and executing a blue ocean strategy. It emphasizes the need for systematic approaches to creating blue oceans, contrasting them with the traditional tools used for red ocean strategies. The chapter introduces the strategy canvas and the four actions framework, illustrating their application through the case of the US wine industry and the success of [yellow tail]. Additionally, it highlights the importance of the eliminate-reduce-raise-create grid and the three characteristics of a good strategy: focus, divergence, and a compelling tagline.

### Key Points

- **Strategy Canvas**: A diagnostic and action framework that captures the current state of play in the known market space, helping to visualize the competitive factors and offerings in an industry.
  - *Horizontal Axis*: Factors the industry competes on and invests in.
  - *Vertical Axis*: The offering level that buyers receive across these factors.
  - *Value Curve*: A graphic depiction of a company's relative performance across its industry's factors of competition.

- **Four Actions Framework**: A tool to reconstruct buyer value elements and create a new value curve by answering four key questions:
  - Which factors should be eliminated?
  - Which factors should be reduced well below the industry standard?
  - Which factors should be raised well above the industry standard?
  - Which factors should be created that the industry has never offered?

- **Eliminate-Reduce-Raise-Create Grid**: Complements the four actions framework by ensuring companies act on all four actions, driving them to pursue both differentiation and low costs.

- **Three Characteristics of a Good Strategy**:
  - *Focus*: Clear emphasis on a few key factors.
  - *Divergence*: A distinctive value curve that sets the company apart from competitors.
  - *Compelling Tagline*: A clear and appealing message that communicates the strategy effectively.

- **Application in the US Wine Industry**: The chapter illustrates how Casella Wines applied these tools to create [yellow tail], a fun and easy-to-drink wine that appealed to a broad audience, diverging from traditional wine industry norms.

- **Reading Value Curves**: Analyzing value curves helps companies identify their strategic position and potential for creating blue oceans. Key insights include whether a company has focus, divergence, a compelling tagline, and whether it is caught in the red ocean or overdelivering without payback.

### Chapter Questions

1. What is the strategy canvas, and how does it help in formulating a blue ocean strategy?
2. Describe the four actions framework and its significance in creating a new value curve.
3. How does the eliminate-reduce-raise-create grid complement the four actions framework?
4. What are the three characteristics of a good strategy, and why are they important?
5. How did Casella Wines use the strategy canvas and four actions framework to create [yellow tail]?
6. What are the key factors the US wine industry competed on before the introduction of [yellow tail]?
7. How can companies use the strategy canvas to identify whether they are caught in the red ocean?
8. What insights can be gained from reading value curves in the context of blue ocean strategy?

## Part 2 - Formulating Blue Ocean Strategy

## Chapter 3 - Reconstruct Market Boundaries

Chapter Overview:

This chapter introduces the first principle of blue ocean strategy: reconstructing market boundaries. It outlines the importance of moving beyond existing industry boundaries to create new market space. The chapter presents the six paths framework, which provides systematic approaches to identify commercially compelling blue ocean opportunities. These paths challenge conventional strategic assumptions and help companies innovate by looking at familiar data from new perspectives.

### Key Points

- **Six Paths Framework**:
  1. **Look Across Alternative Industries**: Identify alternatives that serve the same purpose as your product but in different forms (e.g., NetJets in aviation).
  2. **Look Across Strategic Groups within Industries**: Understand why customers trade up or down between different strategic groups within the same industry (e.g., Curves in fitness).
  3. **Look Across the Chain of Buyers**: Shift focus from traditional target buyer groups to overlooked ones (e.g., Novo Nordisk targeting patients rather than doctors).
  4. **Look Across Complementary Product and Service Offerings**: Consider what happens before, during, and after your product is used (e.g., NABI in buses).
  5. **Look Across Functional or Emotional Appeal to Buyers**: Shift the orientation of your industry from functional to emotional or vice versa (e.g., Cemex in cement).
  6. **Look Across Time**: Understand how current trends will evolve and impact your industry (e.g., Apple’s iTunes).

- **Path 1: Look Across Alternative Industries**:
  - Analyze why customers trade across alternatives.
  - Example: NetJets created a new market by offering the convenience of private jets at a lower cost compared to commercial flights.

- **Path 2: Look Across Strategic Groups within Industries**:
  - Identify factors that lead customers to trade up or down between strategic groups.
  - Example: Curves created a fitness option that combined the benefits of home exercise and traditional health clubs, eliminating unnecessary elements.

- **Path 3: Look Across the Chain of Buyers**:
  - Shift focus from traditional buyers to overlooked ones.
  - Example: Novo Nordisk developed user-friendly insulin delivery systems targeting patients instead of doctors.

- **Path 4: Look Across Complementary Product and Service Offerings**:
  - Define the total solution buyers seek and address complementary needs.
  - Example: NABI redesigned buses using fiberglass to reduce maintenance costs and improve efficiency.

- **Path 5: Look Across Functional or Emotional Appeal to Buyers**:
  - Challenge the functional or emotional orientation of your industry.
  - Example: Cemex turned cement from a functional product into an emotional investment by associating it with home building.

- **Path 6: Look Across Time**:
  - Analyze irreversible trends with a clear trajectory and understand their future impact.
  - Example: Apple’s iTunes capitalized on the trend of digital music sharing to create a new market space.

### Chapter Questions

1. What is the six paths framework, and why is it essential for creating blue oceans?
2. How did NetJets utilize the first path (looking across alternative industries) to create a new market?
3. What factors did Curves eliminate, reduce, raise, and create to differentiate itself within the fitness industry?
4. How did Novo Nordisk shift its focus to create a blue ocean in the insulin industry?
5. What complementary product or service offerings did NABI consider to reduce costs and improve efficiency in the bus industry?
6. How did Cemex transform the cement industry by shifting its orientation from functional to emotional appeal?
7. What trend did Apple identify and capitalize on to create the iTunes store?
8. How can companies apply the six paths framework to identify new market opportunities within their industries?

## Chapter 4 - Focus on the Big Picture, Not the Numbers

Chapter Overview:

This chapter emphasizes the importance of aligning the strategic planning process with the goal of creating blue oceans. It highlights the pitfalls of traditional strategic planning, which often focuses on competing within existing market space. Instead, the chapter advocates for a visual approach to strategy formulation, centered around the strategy canvas. This approach helps companies focus on the big picture, fostering creativity and clear strategic direction. The chapter introduces a four-step process for visualizing strategy: visual awakening, visual exploration, visual strategy fair, and visual communication.

### Key Points

- **Traditional Strategic Planning Issues**:
  - Focuses on competing within existing market space.
  - Involves lengthy descriptions of industry conditions and competitive situations.
  - Results in tactical, red ocean moves rather than strategic, blue ocean moves.
  - Leads to confusion and lack of clear direction.

- **Strategy Canvas**:
  - A tool to visualize a company's current strategic position and future strategy.
  - Shows the strategic profile of the industry, competitors, and the company.
  - A successful strategy canvas exhibits focus, divergence, and a compelling tagline.

- **Four Steps of Visualizing Strategy**:
  1. **Visual Awakening**:
     - Identify the current state of play by drawing the company's value curve.
     - Highlight the lack of focus and differentiation in the current strategy.
     - Example: European Financial Services (EFS) identified weaknesses in their strategy by comparing it to competitors.

  2. **Visual Exploration**:
     - Conduct field research to understand how customers and noncustomers use products or services.
     - Interview and observe customers, noncustomers, and users to uncover unmet needs.
     - Example: EFS discovered the importance of transaction speed and accuracy through field research.

  3. **Visual Strategy Fair**:
     - Present new strategic options using value curves to an audience of external judges.
     - Collect feedback and refine the strategy based on common likes and dislikes.
     - Example: EFS held a strategy fair to identify the most compelling future strategy.

  4. **Visual Communication**:
     - Communicate the new strategy clearly to all employees.
     - Distribute a one-page picture showing the new and old strategic profiles.
     - Ensure all investment decisions align with the new strategy.
     - Example: EFS used the new strategy canvas to guide all investment decisions and align efforts.

- **Visualizing Strategy at the Corporate Level**:
  - Use strategy canvases to facilitate dialogue among business units and the corporate center.
  - Share strategic best practices across units.
  - Example: Samsung Electronics institutionalized strategy canvases to drive value innovation and growth.

- **Pioneer-Migrator-Settler (PMS) Map**:
  - Classify businesses as pioneers, migrators, or settlers to assess growth potential.
  - Pioneers offer unprecedented value and drive profitable growth.
  - Migrators offer improved value but not innovative value.
  - Settlers conform to industry standards and have limited growth potential.
  - Use the PMS map to balance the business portfolio and push for value innovation.

### Chapter Questions

1. What are the main issues with traditional strategic planning that the chapter highlights?
2. How does the strategy canvas help companies focus on the big picture?
3. What are the three complementary qualities of a successful strategy canvas?
4. Describe the four steps of visualizing strategy.
5. How did European Financial Services (EFS) use the visual strategy fair to refine their strategy?
6. Why is it important to conduct field research during the visual exploration step?
7. How can companies use the Pioneer-Migrator-Settler (PMS) map to assess their growth potential?
8. How does Samsung Electronics utilize strategy canvases at the corporate level to drive growth?
9. How can visualizing strategy address the limitations of traditional strategic planning?
10. What role does visual communication play in ensuring the successful implementation of a new strategy?

## Chapter 5 - Reach Beyond Existing Demand

Chapter Overview:

This chapter explores how to maximize the size of the blue ocean by reaching beyond existing demand. It introduces the third principle of blue ocean strategy: focus on noncustomers and commonalities rather than existing customers and segmentation. By doing so, companies can create a significant new market. The chapter details how to identify and target three tiers of noncustomers to unlock and aggregate latent demand, transforming noncustomers into customers and expanding the blue ocean.

### Key Points

- **Third Principle of Blue Ocean Strategy**:
  - Focus on noncustomers and commonalities rather than existing customers and segmentation.
  - Aim to create significant new demand and expand the market.

- **Challenge Conventional Strategy Practices**:
  - Conventional strategies focus on existing customers and finer segmentation.
  - These often lead to small target markets and intense competition.
  - Reverse this by looking at noncustomers and focusing on commonalities to aggregate demand.

- **Callaway Golf Example**:
  - Identified why noncustomers (potential golfers) didn't take up golf.
  - Created the Big Bertha club with a larger head, making golf easier and more enjoyable.
  - Attracted noncustomers and existing customers, creating a new mass market.

- **Three Tiers of Noncustomers**:

  1. **First-Tier Noncustomers**:
     - On the edge of the market; use the product minimally and are ready to leave.
     - Example: Pret A Manger attracted professionals who needed fast, fresh, and reasonably priced meals.

  2. **Second-Tier Noncustomers**:
     - Refuse to use current offerings because they find them unacceptable or unaffordable.
     - Example: JCDecaux created "street furniture" for urban advertising, converting municipalities and advertisers who found traditional billboards ineffective.

  3. **Third-Tier Noncustomers**:
     - Have never considered the market's offerings as an option.
     - Example: Joint Strike Fighter (JSF) program consolidated the needs of different military branches, creating a cost-effective, high-performing aircraft.

- **Go for the Biggest Catchment**:
  - Focus on the tier of noncustomers representing the largest catchment your organization can act on.
  - Look for commonalities across tiers to maximize the scope of latent demand.

### Chapter Questions

1. What is the third principle of blue ocean strategy discussed in this chapter?
2. Why is focusing on noncustomers more beneficial than focusing on existing customers?
3. How did Callaway Golf create a blue ocean with the Big Bertha club?
4. Describe the three tiers of noncustomers.
5. What are first-tier noncustomers, and how did Pret A Manger attract them?
6. Explain how JCDecaux created a new market space with "street furniture."
7. Who are third-tier noncustomers, and how did the JSF program cater to them?
8. Why should companies focus on commonalities among noncustomers rather than differences?
9. What factors should a company consider when deciding which tier of noncustomers to target?
10. How can a company maximize the scope of latent demand when formulating future strategies?

By understanding and targeting noncustomers across these three tiers, companies can uncover and capitalize on significant opportunities for creating new demand and expanding their markets.

## Chapter 6 - Get the Strategic Sequence Right

Chapter Overview:
This chapter discusses the fourth principle of blue ocean strategy: getting the strategic sequence right. It emphasizes the importance of building a robust business model by following a specific sequence—buyer utility, price, cost, and adoption—to ensure commercial viability and reduce business model risk. The chapter provides tools and frameworks to help companies test and refine their blue ocean ideas to ensure they deliver exceptional value and are commercially successful.

### Key Points

- **Strategic Sequence**:
  - **Buyer Utility**: Ensure the offering unlocks exceptional utility and provides a compelling reason for the target mass of people to buy it.
  - **Strategic Pricing**: Set a price that attracts the mass of target buyers and ensures they have the ability to pay.
  - **Cost**: Ensure the offering can be produced at the target cost while still earning a healthy profit margin.
  - **Adoption**: Address any hurdles in rolling out the idea to ensure successful adoption by employees, partners, and the general public.

- **Testing for Exceptional Utility**:
  - Use the **Buyer Utility Map** to identify where the offering can deliver exceptional utility across the buyer experience cycle (purchase, delivery, use, supplements, maintenance, disposal) and utility levers (customer productivity, simplicity, convenience, risk, fun and image, environmental friendliness).
  - Remove the greatest blocks to utility to convert noncustomers into customers.

- **Strategic Pricing**:
  - Determine the **price corridor of the target mass** by identifying products and services that perform the same function or have the same objective.
  - Set the strategic price within the corridor to attract the largest group of target buyers while considering the degree of legal protection and the company’s exclusive assets or core capabilities.

- **Target Costing**:
  - Start with the strategic price and deduct the desired profit margin to arrive at the target cost.
  - Use three levers to hit the target cost: streamlining operations and introducing cost innovations, partnering, and changing the pricing model of the industry.

- **Adoption**:
  - Address potential resistance from employees, partners, and the general public by educating them about the new idea and its merits.
  - Engage in open discussions to set clear expectations and address concerns.

- **Blue Ocean Idea (BOI) Index**:
  - Use the BOI index to test the viability of blue ocean ideas across the dimensions of utility, price, cost, and adoption.
  - Refine the idea to pass the BOI index to ensure commercial success.

### Chapter Questions

1. What is the fourth principle of blue ocean strategy?
2. Why is following the strategic sequence of buyer utility, price, cost, and adoption important?
3. How does the Buyer Utility Map help in assessing exceptional utility?
4. What are the six stages of the buyer experience cycle in the Buyer Utility Map?
5. What are the six utility levers in the Buyer Utility Map?
6. How can companies identify the price corridor of the target mass?
7. What factors should be considered when setting a strategic price within the price corridor?
8. What are the three levers companies can use to meet their target cost?
9. How can partnering help companies achieve their target cost?
10. Why is it important to address adoption hurdles, and how can companies do this effectively?
11. How does the Blue Ocean Idea (BOI) Index help in testing the viability of blue ocean ideas?
12. What are the key steps in refining a blue ocean idea to ensure it passes the BOI index?

## Part 3 - Executing Blue Ocean Strategy

## Chapter 7 - Overcome Key Organizational Hurdles

Chapter Overview:
This chapter discusses the fifth principle of blue ocean strategy: overcoming key organizational hurdles to execute a strategic shift. It identifies four primary hurdles—cognitive, resource, motivational, and political—that managers must address to ensure successful strategy implementation. The chapter introduces tipping point leadership as an approach to overcome these hurdles quickly and at low cost by focusing on factors of disproportionate influence.

### Key Points

- **Four Organizational Hurdles**:
  - **Cognitive Hurdle**: Waking employees up to the need for a strategic shift.
  - **Resource Hurdle**: Executing a strategic shift with limited resources.
  - **Motivational Hurdle**: Motivating key players to move fast and tenaciously.
  - **Political Hurdle**: Overcoming resistance from powerful vested interests.

- **Tipping Point Leadership**:
  - Focus on acts and activities that exercise disproportionate influence on performance.
  - Overcome hurdles by concentrating efforts on key factors rather than spreading resources thinly.

- **Breaking through the Cognitive Hurdle**:
  - **Ride the “Electric Sewer”**: Make people experience the worst operational problems firsthand.
  - **Meet with Disgruntled Customers**: Listen directly to the most dissatisfied customers to understand the need for change.

- **Jumping the Resource Hurdle**:
  - **Redistribute Resources to Hot Spots**: Focus on activities with low resource input but high performance gains.
  - **Redirect Resources from Cold Spots**: Identify and reduce resource allocation to activities with high resource input but low performance impact.
  - **Engage in Horse Trading**: Trade excess resources with other units to fill resource gaps.

- **Jumping the Motivational Hurdle**:
  - **Zoom In on Kingpins**: Focus efforts on key influencers in the organization.
  - **Place Kingpins in a Fishbowl**: Make their actions and performance highly visible to create accountability.
  - **Atomize to Get the Organization to Change Itself**: Break the strategic challenge into achievable, bite-size tasks.

- **Knocking Over the Political Hurdle**:
  - **Secure a Consigliere**: Have a respected insider who knows the political landscape.
  - **Leverage Your Angels and Silence Your Devils**: Identify and support those who benefit from the change while isolating detractors.

- **Challenging Conventional Wisdom**:
  - Conventional wisdom focuses on moving the mass; tipping point leadership focuses on transforming the extremes to change the core.

### Chapter Questions

1. What are the four primary hurdles to executing a strategic shift?
2. How does tipping point leadership differ from conventional wisdom in addressing these hurdles?
3. What is the cognitive hurdle, and how can it be overcome using tipping point leadership?
4. How does experiencing the worst operational problems firsthand help break the cognitive hurdle?
5. What are hot spots and cold spots in the context of resource allocation?
6. How can resource redistribution to hot spots help jump the resource hurdle?
7. What is horse trading, and how can it be used to fill resource gaps?
8. Who are kingpins, and why are they important in motivating organizational change?
9. How does placing kingpins in a fishbowl create accountability and motivation?
10. What does atomization mean, and how does it help in executing a strategic shift?
11. Why is it important to have a consigliere on the top management team?
12. How can leveraging angels and silencing devils help overcome the political hurdle?
13. What is the key difference between conventional wisdom and tipping point leadership in executing a strategic shift?

## Chapter 8 - Build Execution into Strategy

Chapter Overview:
This chapter emphasizes the necessity of incorporating execution into strategy from the start to ensure successful implementation. It introduces the concept of fair process, which is crucial for fostering trust and commitment among employees. By engaging employees, explaining decisions, and setting clear expectations, companies can align the entire organization with the new strategy, ensuring effective execution.

### Key Points

- **Importance of Execution**:
  - A company is not just top management but everyone from the top to the front lines.
  - Overcoming organizational hurdles is essential, but the fundamental base of action is the attitudes and behavior of people deep in the organization.
  - Building a culture of trust and commitment is crucial for strategy execution.

- **Fair Process**:
  - Engages people in the strategic decisions that affect them.
  - Involves three principles: engagement, explanation, and expectation clarity.
  - Builds people’s buy-in up front, ensuring voluntary cooperation in executing strategies.

- **Poor Process Can Ruin Strategy Execution**:
  - Example of Lubber: Sales force resisted a new strategy because they were not involved in the strategy-making process.
  - Without fair process, even good strategies can fail due to lack of trust and commitment from employees.

- **Three E Principles of Fair Process**:
  - **Engagement**: Involve individuals in strategic decisions by seeking their input.
  - **Explanation**: Ensure everyone understands why decisions are made and the rationale behind them.
  - **Expectation Clarity**: Clearly state new rules, goals, targets, and responsibilities after setting a strategy.

- **Case Study: Elco’s Two Plants**:
  - Chester plant failed due to lack of fair process: Employees were not engaged, explanations were missing, and expectations were unclear.
  - High Park plant succeeded by adhering to fair process: Engaging employees, explaining the strategy, and setting clear expectations.

- **Intellectual and Emotional Recognition Theory**:
  - Fair process conveys intellectual and emotional recognition.
  - Intellectual recognition leads to knowledge sharing.
  - Emotional recognition leads to voluntary cooperation.

- **Fair Process and Intangible Capital**:
  - Commitment, trust, and voluntary cooperation are intangible capital critical for executing blue ocean strategies.
  - Fair process minimizes management risk by aligning people’s attitudes and behavior with the strategy.

- **Fair Process and External Stakeholders**:
  - Fair process is equally important for external stakeholders, especially in complex projects with multiple partners.
  - Example of the F-35 program: Lack of fair process led to cost overruns and delays.

### Chapter Questions

1. Why is it essential to incorporate execution into strategy from the start?
2. What is fair process, and how does it contribute to strategy execution?
3. What are the three E principles of fair process?
4. How did the lack of fair process contribute to the failure of Lubber’s strategy?
5. Compare the outcomes of implementing the new manufacturing process at Elco’s Chester and High Park plants.
6. How does fair process convey intellectual and emotional recognition?
7. Why is intangible capital such as commitment, trust, and voluntary cooperation crucial for executing blue ocean strategies?
8. How did the violation of fair process affect the F-35 program’s execution?
9. What are the potential consequences of not practicing fair process with external stakeholders?
10. How can fair process help in building a culture of trust and commitment within an organization?

## Chapter 9 - Align Value, Profit, and People Propositions

Chapter Overview:
This chapter delves into the importance of aligning the three essential strategy propositions—value, profit, and people—for the success and sustainability of a blue ocean strategy. It emphasizes that while each proposition is crucial on its own, their alignment is what ensures a high-performing and enduring strategy. The chapter discusses how to develop and align these propositions effectively, using real-world examples to illustrate the concepts.

### Key Points

- **Three Strategy Propositions**:
  - **Value Proposition**: An offering that attracts buyers by providing a leap in value.
  - **Profit Proposition**: A business model that enables the company to make money from its offering.
  - **People Proposition**: Motivating the people working for or with the company to execute the strategy.

- **Importance of Alignment**:
  - A strategy must align value, profit, and people propositions to achieve sustainable success.
  - Misalignment can lead to short-lived success or failure, as seen in many innovations that fail to sustain their initial market excitement.

- **Strategic Alignment Responsibility**:
  - Top executives are responsible for ensuring the alignment of the three strategy propositions.
  - Functional biases often prevent a holistic approach, emphasizing the need for a comprehensive view of strategy.

- **Blue Ocean Strategy Alignment**:
  - Achieving high performance in blue ocean strategy requires aligning the propositions to pursue both differentiation and low cost.
  - Proper alignment is difficult to imitate, particularly the people proposition, which involves human relationships and takes time to cultivate.

- **Case Study: Comic Relief**:
  - Comic Relief's success in the crowded UK charity industry is attributed to its aligned value, profit, and people propositions.
  - **Value Proposition**: Differentiated by fun and engaging fund-raising methods, involving the community, and recognizing even small donations.
  - **Profit Proposition**: Low-cost structure by leveraging volunteers and avoiding traditional expensive fund-raising methods.
  - **People Proposition**: Motivating volunteers, corporate sponsors, and celebrities by creating a platform for fun and pride in contributing to a greater cause.

- **When Strategy is Misaligned**:
  - The Tata Nano's initial success was not sustained due to misalignment in the people proposition, particularly with external stakeholders.
  - Misalignment in any proposition can bring about negative performance consequences.

- **Holistic Understanding of Strategy**:
  - Successful strategy requires a holistic understanding and alignment of value, profit, and people propositions.
  - Examples of Napster and Apple’s iTunes illustrate the importance of alignment for sustainable success.

### Chapter Questions

1. What are the three essential strategy propositions, and why are they important for a successful strategy?
2. Why is it crucial to align value, profit, and people propositions in a strategy?
3. Who is responsible for ensuring the alignment of the three strategy propositions within an organization?
4. How does aligning the three strategy propositions differ between red ocean and blue ocean strategies?
5. Explain how Comic Relief successfully aligned its value, profit, and people propositions.
6. What went wrong with the Tata Nano’s strategy despite its compelling value and profit propositions?
7. How did Apple’s iTunes succeed where Napster failed in the digital music industry?
8. Why is it challenging to imitate a well-aligned blue ocean strategy, especially the people proposition?
9. How can a company identify if its strategy propositions are misaligned?
10. What steps can a company take to ensure the alignment of its value, profit, and people propositions for sustained success?

## Chapter 10 - Renew Blue Oceans

### Chapter Overview
This chapter discusses the dynamic nature of blue ocean strategies and emphasizes that creating a blue ocean is an ongoing process rather than a one-time achievement. It addresses the challenges of imitation and the necessity of renewing blue ocean strategies over time to maintain competitive advantage. The chapter explores the barriers to imitation and provides guidelines for monitoring and renewing blue oceans at both the individual business and corporate levels.

### Key Points

- **Barriers to Imitation**:
  - **Alignment Barrier**: The alignment of value, profit, and people propositions creates a robust system difficult to replicate.
  - **Cognitive and Organizational Barriers**: Conventional strategic logic and internal politics often prevent quick imitation.
  - **Brand Barrier**: Strong brand identity and loyalty make it hard for imitators to compete.
  - **Economic and Legal Barriers**: Natural monopolies, economies of scale, network effects, patents, and legal permits protect blue oceans.

- **Renewal Process**:
  - **Monitoring Value Curves**: Regularly assess the company's value curve against competitors to determine when to innovate again.
  - **Avoiding Early Value Innovations**: Focus on operational improvements and market expansion before pursuing a new blue ocean.

- **Examples of Successful Renewal**:
  - **Salesforce.com**: Demonstrated dynamic renewal by continuously innovating and extending its offerings as competitors imitated its initial blue ocean strategy.
  - **Apple Inc.**: Managed a balanced portfolio of pioneers, migrators, and settlers, ensuring continuous growth through strategic innovations.

- **Renewal at the Corporate Level**:
  - **Dynamic PMS Map**: A tool for companies with diverse portfolios to visualize and plan the renewal of their business offerings.
  - **Maintaining Balance**: Companies should maintain a mix of pioneers, migrators, and settlers to ensure sustainable growth and profitability.

- **Case Study: Apple Inc.**:
  - **iMac, iPod, iTunes, iPhone, and iPad**: A series of strategic moves that kept Apple ahead by continuously creating new blue oceans.
  - **Dynamic Portfolio Management**: Apple’s strategic moves illustrate how to balance and renew business offerings to stay ahead of competitors.

- **Challenges of Renewal**:
  - **Microsoft**: Struggled to maintain a balanced portfolio, relying heavily on settler businesses without launching new pioneers, leading to stagnation in growth.

### Chapter Questions

1. What are the primary barriers to imitation that help sustain a blue ocean strategy?
2. How can companies monitor their value curves to determine when to innovate again?
3. Why is it important for a company to avoid early value innovations when there is still profit to be gained from the current blue ocean?
4. How did Salesforce.com successfully renew its blue ocean strategy over time?
5. What role does the dynamic PMS map play in managing the renewal process for multibusiness firms?
6. How did Apple Inc. balance its portfolio of pioneers, migrators, and settlers to maintain continuous growth?
7. What were the consequences of Microsoft’s failure to maintain a balanced portfolio of businesses?
8. Why is it important for companies to master both red ocean and blue ocean strategies?
9. How can companies use operational improvements and geographical expansion to maximize the rent stream from their blue ocean strategies?
10. What steps should a company take to renew its blue ocean strategy before competitors catch up?

## Chapter 11 - Avoid Red Ocean Traps

### Chapter Overview
This chapter highlights common misconceptions that hinder the effective implementation of blue ocean strategies. These misconceptions, or "red ocean traps," often stem from traditional competitive thinking and prevent organizations from fully embracing the principles of blue ocean strategy. Understanding and avoiding these traps is essential for organizations to successfully create and sustain blue oceans.

### Key Points

- **Red Ocean Trap One**: Believing blue ocean strategy is customer-oriented and customer-led.
  - Focus on noncustomers rather than existing customers to identify new demand.
  - Analyzing noncustomers provides insights into industry pain points and points of intimidation.

- **Red Ocean Trap Two**: Believing you must venture beyond your core business to create a blue ocean.
  - Blue oceans can be created within the core business, not necessarily outside it.
  - Successful examples include [yellow tail], Nintendo Wii, and Apple iMac.

- **Red Ocean Trap Three**: Believing blue ocean strategy is about new technologies.
  - Technology is not a prerequisite for blue ocean strategy.
  - The key is linking innovation to value, as seen with Comic Relief, [yellow tail], and Starbucks.

- **Red Ocean Trap Four**: Believing you must be first to market.
  - Success comes from being first to get it right by linking innovation to value, not just speed.
  - Apple’s iPod, iPhone, and iPad were not first but succeeded by getting it right.

- **Red Ocean Trap Five**: Confusing blue ocean strategy with differentiation strategy.
  - Blue ocean strategy is about breaking the value-cost trade-off to pursue both differentiation and low cost simultaneously.
  - Examples include [yellow tail] and Comic Relief.

- **Red Ocean Trap Six**: Confusing blue ocean strategy with low-cost strategy.
  - Blue ocean strategy achieves differentiation and low cost by creating a leap in buyer value.
  - Examples include Southwest Airlines and Swatch.

- **Red Ocean Trap Seven**: Believing blue ocean strategy is the same as innovation.
  - Blue ocean strategy focuses on value innovation, not just innovation for its own sake.
  - Innovation must create a leap in value for the target mass of buyers.

- **Red Ocean Trap Eight**: Seeing blue ocean strategy as a theory of marketing and niche strategy.
  - Blue ocean strategy involves more than marketing; it requires a holistic approach involving value, profit, and people propositions.
  - It focuses on desegmenting markets to capture new demand, not on niche markets.

- **Red Ocean Trap Nine**: Believing blue ocean strategy sees competition as bad.
  - Competition is not always good; when supply exceeds demand, it can lead to negative economic consequences.
  - Blue ocean strategy focuses on value innovation to make the competition irrelevant and create new market space.

- **Red Ocean Trap Ten**: Confusing blue ocean strategy with creative destruction or disruption.
  - Blue ocean strategy goes beyond creative destruction to embrace nondestructive creation.
  - It redefines problems to create new demand and often complements existing products and services.

### Chapter Questions

1. Why is focusing on noncustomers more effective than focusing on existing customers for creating blue oceans?
2. How can blue oceans be created within an organization's core business?
3. Why is technology not a prerequisite for blue ocean strategy?
4. What is more important than being first to market in blue ocean strategy?
5. How does blue ocean strategy differ from traditional differentiation strategy?
6. Why is blue ocean strategy not synonymous with low-cost strategy?
7. What distinguishes value innovation from general innovation in blue ocean strategy?
8. Why is blue ocean strategy more than just a marketing strategy or niche strategy?
9. How does blue ocean strategy approach competition differently from traditional strategies?
10. How does blue ocean strategy go beyond creative destruction to embrace nondestructive creation?
