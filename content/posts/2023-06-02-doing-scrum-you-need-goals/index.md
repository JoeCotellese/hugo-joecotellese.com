---
title: Doing Scrum? You need goals
description: Transcript of an exchange with an Agile coach on Sprint misconceptions
date: 2023-06-02T14:29:50.075Z
preview: ""
draft: false
tags: []
categories: []
slug: sprint-goals-are-critical
lastmod: 2023-12-18T19:52:00.420Z
---

I came across a LinkedIn thread the other day from an Agile Coach writing about Scrum misconceptions.

1. You have to complete everything in the Sprint
2. Changes to the Sprint are bad
3. Not finished what you promised means you don't know what you're doing
4. You should plan your Sprint at full capacity
5. Doing Sprints means rushing
6. The Sprint Goal is optional

*This is terrible advice and I disagreed with all of it.*

![Jim from The Office, disagreeing](jim-the-office-disagree.gif)

Here is where I think they missed the boat.

## Completing a sprint

Having carry over is a symptom of a larger problem in the team and organization. When I see this, I triage by look at these factors:

1. The team doesn't fully understand the work they are taking on.
2. The team doesn't fully understand the code base they are working in.
3. The team is afraid to make commitments to each other, the why's could vary.
4. The team is frequently interrupted by external factors.

## Changes to a sprint are bad

According to the Scrum guide a Sprint is "where ideas are turned into value." A Sprint's job is not to support change.

Changes are a disruptive and violent affair, they are allowed but should only happen in rare cases. Frequent change? Your team has issues. Infrequent change? External events rendered the goal irrelevant. 

## Not finishing something means you don't know what you are doing

"Don't know what you're doing" is a pretty loaded term. What I would say is "As a team, we haven't fully understood the problem we are trying to solve". In a retrospective, we'd try to understand why.

## You should plan your Sprint at full capacity

I'm not sure what is meant by "full capacity." Wall time? Yes, you should not fill every 8 hour day with work. 

Your capacity is defined by how much work you can take on based on historical data and currently team availability. Based on that, you *should* be filling to capacity.

## Doing Sprints means rushing

Who has that misconception? Sprint is a ceremony. If external forces think Sprint's mean rushing then they aren't fully briefed on Scrum.

## The Sprint Goal is optional

The Sprint goal creates a shared objective by the entire team working the Sprint.  It's one of the fundamental artifacts of Scrum. Without a Sprint goal, the team has no shared objective. You also have no way to externalize what the team is working on to the broader organization.

## What does a successful Sprint team do?

Someone then asked a follow up question – "Would you be willing to unpack what it looks like when a team a) fully understand the work they are taking on, and b) fully understand the code base they are working in?"

If the code base is unclear, I'll often have teams time box exploration within a Sprint or have a Sprint 0 to dig into the code and come up with a plan.

What does a successful Scrum team look like? They embody the values of Commitment, Focus, Openness, Respect, and Courage as defined in the Scrum Guide.

I don't believe these things are absolutes but rather ideals to achieve through the continuous improvements of self and team.

"Sure, that all sounds great but what does it actually *look* like?"

**A successful Agile Sprint hits three things:**

1. You achieved the stated goals and
2. delivered value
3. within the specified timeline.

How do you know you've done these things? That's what your KPIs are for (you are measuring stuff right?)

I look at the following key indicators. As you read these, note that the devil is in the details, I coach teams on getting these things right. [Reach out if you need more specific help](/services/).

### 1. Completed User Stories

The team has successfully completed all the user stories committed to the Sprint during the Sprint Planning session. This means that all the planned work has been implemented, tested, and meets the Definition of Done (DoD). When working on stories, you should always ensure you have a clear Definition of Done, otherwise, the team doesn't know what this looks like. 

These stories should be completed throughout the Sprint, if you look at a burn down chart and all the stories close at the end of the Sprint rather than throughout the Sprint you have bottlenecks some where in your process.

Demonstrable Increment: At the end of the Sprint, the team can showcase a working and potentially shippable product increment to the stakeholders. This demonstrates progress and provides an opportunity for feedback and validation.

### 2. Team Collaboration

The team members have actively collaborated and communicated effectively throughout the Sprint. They have participated in daily stand-up meetings, discussed progress, shared knowledge, and resolved any blockers or dependencies. If through the course of a Sprint, you keep hearing people talking about the same issues there might be a communication problem within your team.

### 3. Adherence to Sprint Goal

The team has successfully achieved the Sprint Goal.

### 4. Predictable Velocity

The team has established a predictable velocity, meaning they have a good understanding of their capacity and can consistently deliver a certain amount of work in each Sprint. This allows for accurate planning and forecasting for future Sprints and helps manage stakeholder expectations. This is the hardest thing to get right.

### 5. Continuous Improvement

The team has actively embraced the principles of Agile and Scrum, regularly reflecting on their processes and looking for ways to improve their efficiency and effectiveness. They have participated in the Sprint Retrospective, identified areas of improvement, and implemented appropriate changes in subsequent Sprints. One way to do this is track the items that come up in a retrospective and as a team, pick one to focus on for upcoming Sprints.

### 6. Customer Satisfaction

The delivered features and functionality meet or exceed the expectations of the customers or end-users. Their feedback and satisfaction serve as a measure of success for the Sprint. What you ship in a single Sprint might not be all of a single feature so you might need to have internal stakeholders act as proxies for a customer.

### 7. High-Quality Deliverables 

The team has delivered high-quality work that meets the acceptance criteria and fulfills the requirements outlined in the user stories. The implemented features should be well-tested, bug-free, and ready for deployment.

### 8. Minimal Technical Debt

The team has avoided accumulating excessive technical debt during the Sprint. They have followed good software engineering practices, maintained code quality, and addressed any technical issues promptly. This is another tricky one. If your goal in a Sprint is to build a throwaway prototype you want to use for learning then you don't care about technical debt.

## Conclusion

Doesn't this sound lovely? I'm telling you, nothing makes a team feel better than hitting their Sprint goals on a regular cadence. This *can* happen, I've worked on those teams.

Understanding and correctly implementing Scrum principles is crucial for the success of any Agile team. By embracing the true spirit of Scrum, teams can achieve remarkable efficiency and satisfaction in their sprints.

It's not just about following a methodology; it's about fostering a culture of collaboration, continuous improvement, and clear goal-setting.

If this post was interesting to you sign up for the product management list below and I'll send you an email whenever I post new content.
