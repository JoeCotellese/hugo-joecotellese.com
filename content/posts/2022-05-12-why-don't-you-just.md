---
title: Why don't you just ...
description:
  If you have worked in product management for more than a few months someone
  has said this to you. It's deeply frustrating and can feel condescending.
date: 2022-05-12T17:04:15.894Z
preview: ""
draft: ""
categories: ""
lastmod: 2023-03-09T18:15:59.515Z
---

"Why don't you just do ...?"

"You should just ..."

If you have worked in product management for more than a few months someone has
said this to you. It's deeply frustrating and can feel condescending.

<!--more-->

I suspect that the product management team at Twitter is going to get a buttload of that if Elon Musk takes over.

Relax ...

It's not their fault if they don't have all the context or if they
are stating the obivous. They are just trying to help.

When I hear this, I'll usually respond with some variation of "thanks for the feedback,
we've tried that and it didn't work because X."

Resist the urge to lose your cool. I know this is hard but remember you don't have all the answers.
If we react poorly when someone provides us feedback we close down discussion and debate -- the life blood of product management.
