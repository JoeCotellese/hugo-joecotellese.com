---
title: Add Agility to your Product Management Process
date: Sun, 07 Dec 2014 12:42:48 +0000
draft: false
lastmod: 2023-03-09T18:03:11.510Z
---

Agile is a software development process that focuses on delivering customer facing value in small iterations.

This article assumes that your development team works in [Agile Scrum](https://www.scrumalliance.org/why-scrum).

At my current company, the Product Management Process is what I would term Scrumfall. We do not build heavy Product Requirements Documents that we turn over to developers. Rather, we plan iterations while still maintaining an overall vision of what we think the final product should look like.

In order for Product Management to continue to focus on long term planning and stay one step ahead of the development teams we adopted Agile into our process. The requirements for a product are more than just the User Stories. They require us to research our customers, design the interactions and provide the UI assets that together make up a product specification. This can’t happen ad-hoc in the days leading up to a development team sprint. Yet we found that this was happening constantly. We knew that in order for us to get sufficiently ahead (but not too far ahead) it must be built into our overall process.

What we settled on is working on our own “Product Sprint.” This sprint allowed us to do two things. It gives time for the Product Owner and the UX Designer to get two sprints ahead of the development teams in order to produce wireframes and user stories. It also gives the UI and visual designers one sprint to take the user stories and wireframes and build assets. Together the Product team works two weeks ahead of the development teams. When it comes time for the sprint team to begin working on a User Story it meets their definition of ready and they can pull it into a sprint.

But wait, that’s not Agile! Well, ok, maybe Agile purists will say that the designers should be working within a sprint in order to build out the wireframes and assets. I’ve never worked in an environment where that works at well.

What we are doing as I said is more akin to Scrumfall where we do as much prep work as necessary but no more than the development teams need. In this way, we have a more efficient way of working and spend less time scrambling mid-sprint in order to provide assets and prototypes. Also, like everything else, this is an iterative process. We’re working this way for now but if it doesn’t fit we’re going to adapt.

How do you ensure that your development teams are ready to work their backlog?
