---
title: "Go Paperless: Transform Your Mac into a Digital Filing Cabinet"
description: Learn how to conquer paper clutter with a digital filing system on your Mac. Discover tools and tips to efficiently scan, name, and organize your documents digitally.
blogTitle: "From Piles to Files: The Ultimate Guide to Digital Filing with Your Mac"
date: Mon, 19 Nov 2018 12:59:51 +0000
draft: false
contenttypes:
  - BlogPosting
lastmod: 2024-03-11T00:00:58.225Z
---

I have a huge filing cabinet and I hate using it. Each day, I get mail – junk mail, bills, medical records, brochures, manuals, school work, investments. You name it, I get it. These papers end up stacked like little mountains in my kitchen, on my step and on top of my filing cabinet. I always tell myself, “Tomorrow, I’ll take care of this.” We’ve heard that old line before, tomorrow never comes.

Being a nerd, I wanted to see if I could just store all of my mail on my Mac. [Hard drive space is so insanely cheap](https://www.amazon.com/s/?_encoding=UTF8&camp=1789&creative=390957&field-keywords=hard%20drives&linkCode=ur2&sprefix=hard%20drives%2Caps%2C158&tag=clearstaticor-20&url=search-alias%3Dcomputers) that it is possible to really change the way I deal with my paper tidal wave. I attempted a bunch of possible solutions. I will cut to the chase and show you what I settled on. You can adapt what I’m showing you here to suit your own needs. Since this is a Mac related article, obviously you need a Mac. Although the basics of what I’m going to show you you can do with a PC.

## Paper Meet Scanner

Good, so you passed the first test. You have a Mac. Now, you need to get the paper into your Mac. There are a couple of ways you can do this.

### All in one printer / Flatbed Scanner

There are a boat load of all in one scanner’s on the market. They will usually do one sheet at a time on a flatbed scanner. The slightly more high-end ones will do double sided sheets and have a document feeder. I use a [Canon MG3120](https://www.amazon.com/gp/product/B005TI2Q8M/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B005TI2Q8M&linkCode=as2&tag=clearstaticor-20) that works quite nicely and does a reasonably good job of scanning documents. Like a smartphone, the biggest drawback to this is speed.

### A Full On Document Scanner

This is the Mac-daddy if you want to scan paper into your Mac. These scanners are designed to cut through stacks of documents like butter. There are a few on the market. I’ve personally looked at scanners made by [Neat](https://www.amazon.com/gp/product/B001CQ8ER2/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B001CQ8ER2&linkCode=as2&tag=clearstaticor-20) and the [Fujitsu](https://www.amazon.com/gp/product/B00ATZ9QMO/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00ATZ9QMO&linkCode=as2&tag=clearstaticor-20). Both of these will rock your world. For my digital filing cabinet, I use the Neat Scanner. As of 2016 they updated their software to a SaaS model. I think if I were to look at a scanner again I’d go with the Fujitsu. It’s wireless and supports my workflow better. While the specs between each vary, my Neat Scanner can hold up to 50 pages and scan at about 24 pages per minute. When you are ready to go all in, I seriously recommend picking one of these babies up.

### Use a Smartphone

I started out experimenting with my digital filing cabinet by using my iPhone. I would take a photo of a document and store it on my Mac. If you are just playing around and already have an iPhone you can do everything I’m going to show you. The only drawback is scanning speed and quality. In order to do this, I use [JotNot](https://www.jotnot.com/scanner.html), JotNot will take a photo and post process it so that it looks like a scanned document. It does a really great job with whiteboards. I've since moved to using an Android Pixel. There are a couple of scanning applications out there. I've tried [CamScanner](https://www.camscanner.com/) but don't like their subscription model. As it turns out the easiest solution is to just use the scanning feature built into the Android Dropbox app. More on that below.

## Software

For the simplest setup, you don’t need to invest a lot in software. If you go with either of the scanners I mentioned above, they come with software for getting paper into your Mac. In addition to that software, for filing you need a few more things.

### Dropbox

[Dropbox](https://www.dropbox.com/) is a wonder. If you don’t have it installed yet on your Mac you are really missing out. Dropbox creates a folder on your Mac. When you use your Dropbox folder your files magically appear on any other device that you have connected to your Dropbox account. It’s an awesome way to share files between Macs, iPhones and iPads.

## Other Optional Software

This software isn’t necessary but it will make your life a little easier. I’ll give you a brief description of each here. Later, when we are talking about the workflow we can dig into how to use each one.

### Hazel

Think of [Hazel](https://www.noodlesoft.com/hazel.php) as your assistant. Hazel watch folders and take actions on files based on rules. I use it to watch for files with specific files names and automatically file them into the appropriate places in my digital filing cabinet.

### TextExpander

[Text Expander](https://smilesoftware.com/TextExpander/index.html) is a utility that will allow you to create keyboard shortcuts to duplicate repetitive tasks. For example, if you find yourself typing your address a lot, create a Text Expander snippet like _addr_then, whenever you type addr, Text Expander will replace it with your full address.

### Goodreader

If you have an iPad, [Goodreader](https://itunes.apple.com/us/app/goodreader-for-ipad/id363448914?mt=8) is an awesome utility to view all of the PDF files you will be creating and storing into your Digital Filing Cabinet. It’s one of those swiss army knife iPad apps that I literally use everyday.

## Prep-work

Before we begin digital filing, let’s get our system established.

### Setup your Scanner Software

If you are using the Neat or Fujitsu scanners the software comes with optical character recognition (OCR) software. This software converts the scanned image into actual text that is indexed by the system. IF you are using the Neat scanner, it is configured to copy everything into it’s internal database. The system I am describing here is based on file folders and Dropbox. Configured the Neat Scanner to use these options by default.

- B&W Text – this setting is used if you intend to only scan paper. Color documents take more time to process and require more storage.
- Normal Processing – This sets the OCR software. Normal processing will only OCR the first page. This is usually enough for indexing.
- Autodetect the document type – the Neat scanner can support different document types. It does a good job of auto detecting though.
- Direct to PDF – Don’t put the files into the internal database, instead just scan them to PDF
- Combined – merge the stack of documents going into the scanner into one PDF file.
- Double-sided – all of the papers are double sided. I leave this set and if I care, clean up the output PDFs later.

### Create your Scan folder

If you haven’t done so already, install Dropbox and create a Dropbox account. Create a folder called “1 Scans” in your Dropbox folder. This will act as your Inbox. I use the number 1 in the title so that it always appears at the top of my sorted list. This is especially useful when you are using this on your mobile device or tablet.

### Create your 1Scans Android Shortcut

If you're using Dropbox on an Android you can create a shortcut to your 1Scans folder right on your home screen. You can then launch Dropbox directly to your scans folder and use the built in scanning feature to digitize your documentation.

### Create your Digital File Cabinet

Create a “Records” folder in Dropbox. This is your filing cabinet. Once this folder is created, organize it anyway you want. Here is the structure that I use. `Dropbox->Records->File Folder (i.e., medical)` Feel free to use whatever structure you think works best for you. Don’t go to crazy with nested organized folders though. Why? Well, remember this is a digital system. When the files are scanned, the software converts that image to text. So, rather than worry about where your last phone bill is, fire up Spotlight and type 2013 April Verizon and your bill will magically appear.

### Naming Conventions

I heard [Merlin Mann speak on a podcast](https://5by5.tv/b2w) a few years ago about how he organizes his files. Until I heard him, I lived in a world where all my files lived in neat folders within subfolders. The problem was, I could never find anything. His suggestion was to put everything into as few folders as possible. He was speaking mainly about text and Word documents. I’m extending here to this system. I name all of my files in the following way. `Date - Description` That’s it, pretty simple. Now I do sometimes break it down further. `Year - Month - cellphone bill` Or if it is a receipt `Year - Month - Day bed bath beyond juicer receipt` The point is, I give each file a date stamp and a good enough description that I can find it later. These files then go into a limited set of folders so I can easily retrieve them later. If I’m ever looking for a file, I can use Spotlight to search for it. I can also get clever and create a Custom search in the Finder that always looks for files with a given name.

### TextExpander

TextExpander can make this process even simpler. I was introduced to this amazing tool after hearing [Merlin Mann](https://www.merlinmann.com/) and [David Sparks](https://www.macsparky.com/) talk about it on a podcast. With TextExpander I define templates for the files that I create often. Then, I only need to type the TextExpander keyword to automatically create consistent filenames. This makes searching and retrieving the data later even easier. Using my cell phone bill as an example. The bill comes every month so I can create a TextExpander snippet (template) as follows. `YEAR - MONTH - cellphone bill` I assign this the keystroke *.cellbill* then when I type .cellbill it expands to 2013–04 cellphone bill saving me the trouble of remembering a consistent name and also typing it in. TextExpander is part of my everyday toolbox. I’ll go into more detail in a later article showing exactly how I do this. _Note: Since I've written this I've switched to [Alfred](https://www.alfredapp.com/). It's text expansion functionality isn't as robust as TextExpander but their pricing model is a little more reasonable._

## Down to Brass Tacks

Ok, so now with all of this software installed and configured, how do you use it? The basic workflow looks like this.

1.  A piece of mail comes in, I put it into a “To-process” wire basket in my kitchen.
2.  Once or twice a week I take those papers into my office.
3.  Scan the document or documents into PDF Files.
4.  Give the files a consistent, descriptive name.
    1.  Use TextExpander snippets for repetitive names otherwise manually name the file.
5.  Store those documents into my 1Scans folder
6.  During my [weekly review](/posts/how-to-implement-gtd-using-onenote/) I move the files in the 1Scans folder to the appropriate place on Dropbox.

### Hazel Rules

Remember before when I said that Hazel was like your personal assistant? Well, with Hazel, you can have it monitor your “Inbox.” Whenever it sees a file named *cellphone bill* it can move it to your “Records” folder. I’ll go into exact detail on just how this works in a later article.

## Wrap Up

We’ve only scratched the service with ways you can get rid of paper and go digital. There is more you can do with AppleScripting and your iPad that will blow your mind. I’ll save that for later. For now, try some of this out even if you just get started with Dropbox and an iPhone. I’d love to hear how you make out in the comments. I’ve pulled this workflow together through trial and error and by scouring for tips on the Internet. If you want to dig in deeper you can check out the book Paperless by David Sparks . It goes a little deeper on how to choose scanners, and also gives some other suggestions for software you can use.[https://www.macsparky.com/paperless/](https://www.macsparky.com/paperless/) Have fun with this and welcome to your new paper free lifestyle. Do you have any tips for how to organize your mountains of paper? I'd love to hear about it in the comments.
