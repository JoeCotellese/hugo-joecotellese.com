---
title: Stop Fighting Your Product Roadmaps
description: Focus on thematic outcome based roadmaps to break out of the feature-factory mindset.
date: 2023-06-07T17:12:32.711Z
preview: ""
draft: false
tags: []
categories: []
lastmod: 2023-12-23T12:54:17.012Z
---

Imagine we’re taking a trip cross country and we’ve plotted out our course on a map, yeah, the paper kind. Stick with me a minute here. Somewhere around Colorado we find out that a massive storm is supposed to hit. Some locals tell us that our vehicle isn’t going to make it over the mountain passes. They suggest taking another way, or even stopping for the night to ride out the storm. 

Do we listen to the locals and change our plan, or do we blindly charge our way through the storm?

If we used actual roadmaps the same way we use product roadmaps, we might just keep driving. It’s all about miles traveled – better to keep inching forward than to stop, right?

We can probably all see the disconnect here. And this kind of thing happens with product roadmaps all the time. In fact, it has probably happened to you.

Let’s take a look at why so many organizations get derailed by the very thing that’s supposed to keep them on track, and then discuss how can we, as product managers, can keep everyone’s eyes on the goal instead of the to-do list.

## Focusing on Outputs Instead of Outcomes

Executive stakeholders often want to see a roadmap plotting out the course over the next year. As product managers, we grudgingly go through the exercise of filling up a roadmap – laying out projects side by side over the course of the year. It might look something like this. 

 ![](roadmap.jpg)

Success is often pegged to the output – how many of these roadmap projects are completed.

Sure, we caveat this roadmap by saying things like “We may need to change things later,” or “I don’t know how long this will actually take.” Nevertheless, once published, organizations often treat it as the path forward rather than a potential path. 

We drive on down that road, oblivious to the reality that there is a snowstorm looming in Colorado.

So what happens next? In the interest of transparency, the roadmap is published throughout the organization.

The team looks at it and says “How can we do project feature 4 in Q3 in only six weeks? We don’t even know what feature 4 is!” So, you end up having the same conversation. “We may need to change things later,” or “I don’t know how long this will actually take.”

My point is that publishing long term roadmaps the way most organizations do it is potentially damaging to the culture and bottom line. Here’s why.

### It Disappoints the Team

Stacked roadmaps set the team up for failure. The caveats you delivered with your roadmap are quickly forgotten as time marches on. Teams feel the stress of needing to get something done quickly. Executives stakeholders get annoyed because “We’re behind schedule and we’re not hitting our goals.”

### It Breaks Your Process

The Lean process focuses on small iterations of build-measure-learn. If your roadmap is pre-planned and tightly packed, you will sacrifice the time you should be taking to measure and learn in order to race to start the next project feature.

Stacked roadmaps ultimately lead to an organization that is focused on “being busy” but that is ultimately sacrificing outcomes for outputs. Your roadmap becomes a glorified to-do list.

## There’s a Better Way

Now that we’ve identified the problem, let’s look at how to solve it.

### 1. Focus on Outcomes

We need to shift the conversation around what the projects on the roadmap represent. Success at the end of the year shouldn’t be measured on “How many roadmap projects are completed?” Instead, it should be “Did the projects have a positive impact on the business?”

The easiest way to begin having those conversations is to have a clear understanding of the goals that the organization is trying to achieve. Executive stakeholders and product managers should ask the question: “Roadmap items aside, what are the metrics, and how much do we need to drive these numbers to be successful?”

So that might look like this:

![](Screenshot%202023-06-07%20at%201.20.05%20PM.png)

This decouples the outcomes you need to achieve from the individual outputs that will get you there. 

### 2. Use Thematic Roadmaps

Once you know the outcome you’re trying to achieve, divide your roadmap into quarterly themes based on those goals.

Group items in your backlog by those goals. If you use a tool like JIRA you can just label them. Then, further refine the backlog by using your scoring system of choice to rank each of the grouped items. 

This gives you a roadmap that looks like this:

![](Screenshot%202023-06-07%20at%201.20.54%20PM.png)

The features in quarter one aren’t organized by time, instead they are organized by priority. Laid out this way the conversation changes from “You don’t know how long feature 1 will take,” to “Feature 1 is our top priority. We’re going to deliver it then decide whether to iterate on it or move to the next feature.”

### 3. Leave Space

When you take time out of your roadmap, you focus on the measure and learn parts of the Lean cycle. What does that look like, exactly?

Put the projects you think are going to most impact your goals at the top of this list.

When feature 1 launches, involve the entire team in the measure and learn steps. Then, as we said above, you can decide as a team whether to iterate on the feature or move to the next feature.

Ultimately, you may not complete all the features you listed for every quarter – but that shouldn’t be your goal anyway. Instead, your goal is to focus on intentionally working through the entire Lean process on your high-priority items. If lower-priority features get bumped to another quarter, that’s not a fail. It’s just part of your process.

This will also have the added benefit of making everyone feel accountable for the outcome, rather than just output.

## Better Roadmaps Lead to Better Culture

As a product manager you are responsible for two products – the thing you ship and the culture in your organization.  There’s no denying that roadmaps have a huge impact on company culture. I’ve seen organizations improve the morale and engagement of the team by implementing just some of the ideas here. A more engaged team is a team that has less turnover. Less turnover leads to more effective teams that operate at a higher velocity.

Reframing questions about output and outcomes can help ensure that executive stakeholders and engineering teams are all prioritizing the same things.