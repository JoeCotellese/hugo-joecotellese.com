---
title: Stay on task with macOS Shortcuts
description: macOS Shortcuts with Fantastical and HeyFocus keeps me on task.
date: 2022-07-13T12:37:19.669Z
preview: ""
draft: "false"
tags: ""
categories: ""
lastmod: 2022-07-13T13:20:33.820Z
---

For effective work, you need to get into "[the zone](https://amzn.to/3RvAbqI)." This is hard for me when I'm working at my computer. The call of social media is sometimes too great for me to resist. 

Recently, I've turned to automation via [Shortcuts](https://support.apple.com/guide/shortcuts-mac/intro-to-shortcuts-apdf22b0444c/mac), introduced in macOS Monterrey. I think I've finally hit on a process that works for me. In this post, I'll break down how I do it. 

<!--more-->

>I'm using two third party apps to help me, [Fantastical](https://flexibits.com/fantastical) and [Focus](https://heyfocus.com). Both are excellent but not necessary. You can do the same thing with the built in Mac Calendar app.

## How do I want to work?
I want to make sure that I'm not distracted by notifications, social media, etc. I also want to keep track of those focus blocks on my calendar so I can look back at the end of the week to see how well I've done. To do this, I am going to glue a couple of tools together using macOS Shortcuts. 

### Focus for 25 minutes
I think I work best in 25 minute blocks. This gives me enough time to get in the zone and, if things are going well, I can lose track of time and just keep going. This is also the suggested length of focus time if you are following the Pomodoro technique.

Here's how I set it up in Shortcuts

![macOS shortcut focus time example](/img/macos-shortcut-25-minute-focus.png)

Let's break it down.

1. Create a variable representing the length of a focus session. You can set this to whatever you want.
2. Create a date adjustment that adds that variable to the current date and time
3. Then, we create an event called Focus in Fantastical
4. Turn on Do Not Disturb for the focus length
5. Call HeyFocus for the focus length

I then add it to the Menu Bar by tapping "Details" and ticking "Pin in Menu Bar"

![](/img/shortcut-details-pin-menubar.png)

Whenever I want to focus on work, I launch the shortcut via the Menu Bar.

![](/img/shortcut-launch-via-menubar.png)

This has really been a game changer for me and helped me stay on task throughout my day. 

I invite you to try this Shortcut for yourself. Hopefully, you will find that after a few weeks you can look back and see just how productive you are.