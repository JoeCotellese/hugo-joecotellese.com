---
title: Getting Things Done Book Outline
date: Sun, 11 Nov 2018 14:33:11 +0000
draft: false
slug: getting-things-done-book-summary
tags:
   - books
lastmod: 2024-02-08T18:55:17.676Z
---

I’ve been a practitioner of the book [“Getting Things Done” by David Allen](https://amzn.to/2DyEZbW) for years. I reread the book every few years.

Here is the Getting Things Done book outline that I use during my regular review process. I hope it helps you be more productive.

## Part 1 - The Art of Getting Things Done

Part 1 provides a broad overview of the GTD technique.

## 1 - A New Practice for a New Reality

1. **The Power of Being Fully Present**: Emphasizes the importance of being completely engaged and present in the moment, despite numerous tasks and commitments.

2. **Achieving Productive Functioning**: Discusses achieving a state of clear-headedness and relaxed control amidst a multitude of tasks, leading to high effectiveness and efficiency.

3. **Three Key Objectives for Productivity**:
   - **Capture**: Record all tasks and ideas outside your head into a trusted system.
   - **Clarify**: Make decisions on all inputs to maintain a manageable list of actionable next steps.
   - **Coordinate**: Manage these tasks by recognizing and aligning them with your various commitments.

4. **Modern Work Challenges**:
   - **Lack of Clear Boundaries**: Work in the knowledge era lacks clear edges, making it hard to define when a task is truly complete.
   - **Information Overload**: The availability of endless information can be overwhelming.
   - **Rapid Technological Change**: Constant technological advancements create new demands and stress.

5. **Shifting Job Nature**:
   - **Evolving Responsibilities**: Jobs and personal responsibilities are continually changing, demanding adaptability.
   - **Increased Complexity**: Today's work often requires cross-divisional communication and collaboration.

6. **Inadequacy of Traditional Methods**:
   - Traditional time management and organizational methods are outdated in the face of modern work’s speed, complexity, and changing priorities.

7. **Need for New Methods**:
   - Advocates for new ways of thinking, technologies, and work habits to manage work effectively in a complex, fast-paced environment.

8. **Application Beyond Work**:
   - The principles and methods are applicable to personal life, not just professional work.

## 2 - Getting Control of Your Life: The Five Steps of Mastering Workflow

### **Overview of the Five-Step Method**

- The five-step method is essential for mastering relaxed and controlled engagement in work and life.
- It's not merely about getting organized or setting priorities; these outcomes result from applying the five steps.

### **The Five Steps Explained**

1. **Capture**: Identify and gather everything that has your attention. This involves noting down all tasks, ideas, and commitments.

- make note of anything incomplete in your world
- if you say “should”, “need to” or “ought to” about a thing, it’s incomplete (i.e., I ought to figure out what to do with this old iPhone on my desk)
- get those things into containers
- get it out of your head
  
2. **Clarify**: Determine the meaning of each item captured and decide on the necessary actions. This step involves making decisions about what each item represents and how to handle it.

- The decision model for clarifying actions
  - What is it?
    - *Note: There is a flow diagram in the book.*
  - Is it actionable?
    - Yes then…
      - put it into a projects list or
      - determine the next action to move this thing to done.
    - No?
      - trash it
      - or file it
  - There are three things you can do with actionable items:
    - Less than 2 minutes? Do it
    - Delegate it
      - I put something on a “waiting for” list when I delegate it
    - More than 2 minutes Defer it
      - goes into Next Action list
  
3. **Organize**: Arrange the results of the clarification process. This step involves placing things where they belong, based on their priority and relevance.

- There are 8 categories that all your processed stuff goes in.
  - Non-actionable items
    - Trash
    - Incubation
    - Reference
  - Actionable things?
    - List of projects
    - Storage or files for project plans
    - Calendar
    - List of next actions
    - List of waiting for
- Your lists belong in something
  - I use Omnifocus for this your system may vary
  - Incubating things can go in your
    - calendar
    - tickler file

4. **Reflect**: Review and assess the options presented by your organized system. This step is about regularly checking your organized lists and plans to stay current.

- What do I review?
  - If you follow guidelines - Prj list, calendar, NAs, Waiting For, not much maintenance is needed.
  - Calendar - hard landscape, must get done stuff
  - NA list
  - Project / Waiting For / Someday Maybe as needed

5. **Engage**: Choose to act on the tasks. This step involves actually doing the tasks based on the organized system and priorities.

### **Application of the Method**

- These steps are universally applicable, whether in personal or professional settings.
- They are not just theoretical but are practical steps we all use instinctively to gain control and stabilize situations for productive action.

### **Common Weaknesses and Challenges**

- Many people struggle with the capture process, often failing to record all their commitments and ideas.
- There is often a lack of clarity regarding captured items, leading to inaction and stress.
- Organizational inefficiencies often arise due to poor management of the clarified items.
- Reflecting on the organized content is crucial but often neglected, leading to outdated systems and last-minute pressures.
- Engaging effectively depends on the strength of the previous four steps. Decisions on what to do are often influenced by immediate pressures rather than informed choices.

### **Importance of Integration and Consistency**

- All five steps must be integrated and supported with consistent standards for effective workflow management.
- Avoiding mastery of these steps can lead to increased complexity and stress due to the rapid pace of information and change.

By mastering these steps, you can achieve a sense of control and productivity, effectively managing your tasks and responsibilities

## 3 - Getting Projects Creatively Under Way: The Five Phases of Project Planning

1. **Introduction to Vertical Focus**:
   - Vertical focus is required for rigor and control in complex projects.
   - It complements the horizontal focus (defined outcomes and next actions) by adding depth to project planning.

2. **Importance of Informal Planning**:
   - Most planning is informal, like 'back-of-the-envelope' planning.
   - Formal planning and project management tools are useful but often insufficient alone.

3. **The Natural Planning Model**:
   - The brain's natural planning process involves five steps:
     1. **Defining Purpose and Principles**: Establishing the aim and guidelines for the project.
     2. **Outcome Visioning**: Envisioning the desired end result.
     3. **Brainstorming**: Generating a wide range of ideas.
     4. **Organizing**: Structuring the generated ideas into a coherent plan.
     5. **Identifying Next Actions**: Determining immediate action steps.

4. **Applying the Model to Real Situations**:
   - The model is applicable in everyday scenarios, such as planning a dinner.
   - It emphasizes clarity in the project's purpose and envisioned outcome.

5. **The Unnatural Planning Model**:
   - Contrasts with the natural model, often starting with seeking good ideas without clear purpose or vision.
   - Can lead to inefficiency and stress.

6. **Detailed Exploration of the Five Phases**:
   - **Purpose**: Defines success and creates decision-making criteria.
   - **Principles**: Sets the boundaries and standards for the project.
   - **Vision/Outcome**: Provides a clear picture of the project's successful completion.
   - **Brainstorming**: Encourages the generation of ideas without immediate judgment or analysis.
   - **Organizing**: Involves structuring the brainstormed ideas into actionable components.

7. **Implementation and Action Steps**:
   - Determining the next action is crucial for moving the project forward.
   - Projects need as much planning as necessary to get them off one's mind, ranging from simple lists to detailed planning sessions.

8. **Effectiveness of Natural Planning**:
   - Leveraging the natural planning model can make project evolution easier, faster, and more productive.
   - Emphasizes that knowing how to plan is different from actually doing it effectively.

These notes capture the essence of Chapter 3, focusing on the importance of both informal and formal planning, and the natural thought process that underlies effective project management.

### End of Part 1

- None of this stuff required new skills to increase your productivity, just systematic behaviors
- just knowing how to do this doesn’t product results
- you need a coach
- that’s in part 2

## Part 2 - Practicing Stress Free Productivity

- Move from conceptual framework to full-scale implementation and best practices
- follow along in logical sequence
- read it multiple times

## 4 - Getting Started: Setting Up the Time, Space, and Tools

Chapter 4 of "Getting Things Done: The Art of Stress-Free Productivity" by David Allen focuses on the concept of managing both the big picture and the finer details in life and work. Here are the key points from this chapter:

### The Big Picture vs. the Nitty-Gritty

The chapter begins by discussing the importance of balancing high-level goals and values with day-to-day tasks and commitments. It emphasizes that while focusing on major goals and values is crucial, it doesn't reduce the number of tasks or challenges one faces. In fact, it often increases them.

### The Problem with Only Focusing on High-Level Goals 

Three primary reasons are given for why focusing only on high-level goals can fail:

- Distraction at the day-to-day level hinders focus on higher levels.
- Ineffective personal organizational systems lead to subconscious resistance to bigger projects.
- Clarifying higher-level values and goals raises standards, making us aware of more areas that need change.

### The Promise of a 'Ready State'

The chapter introduces the concept of a "mind like water" state, akin to the 'zone' experienced by top athletes. This state is described as one of clear-mindedness, where one is fully engaged, relaxed, and productive. The goal is to manage work and life with minimal effort and maximum efficiency.

### Achieving a 'Mind Like Water' State

This state is achievable through a system that manages both the big picture and the minute details effectively. It requires a balance of relaxation and responsiveness, akin to the way water responds perfectly to its environment. The key is not to overreact or underreact to situations but to maintain a state of readiness and balance.

### The Challenge of Maintaining Productivity

The chapter stresses the importance of being able to enter a highly productive state when needed. It questions whether one can regain control, focus, and productivity after falling out of this state. The methodology of "Getting Things Done" is presented as a solution to help regain this productive state.

### Relating to Real-Life Examples

To make the concepts more relatable, the chapter uses metaphors from martial arts and rowing. These metaphors illustrate the principles of balance, readiness, and responsive action in achieving effectiveness in both work and personal life.

## 5 - Capturing: Corralling Your “Stuff”

### What Is It?

Chapter 5 in "Getting Things Done" addresses the foundational step of capturing or corralling all the "stuff" you need to deal with. This includes any form of input, like emails, voice mails, memos, meeting notes, and self-generated ideas. The chapter emphasizes that effective personal organization starts with understanding what your "stuff" is and determining whether it's actionable or not.

### Is It Actionable?

In determining if an item is actionable, there are two possible outcomes: yes or no.

- **No Action Required**: If something is not actionable, it falls into one of three categories: trash, incubation, or reference. These categories need to be managed appropriately using a wastebasket and delete key for trash, a tickler file or calendar for incubating ideas, and a filing system for reference information.
  
- **Actionable**: For items that require action, two key things need to be decided. First, if it's related to a project, it should be added to a "Projects" list to keep track of it until completion. Second, the next physical, visible activity (the "next action") that moves this item towards completion should be determined.

### Organizing

The organization involves sorting and storing items based on their meaning to you. The process creates a system consisting of various categories, including:

- **Projects**: A list of ongoing projects that require attention.
- **Next Actions**: A list of immediate actions to be taken.
- **Waiting For**: A list of items you're waiting on from others.
- **Incubation and Reference**: For nonactionable items, a system for future reassessment or reference.

Organizational tools can range from paper-based systems to digital applications, but the key is that they must be reviewable and match the significance of the items to your life and work.

## 6 - Clarifying: Getting “In” to Empty

### Overview

This section discusses the process of clarifying and organizing the items you've captured. It's important to understand what each item is, its relevance, and the actions it necessitates. The chapter underscores the distinction between processing and doing, emphasizing that getting “in” to empty is about identifying and deciding on actions, not necessarily completing them.

### Processing Guidelines

The process involves a few key rules:

- **Process the top item first:** This rule is crucial for maintaining focus and efficiency. It ensures that every item gets equal attention and is processed systematically.
- **Emergency Scanning Is Not Clarifying:** Emergency scanning, or looking for urgent items first, is different from processing. The latter requires going through each item in order, without skipping or favoring.
- **LIFO or FIFO?:** Whether you process the last or the first item in your inbox doesn't matter as much as ensuring that everything is processed within a reasonable time frame.
- **One Item at a Time:** Focus on processing one item at a time. This prevents the creation of side piles and ensures each item is adequately addressed.
- **The Multitasking Exception:** Some people may need to shift focus briefly between items to decide effectively. This is an exception, not the rule.
- **Nothing Goes Back into “In”:** Once an item is taken out of “in,” it should be processed and not returned. This prevents decision fatigue and keeps your system efficient.

### The Key Processing Question: “What’s the Next Action?”

Determining the next action for each item is essential. This step involves deciding whether there is an action associated with the item and, if so, what it is. 

- **What If There Is No Action?:** If an item requires no action, it falls into one of three categories: trash, incubation, or reference material. Each category has its own way of processing.
- **And If There Is an Action . . . What Is It?:** If an item requires action, you need to determine the specific next physical step. This decision leads to three options: do it (if it takes less than two minutes), delegate it, or defer it into your organization system for later execution.

### Identifying the Projects You Have

The final step in processing is to identify your projects - outcomes that require more than one action step to complete. It’s essential to list all your projects, ensuring that you have placeholders for all open loops. This helps in maintaining a week-to-week control of your life and work.

## 7 - Organizing: Setting Up the Right Buckets

### Introduction

The chapter emphasizes the power of a seamless organization system to elevate your mind to intuitive focusing by releasing the burden of lower-level thinking. The key is aligning the physical organization system with your mental one.

### The Essence of Being Organized

Organization is not complex; it's about matching the location of items with their significance to you. However, this simple concept requires you to clarify what each item means to you. This chapter delves into refining this process and developing organizing tools and steps as you process your "in-tray".

### The Evolution of Your Organization System

Your organization system is dynamic and evolves as you process your stuff. It's not static and adapts as your understanding of what things mean to you changes over time. The chapter guides you through this evolutionary process.

### The Importance of Hard Edges

Maintaining distinct categories in your organization system is crucial. These categories represent different types of agreements with ourselves and should not blend into one another to maintain the value of organizing.

### Organizing Action Reminders

The chapter discusses how to organize reminders for actions that can't be delegated and are longer than two-minute tasks. It also covers the management of reminders for things you've delegated and the importance of categorizing them correctly.

### The Actions That Go on Your Calendar

This section covers the two types of actions: those that must be done on a specific day/time and those that need to be done as soon as possible. The calendar should only show "hard landscape" actions.

### Organizing As-Soon-As-Possible Actions by Context

Organizing actions based on context (where or with what tools they can be done) is emphasized. Categories like Calls, At Computer, Errands, and At Office are examples provided.

### The Most Common Categories of Action Reminders

Common list headings for next actions, like Calls, At Computer, Errands, are discussed. These lists help focus and pick the best action in a given moment.

### All You Really Need Are Lists and Folders

The chapter concludes that lists and folders are sufficient tools for organizing actions, reference, and support materials. Proper management of these tools is key to a clear mind and effective organization.

### Organizing Project Reminders

The significance of having a complete Projects list is highlighted. It is a major tool for bridging day-to-day activities with larger commitments.

### Organizing Nonactionable Data

This section covers how to handle nonactionable items like reference materials, reminders of possible future actions, and trash. Proper management of these items is essential for a clutter-free mind.

### Using the Original Item as Its Own Action Reminder

The efficiency of using some paper-based materials and emails as their own reminders is discussed. This method is particularly useful for certain types of actionable items.

### Organizing Ad Hoc Project Thinking

The chapter advises on organizing random project ideas and thoughts, suggesting various tools and methods like attached notes, digital organization, and paper-based files.

### Organizing “Waiting For”

Managing commitments from others is crucial to avoid crisis. This involves tracking deliverables or projects others are responsible for, such as ordered items or approvals.

### Checklists: Creative and Constructive Reminders

The role of checklists in managing projects, work processes, and personal interests is emphasized. They are portrayed as essential tools for ensuring nothing is missed in various situations.

## 8 - Reflecting: Keeping It All Fresh and Functional

### Introduction

This chapter emphasizes the importance of regularly engaging with all your commitments and activities to maintain focus and clarity. A thorough review process is essential to keep your system current and functional, ensuring that your brain can rely on it for organization and free up mental space for more creative activities.

### The Necessity of a Dynamic System

Your organizational system must be dynamic and regularly updated to support appropriate action choices. It should facilitate consistent and proper evaluation of your life and work at various levels. The key is to regularly review and reflect on your system.

### What to Look At, When

This section focuses on the need for your personal system to allow visibility of all action options when necessary. Frequent review of your daily calendar and action lists is critical. The right review in the right context helps in making informed decisions about your actions.

### Updating Your System

To maintain trust in your system, it's crucial to keep it updated. Falling behind in updating your system will force your brain to re-engage in lower-level tasks like remembering and processing, which it doesn't do efficiently.

### The Power of the Weekly Review

The Weekly Review is a crucial process to regain control and focus. It involves capturing, clarifying, organizing, and reviewing all outstanding commitments, intentions, and inclinations until you have a complete overview of what you're not doing but could be doing.

### Get Clear, Get Current, and Get Creative

The Weekly Review consists of three parts: getting clear (gathering and processing loose ends), getting current (updating all lists and calendar data), and getting creative (generating new ideas and perspectives). 

### The Right Time and Place for the Review

Choosing the right time and place for the Weekly Review is important. For many, the end of the workweek is ideal. However, individual lifestyles may require different scheduling. Consistency in conducting these reviews is key.

### The “Bigger Picture” Reviews

This section emphasizes the importance of occasionally reviewing your goals and objectives at higher horizons. Regular engagement at these levels ensures alignment with broader life and career aspirations.

### Flexibility in Future-Thinking

The chapter concludes with the importance of staying flexible and informal about goal setting. Adapting a dynamic and responsive approach to future planning, similar to agile methodologies in software development, is recommended for effective execution and adaptability.

## 9 - Engaging: Making the Best Action Choices

### Trusting Your Intuition

The chapter begins by emphasizing the importance of trusting your intuition when deciding what to do in your day-to-day work. Intuitive decision-making, as opposed to reactive choices, is key.

### The Four-Criteria Model for Choosing Actions in the Moment

#### Context

Your actions depend on your current context, including your location and available tools. Organizing action reminders by context, such as Calls, At Home, At Computer, Errands, and so on, can streamline your decision-making process.

#### Time Available

The amount of time you have before your next commitment also influences your choice of action. Knowing your available time helps in selecting appropriate tasks to complete.

#### Energy Available

Your energy levels at different times of the day should guide your choice of activity. When energy is low, choose tasks that require less mental effort.

#### Priority

After considering context, time, and energy, the final criterion is priority. This involves making conscious decisions about your goals and values, and aligning your actions with them.

### The Threefold Model for Evaluating Daily Work

This model categorizes work activities into three types: doing predefined work, doing work as it shows up, and defining your work. Balancing these activities is crucial for effective time management.

### The Six-Level Model for Reviewing Your Own Work

This model provides a hierarchical view of work, ranging from immediate actions (ground level) to life goals (highest level). Each level should align with and support the levels above it.

### Working from the Bottom Up

The practical approach to prioritizing involves starting with the most immediate tasks (ground level) and working upwards. This method clears the mental clutter, allowing for a better focus on higher-level goals and visions.

### Handling Surprises and Interruptions

Effectively managing unexpected tasks and interruptions is crucial. Having a well-organized system allows you to bookmark tasks and switch focus as needed, maintaining productivity even in the face of surprises.

### Balancing Workflow

Understanding and balancing the various aspects of your workflow, from daily tasks to long-term goals, is key to maintaining a healthy work-life balance. Regular reviews at each level ensure alignment and focus.

### Conclusion: Engaging Effectively

The chapter concludes by encouraging the reader to capture and process thoughts and ideas that arise while engaging in different work levels. This process helps in aligning actions with overall life goals and maintaining a state of readiness for new challenges and opportunities.

## 10 - Getting Projects Under Control

### The Need for More Informal Planning

The chapter stresses the importance of informal planning for projects. It suggests that more creative and proactive thinking could alleviate pressure and enhance output. The focus is on setting up systems that encourage thinking about projects more frequently and in depth.

### What Projects Should You Be Planning?

Projects that either still demand attention after defining next actions, or those that spontaneously generate ideas, need planning. For the former, detailed planning using the natural planning model is necessary. For the latter, a system to capture and store ad hoc ideas is essential.

### Projects That Need Next Actions About Planning

Some projects require more objectified and detailed planning. Steps like brainstorming, organizing, setting up meetings, and gathering information are crucial for these projects. Each step should have a defined next action to progress the planning process.

### Typical Planning Steps

#### Brainstorming

This step involves thinking freely about a project to generate ideas. Deciding the medium (digital or paper) for brainstorming is crucial.

#### Organizing

Sorting through existing notes and materials to structure them is a key step. It’s essential to determine where this organizing will take place (office, digital tool, etc.) to know where to list this action.

#### Setting Up Meetings

Sometimes, the next step in project planning is to arrange meetings with relevant people. This could involve sending emails or making phone calls to schedule these discussions.

#### Gathering Information

Collecting additional data or input for a project often constitutes a crucial planning step. This could involve conversations, reviewing files, or online research.

### Random Project Thinking

Ideas can occur unexpectedly. It's important to have a system to capture these random thoughts, using tools like notepads or digital devices, ensuring no idea gets lost.

### Tools and Structures That Support Project Thinking

#### Thinking Tools

Good writing tools and paper should be readily available to capture thoughts. Whiteboards and digital tools like laptops can also facilitate idea generation.

#### Support Structures

Having physical folders or digital spaces for each project is crucial to store and organize ideas and notes. A mix of paper and digital tools can be used, depending on personal preference and the nature of the project.

#### Software Tools

Various software tools can aid in project planning. Mind-mapping and outlining applications are particularly useful for informal planning. More complex project management software might be necessary for detailed and large-scale projects.

### Applying Project Planning in Your World

To apply these methods, keep your Projects list updated and set aside time for vertical thinking on each project. Choose the most suitable tools and methods for each project and focus on organizing and developing your ideas. Regularly review and update your project plans to ensure they remain aligned with your objectives.

## End of Part 2

## Part 3 - The Power of Key Principles

## 11 - The Power of the Capturing Habit

## The Power of the Capturing Habit

1. **Introduction to Capturing Habit**
   - Techniques for keeping the mind distraction-free.
   - Enhances efficiency and effectiveness at work.
   - Impacts both the individual and organizational culture positively.

2. **Trust and Confidence through Capturing**
   - Consistent management of agreements builds trust.
   - Capturing enhances self-confidence and mental well-being.
   - Improves communication and relationships in both personal and professional settings.

3. **Organizational Impact**
   - When organizations enforce capturing, productivity increases and stress decreases.
   - Accountability and clear tracking of commitments are crucial.

4. **Emotional Effects of Capturing**
   - Mixed feelings of anxiety and relief during capturing process.
   - Negative feelings arise from broken agreements with oneself.
   - Recognition of the source of negative feelings is key to overcoming them.

5. **Dealing with Negative Feelings**
   - Three ways to handle negative feelings: not making an agreement, completing it, or renegotiating it.
   - Importance of conscious decision-making in agreement creation.
   - The relief experienced in discarding unnecessary commitments.

6. **Rethinking Commitments**
   - Being cautious about making unnecessary internal commitments.
   - Importance of maintaining an inventory of work to manage commitments better.

7. **Completing Agreements**
   - Satisfaction from completing tasks.
   - The psychological benefit of accomplishing small tasks, like the two-minute rule.

8. **Renegotiating Agreements**
   - Importance of renegotiating instead of breaking agreements.
   - The psychological burden of unremembered agreements.
   - Internal stress from not acting on self-made agreements.

9. **Departure from Traditional Time Management**
   - Differences from traditional time management techniques.
   - Importance of capturing everything, regardless of perceived importance.
   - The psychological effect of unmanaged commitments.

10. **Capturing Everything**
   - The need to capture even minor reminders.
   - Achieving a state of 'presence' or being in the 'zone'.
   - The focus on thinking about things rather than just thinking of them.

11. **Impact on Relationships and Organizations**
   - Trust and efficiency in teams and organizations.
   - The negative impact of communication gaps.
   - Improved communication systems leading to reduced need for constant supervision.

12. **Cultural Shift in Organizations**
   - The importance of adopting a capture system in organizational culture.
   - Personal systems cannot be legislated but can be encouraged.
   - Holding people accountable for outcomes and managing incoming work.

13. **The Importance of Renegotiation in Knowledge Work**
   - Facilitating a constant renegotiation process.
   - The role of capture systems in managing complex workloads.

14. **Collective Adoption of Capture Standard**
   - Importance for groups to collectively adopt the capture standard.
   - Ensuring efficient energy utilization in the direction the group is heading.

## 12 - The Power of the Next-Action Decision

## The Power of the Next-Action Decision

1. **Introduction to the Next-Action Principle**
   - Emphasizing the adoption of "What’s the next action?" as a global standard.
   - Envisioning a culture where every interaction and meeting concludes with clarity on action steps and accountability.

2. **Impact of Adopting the Principle**
   - Shift in energy and productivity observed in individuals and groups using the principle.
   - Frustration when dealing with people not using this method.

3. **Accountability in Decision Making**
   - Importance of identifying actions and commitments in interactions.
   - Difference between making decisions proactively versus reactively (when things show up vs. when they blow up).

4. **Origins of the Next-Action Technique**
   - Development of the technique by management consultant Dean Acheson.
   - The methodology focused on clarifying actions for executives and has since been widely adopted.

5. **Learning and Applying the Technique**
   - The technique is not innate but learned through decision making and focused thinking.
   - Importance of proactive behavior in incorporating this method into personal and organizational life.

6. **Creating the Option of Doing**
   - The power of determining the next action to move projects forward.
   - The ease of deciding the next steps for tasks with focused thinking.

7. **The Role of Mental Imagery in Procrastination**
   - Intelligent and sensitive individuals often procrastinate due to their ability to vividly imagine potential negative outcomes.
   - The importance of breaking down tasks into smaller, manageable actions to overcome procrastination.

8. **Defining Real Doing**
   - Importance of defining actionable steps at the most basic level.
   - Organizing reminders and actions to enhance productivity and create a relaxed inner environment.

9. **The Problem of Undecided Actions**
   - Many tasks remain stuck due to a lack of decision on the next action.
   - Example of breaking down a task like getting a car tune-up into actionable steps.

10. **Why Bright People Procrastinate the Most**
    - Intelligent people often have a higher number of undecided things due to their creative and sensitive nature.
    - The tendency of smart individuals to create negative scenarios in their minds, leading to procrastination.

11. **Overcoming Negative Imaging**
    - The need to shift focus to actionable tasks to relieve pressure and increase positive energy.
    - The concept of "intelligent dumbing down" by focusing on the next action.

12. **The Value of Next-Action Decision Making in Organizations**
    - Transformative impact on organizational performance and culture.
    - Benefits include increased clarity, accountability, productivity, and empowerment.

13. **Implementing the Next-Action Standard**
    - Encouraging clarity in conversations and meetings by defining next actions.
    - Holding individuals responsible for their commitments in collaborative environments.

14. **Productivity Through Front-End Decision Making**
    - Improving organizational productivity by making decisions on actions early.
    - The inefficiency and stress caused by delaying action decisions.
  
15. **Empowering Individuals through Action**
    - The empowerment that comes from taking control and acting proactively.
    - Overcoming victim mentality by focusing on changeable actions and accepting unchangeable circumstances.
  
16. **Conclusion: Making Things Happen**
    - The positive cycle of belief and action: believing in the ability to make things happen leads to more action, which in turn reinforces belief.
  
## 13 - The Power of Outcome Focusing

1. **Introduction to Outcome Focusing**
   - The concept involves directing mental processes to effect change.
   - Emphasis on practical application: facilitating task accomplishment with less effort.

2. **Focus and the Fast Track**
   - Application of the method in day-to-day situations leads to increased personal productivity.
   - Professionals integrating this method experience enhanced careers and lifestyles.
   - The method is most quickly adopted by those already invested in self-development.

3. **The Significance of Applied Outcome Thinking**
   - Defining specific projects and next actions to address quality-of-life issues.
   - Emphasis on clarity and consistent system processing in work and life.
   - Connection between defining outcomes and identifying necessary actions.

4. **Multilevel Outcome Management**
   - The challenge of integrating high-level focus with everyday life.
   - The role of a consultant/coach in eliciting productive responses.
   - Combining high-level vision with management of details.

5. **The Power of Natural Planning**
   - Natural planning as an integrated, flexible way to approach situations.
   - Importance of defining outcomes, brainstorming, and taking action.
   - The model's application across all aspects of life and work.

6. **Shifting to a Positive Organizational Culture**
   - Small changes in key people can significantly impact group productivity.
   - Outcome/action thinking as a required behavior in the modern corporate landscape.
   - Common productivity issues in organizations often revolve around meetings and emails.

7. **Empowerment through Outcome and Action Focus**
   - Individuals and groups gain empowerment by focusing on outcomes and actions.
   - Application of outcome/action principles leads to improvement in atmosphere and output.
   - The principles of GTD reflected in individual behavior influence organizational culture.

8. **Application of GTD in Organizations**
   - GTD principles are as applicable to enterprises as to individuals.
   - Organizations need individuals skilled in GTD to meet demands of the knowledge economy.
   - Training and modeling of GTD from the top down can profoundly impact organizational output.

## 14 - GTD and Cognitive Science

## GTD and Cognitive Science

1. **Introduction to GTD's Cognitive Science Connection**
   - Recognition of GTD principles by cognitive psychology.
   - GTD's effects: clarity, control, and focus.

2. **Frameworks and Categories in Cognitive Science**
   - Positive psychology.
   - Distributed cognition: external mind concept.
   - Cognitive load of incompletions.
   - Flow theory.
   - Self-leadership theory.
   - Goal-striving and implementation intentions.
   - Psychological capital (PsyCap).

3. **GTD and Positive Psychology**
   - Shift in psychology focus towards positive aspects of human condition.
   - GTD aligns with positive psychology in promoting meaningful work and psychological well-being.

4. **Distributed Cognition: An External Mind**
   - Mind's design for pattern recognition, not recall.
   - Necessity of an “external brain” to manage information.

5. **Relieving Cognitive Load of Incompletions**
   - Research by Dr. Roy Baumeister on effects of unfinished tasks.
   - Importance of a trusted plan to engage in future actions.

6. **Flow Theory**
   - GTD's alignment with flow: clarity, control, and focus on singular activities.
   - Enhanced engagement in work and personal life through GTD practices.

7. **Self-Leadership Theory**
   - GTD's connection to self-leadership strategies like self-cuing and mental components.
   - GTD's role in improving self-efficacy and organizational psychology constructs.

8. **Goal-Striving via Implementation Intentions**
   - GTD's role in facilitating goal achievement through clear action steps and context-specific planning.

9. **Psychological Capital (PsyCap)**
   - Four components: self-efficacy, optimism, hope, and resilience.
   - GTD's contribution to developing PsyCap through structured methods and mental clarity.

10. **GTD's Impact on Individuals and Groups**
    - Anecdotal evidence of GTD improving handling of personal and professional challenges.
    - GTD's influence in creating positive organizational culture and enhancing productivity.

11. **Conclusion**
    - Anticipation of continued scientific validation of GTD's effectiveness.
    - GTD's role in allowing the more mature and intelligent part of individuals to flourish.

## 15 - The Path of GTD Mastery

1. **Introduction to GTD as a Lifelong Practice**
   - GTD compared to mastering skills like playing an instrument or a sport.
   - Evolution of work and engagement throughout life.
   - GTD as a means to navigate commitments confidently.

2. **The Nature of Mastery in GTD**
   - Mastery not as a final state, but consistent engagement in productive behaviors.
   - Dealing with disturbances using the “mind like water” concept.

3. **Three Tiers of Mastery in GTD**
   - Employing GTD fundamentals for workflow management.
   - Implementing GTD for total life management.
   - Leveraging GTD skills for expansive expression and manifestation.

4. **Mastering the Basics of GTD**
   - Initial focus on fundamental GTD components.
   - Building proficiency in capturing, clarifying, organizing, and reflecting.
   - Common challenges in maintaining and regressing in basic practices.

5. **Getting Off and Back on Track with GTD**
   - Ease of falling off GTD practices due to life’s challenges.
   - Simplicity in regaining GTD productivity groove.

6. **Graduate Level: Integrated Life Management**
   - Moving from managing daily tasks to addressing bigger life issues.
   - Projects becoming the heartbeat of the operational system.
   - Assessing projects based on roles, accountabilities, and interests.

7. **Mastery of GTD Projects List**
   - Shifting focus from individual tasks to project-driven management.
   - Translating concerns and issues into actionable projects.

8. **An Integrated Total Life-Management System**
   - GTD as a cohesive control room for life’s engagements.
   - Customizing GTD to suit personal needs and direction.

9. **Leveraging GTD in Challenging Situations**
   - Using GTD effectively in high-pressure and unexpected situations.
   - Transitioning from being disrupted by challenges to utilizing GTD to navigate them.

10. **Postgraduate Level: Focus, Direction, and Creativity**
    - Utilizing clear internal space for exploring elevated commitments.
    - Leveraging the external mind for creative value production.
    - Freedom to engage in meaningful activities.

11. **Freedom to Engage in the Most Meaningful Things**
    - Ability to focus on creative and productive activities due to GTD mastery.
    - Impact of GTD on personal creativity and innovation.

12. **Leveraging Your External Mind**
    - Using GTD to trigger creative and productive thinking.
    - The importance of checklists and reminders in fostering creativity.

13. **Advanced GTD Mastery**
    - Integration of GTD practices at all levels of life and work.
    - Mastery reflected in the ability to handle diverse commitments with equanimity.
    - Continuous involvement in various aspects of GTD.

## Conclusion

### Key Takeaways and Tips for Moving Forward with GTD

1. **Reaping Rewards of GTD**
   - The hope that readers have started to experience more efficiency and less stress.
   - Encouragement to enjoy the freedom and creative energy release that GTD can bring.

2. **Validation and Systematic Application**
   - GTD likely validates what readers already know and practice to some extent.
   - The book aims to systematize common sense in an increasingly complex world.

3. **Back to Basics Approach**
   - GTD as a roadmap to achieving positive, relaxed focus.
   - Emphasis on fundamental methods that consistently work, akin to understanding gravity.

4. **Practical Steps for GTD Implementation**
   - Set up a personal physical organization system.
   - Organize your workstation and get necessary in-trays.
   - Create a personal reference system for both work and home.
   - Choose a list-management organizer that is engaging.
   - Feel free to make changes to your work environment to support your new start.

5. **Engaging with GTD in Depth**
   - Dedicate time to thoroughly organize different areas of your office and home using GTD.
   - Share insights from GTD with others as a learning tool.
   - Revisit the GTD book after a few months to gain new perspectives.

6. **Staying Connected and Motivated**
   - Maintain contact with people who embody GTD principles and standards.
   - Continuous encouragement for a productive and fulfilling life journey.

7. **Final Encouragement**
   - Invitation to use GTD as a reference tool for achieving and maintaining productive states.
   - Wishing readers a successful implementation of GTD in their lives.

## Common Questions about GTD

### What tools should I use for GTD?

There are a number of tools you can use to implement Getting Things Done. The most basic way to do GTD is with a note book and paper. If you want to go digital, here are some other ways to do it.

- Omnifocus (Mac)
- [Microsoft One Note](/posts/how-to-implement-gtd-using-onenote/) (Mac, PC, Mobile)
- [Todoist](https://todoist.com) (Web)

### How do I distinguish between a Next Action and a Project?

In the Getting Things Done (GTD) methodology, distinguishing between a Next Action and a Project is quite straightforward:

Project: A project is defined as any outcome you're committed to achieving that will take more than one action step to complete. For example, if you look at tasks like "Call Frank about the car alarm" or "E-mail Bernadette re: conference materials," you may recognize that these tasks are part of larger goals or projects. There's usually more to do after these individual actions, indicating that they are components of a broader project​​.

Next Action: On the other hand, a Next Action is the next physical, visible activity required to move a situation toward closure. It's about identifying what exactly needs to be done next for a specific item on your list. This step might seem straightforward, but it often involves quick analyses and planning steps that need to occur in your mind before you can determine precisely what needs to be done, even for seemingly simple tasks​​.

So, in a nutshell, a Project encompasses a larger goal or outcome requiring multiple steps, while a Next Action is the immediate, tangible step you need to take to move any part of your work forward. Remember, identifying Next Actions helps in breaking down the overwhelming nature of big projects into manageable tasks.

### What is a project in GTD?

- Definition : any result that can happen within a year with more than one action step
- You don’t do a project, only actions related to it

#### Project Support Material

- Your project list is an index
- Reference files you keep out of site
- I use Dropbox folders for reference files, then I can link to them in One Note
- Best practice is to keep digital reference simple as possible, and consistently reviewed and purged.

#### The Next-Action Categories

- NA is central
- next physical, visible behavior on every open loop
- what needs to be tracked?
  - action on specific time / day - calendar
  - as soon as they can - next action list
  - waiting for others? Waiting for list

#### How should I use my Calendar with GTD?

You calendar is for things that have hard deadlines.

- 3 things go here:
  - time-specific actions
  - day-specific actions
  - day-specific information - things you want to know on a specific day.
- no daily to-do lists on calendar, they don’t work
  - priorities change
  - dilutes emphasis on what actually does need to happen on your calendar
  - sacred territory, what’s on there gets done that day or not at all

#### What things should I review when I do my weekly review?

- If you follow guidelines - Prj list, calendar, Next Actions, Waiting For, not much maintenance is needed.
- Calendar - hard landscape, must get done stuff
- Your next action lists
- Project / Waiting For / Someday Maybe as needed

#### What is the purpose of the Weekly Review?

- Critical for success
  - keeps your brain clear
  - review your entire system once a week
- Process
  - Gather and process your stuff
  - review your system
  - update your lists
  - Get up to date on your stuff
- Most people don’t have complete system
- the more complete system, the more you trust it
- weekly review is key to maintaining system

# Fine
If you are a product manager, keeping yourself organized is critical to doing a good job. The book Getting Things Done is how I do it.

If you like this book and want other product manager book suggestions, check out my list of [best product management books.](https://www.joecotellese.com/posts/best-books-for-product-management/)