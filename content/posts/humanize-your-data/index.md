---
title: Humanize Your Data
date: Wed, 23 Aug 2017 20:24:26 +0000
draft: false
lastmod: 2023-12-20T15:39:28.623Z
featuredImage: humanize-your-data.png
---

I've been playing around a lot with the app [Rescue Time](https://www.rescuetime.com), it's a really cool productivity tool that helps keep your workday focused. I've used it for a number of years. Today, I was taking a look at some of my stats and came across this pretty cool statistic. ![](/img/humanize-your-data-before.png)

Wow, I've been productive for over 5 thousand hours. Wait, how long is that? Is that a year? So I popped on over to a time conversion website and plugged the data in. It turns out, it was about 31 weeks out of a total of 49 weeks logged. This data meant a lot more to me then pure hours. I took that information and plunked it into a mockup. ![](/img/humanize-your-data-after.png) This simple change makes the data so much more consumable. So, the lesson here my friends is look at the data you present to your users. If they have to do math in their head you need to simplify things for them. Rock on
