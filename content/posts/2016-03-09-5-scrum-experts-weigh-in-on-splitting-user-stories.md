---
title: 5 Scrum Experts Weigh in on Splitting User Stories
date: Wed, 09 Mar 2016 12:33:23 +0000
slug: 5-scrum-experts-weigh-in-on-splitting-user-stories
draft: false
contenttypes:
  - BlogPosting
lastmod: 2023-03-09T18:03:06.625Z
---

User Stories are the building blocks used by Scrum teams to develop working software. When stories start out they often describe large areas of functionality that can't be completed in one sprint.

Beginning Scrum teams often fall into the trap of breaking the stories down by technical area (i.e., front end work, back end work, database work, etc.) The problem with this approach is that the resultant stories don't deliver value to a customer. It also causes integration work to happen "at the end."

So, how does one approach the topic of splitting user stories? In the posts below, Scrum experts weigh in on some best practices.

[Gojko Adzic](https://twitter.com/gojkoadzic) advocates splitting up stories using the Hamburger Method using User Story Maps.

> Inexperienced teams often can’t get their heads around splitting stories into smaller stories that still deliver business value. But they will happily break a story down into technical workflow or component tasks. I like the idea of User Story Maps which show the big picture under a breakdown of a business workflow. We can do the same on a much lower level, for tasks making up a user story, keeping the team in their comfort zone. Then we use this breakdown to identify different levels of quality for each step, and create vertical slices to identify smaller deliverables. [Read more at gojko.net](https://gojko.net/2012/01/23/splitting-user-stories-the-hamburger-method/)

These User Story Maps create a nice visualization of the story and helps teams break things down into "bite-sized" chunks.

In this next technique, [Chris Sims](https://twitter.com/chrissims), advocates using "timeline analysis" to help teams decompose their stories.

> With this technique, I ask my stakeholder to pretend that the user story is already done, and then I ask “What happens when the functionality gets used?” They will then describe a sequence of events. Even for things that happen non-deterministically, or which could happen in parallel, they still describe it sequentially; it’s just the way our brains and our language work. I then identify the verifiable steps in the timeline. [Read more at agilelearninglabs.com](https://www.agilelearninglabs.com/2013/05/user-story-splitting-four/)

Because our brain describes this sequentially, we can pull out various user stories from the steps in the sequence.

How do you know when a story needs further decomposition? [Roman Pichler](https://twitter.com/romanpichler) says look to the acceptance critiera.

> Stories sometimes look fine until we consider the acceptance criteria. If there are too many—more than about ten—or if requirements hide in the criteria, we need to rework and decompose the story. Here is an example: “As a user, I want to delete a text message.” The acceptance criteria state, “I can select any text message. I can remove the message text. I can save the modified message.” Not only is the second condition redundant, but the other two introduce new requirements rather than specifying acceptance criteria. This story should be split into three: a story about deleting text messages, a story about editing text messages, and another story about saving the modified messages. [Read more at romanpichler.com](https://www.romanpichler.com/blog/decomposing-user-stories/)

[Richard Lawrence](https://twitter.com/rslawrence) has a different take on determining when to split a story.

> How small should stories be? I recommend 6-10 stories per iteration, so how small is small enough depends on your team’s velocity. Before your next planning meeting calculate what estimate should trigger splitting a story. For most teams, it seems to be 8 or 13 points. But on a recent project I’m doing by myself, I’ve needed to split 5-point stories.

and when you hit that wall, you can use one of the patterns he describes in [Patterns for Splitting User Stories - Agile For All](https://www.agileforall.com/2009/10/patterns-for-splitting-user-stories/)

Finally, when you've split your stories, [Mike Cohn](https://twitter.com/mikewcohn) wants you to realize that it's ok if your estimates don't add up.

> I’m often asked if the sum of the estimates on the smaller stories must equal the estimate on the original, larger story. No. Part of the reason for splitting the stories is to understand them better. Team members discuss the story with the product owner. As a product owner clarifies a user story, the team will know more about the work they are to do. [Read more at mountaingoatsoftware.com](https://www.mountaingoatsoftware.com/blog/estimates-on-split-stories-do-not-need-to-equal-the-original)

There are a number of effective techniques you can use to break down user stories. When you feel like your team has fallen into the trap of decomposing along technical lines pull out this article and try one of the techniques listed here.
