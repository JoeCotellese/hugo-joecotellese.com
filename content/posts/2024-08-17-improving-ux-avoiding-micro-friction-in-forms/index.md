---
title: "Improving UX: Avoiding Micro Friction in Forms"
description: Micro-friction in UX is everywhere. My recent experience with Airbnb.
date: 2024-08-17T13:10:47.036Z
preview: ""
draft: false
tags: []
categories: []
lastmod: 2024-08-17T13:29:02.968Z
---

While booking a trip on Airbnb today, I needed to update my emergency contact phone number. Simple enough, right? But instead of being able to type in the country code with auto-complete, I had to manually scroll through a drop-down list.

The frustrating part? Airbnb already knows my address, preferred language, currency, and even my IP address. They could have easily inferred a country code. Instead, I was left dealing with an unnecessary friction point.

This is not a dig at Airbnb. I actually think their website is decent. This stood out because I also added a new personal phone number and it *did* give me a default country code.

These micro-frictions, although minor on their own, can accumulate and subtly degrade the overall user experience. As product designers, we should always ask: Can I provide a sensible default using the data I already have? By doing so, we can reduce user frustration and create smoother, more user-friendly interactions.
