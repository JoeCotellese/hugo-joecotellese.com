---
title: How to Implement GTD using OneNote
date: 2023-05-10T12:01:44.250Z
draft: false
lastmod: 2024-03-08T13:04:46.989Z
description: |
  Implement the Getting Things Done in OneNote with this comprehensive guide. Improve your productivity and streamline your GTD workflow.
---

## Overview

In this article I’m going to discuss how I moved from [Omnifocus](https://www.omnigroup.com/omnifocus/) to [OneNote](https://amzn.to/2FrzPAg) for implementing the Getting Things Done methodology. OneNote is a great cross platform general purpose digital notebook application from Microsoft. The cross platform nature of OneNote suits me really well since I have an Android, Mac and iPad and am constantly switching between each. This doesn’t represent the only way to implement GTD. Customize it to suit your particular way of working (and share what you did in the comments!).

If you've read the book and want a quick reference, check out my [Getting Things Done Study Guide and Notes](/posts/getting-things-done-book-summary/).

I'm including a template that you can use to follow along with this guide. [Copy the template](https://1drv.ms/f/s!AvCbt3nzB_sniBwMzKEns8ZUF6LQ?e=xNzo3M).

Just select the menu and choose `Copy to` as shown below.
![](/img/onedrive-copy-onenote-gtd-template.png)

## Setup OneNote

OneNote organizes content in Notebooks, Sections and Pages. My basic setup consists of the following.

- A single Notebook labeled GTD
- Multiple Sections as follows
  - Collection
  - Project List
  - Next Actions
  - Someday or Maybe
  - Checklists

{{<imgresize "images/Screenshot-2018-12-20-07.59.05.png">}}
Each of those has one or more pages within each section. I’ll talk about each of these in turn.

> A note about OneNote. Hyperlinks are your friend. I link individual project pages to my Project list. I also link out to Dropbox folders for things I have stored digitally. It saves a few seconds every time I need those items.

## Collection

The Collection section consists of a single page labeled **Inbox**. When I’m in a flow state and an idea pops in my head I can quickly get it into my Inbox using a number of methods.

### Getting Stuff into the Inbox

I wanted a way to get things into a single inbox in no matter where I am or what I’m doing. What I settled on is building out some workflows using the service Zapier and the OneNote API. [Zapier](https://zapier.com) connects applications to each other via their application programming interfaces (APIs). With Zapier and the OneNote API I could easily append items on a page. It’s the glue that makes my collection process easy. I’ll show you each of the workflows I’m using below.

#### Email

Getting stuff into my inbox via Email is handled using the "[New inbound Email" trigger](https://zapier.com/apps/email/integrations) and the ["One Note" action](https://zapier.com/apps/onenote/integrations) within Zapier. It's setup as follows.

1.  Create a new Zap
2.  Set the Trigger to "New inbound Email"

This will create a unique email address that you can use to send content into Zapier. Now, connect it to OneNote

1.  Add a OneNote action to Zapier
2.  Select the "Append Note" action, it might be listed under "less common options"
3.  Give Zapier permission to access your OneNote account
4.  Setup the template that maps the email content to OneNote
    1.  Set the note book to the name of your GTD notebook
    2.  Set the Section to "Collection"
    3.  Set the Page/Note to "Inbox"
    4.  Set the Content Type to Text
    5.  Set the Content to the email subject line. I actually append the text "Handle email" before the subject so it shows up in the Inbox in a nicer form.

I then add the Zapier email address to my address book so it's available everywhere. When I receive an email that I need to take action on I just forward it to my inbox. I can then process it during my review.

#### Google Assistant

I want to be able to use the “Take a note” function on my Android to append a note to my inbox. Unfortunately, while OneNote supports “Take a note” it can only create a new page. This isn’t exactly what I want to do. Instead, I rely on using Trello as an intermediary with Zapier. I have a Zap setup that will Append a Trello card to my Inbox whenever a new card is created on a board. It's pretty hacky but it works. You can follow a process similar to the one above but replace the New Inbound Email trigger with a [New Card in Trello trigger.](https://zapier.com/help/trello/)

#### Alfred

Alfred is a killer productivity app that lets you work more efficiently by giving you the ability to control actions on your Mac with the keyboard. One great feature is the ability to trigger automated workflows. This feature allows me to quickly capture ideas into my trusted system without interrupting my flow. To get this to work, I use the [Zapier for Alfred workflow](https://zapier.com/blog/zapier-for-alfred/) developed by the gang at Zapier. You can follow the instructions on that link to learn how to get it running. Once it's running you can add things into your system by tapping a key and typing

> onenote This is an item for my inbox

{{<imgresize "images/Screenshot-2018-12-20-08.00.11.png">}}
It's a game changer for me because it keeps me out of the [rabbit hole I often fall into](/posts/the-rabbit-hole) when I break flow and enter another application.

## Project Lists

David Allen defines projects "as any desired result that can be accomplished within a year that requires more than one action step." Those projects should go on a project list. Within OneNote I have a section labeled "Project List." This section has a number of pages. At the top, is a page titled Projects. This page lists all of the projects that have multiple next actions. Then, I have a series of pages for projects that contain my project support material. Each of those is hyperlinked into the main project list. Not every project in my Projects List needs a project support page but for the more complicated projects it's handy to have everything linked up in one place. A Project Support page uses the following template: Page Title is the Project Name, I use the same name that appears in the Project List Then I have the following sections

- Purpose / Principals
- Outcome
- Brainstorming - where I keep notes
- Next Actions - where I keep a running list of next actions, this is helpful if I think of a bunch of tasks during brainstorming
- Notes - a place to keep miscellaneous notes related to the project

## Next Actions

This sections consists of a series of pages for each of the Categories or Contexts that I care about. Here's my list but feel free to create your own.

- Computer
- Read
- Email
- Errands
- Calls
- Home
- Waiting For
- Thinking

## Someday Maybes

I keep my Someday Maybe list in its own section. I do this in case I want to keep more than one page for Someday / Maybes.

In my setup, I have separate lists for movies, books to read, trips to take, music to purchase, home projects, work projects.

## Checklists

Finally, I have a set of checklists that I use to help keep me on task. I’ve always loved using checklists. If you haven’t read the [Checklist Manifesto](https://amzn.to/2Kd2cRy), I recommend checking it out. Here’s my current set of checklists

- Morning Review
- Weekly Review
- Monthly Review
- Mental Sweep Checklist

### Morning Review

I like to keep my system in check so every morning, I do the following:

1.  Process my email inbox
2.  Review my calendar
3.  Review my GTD inbox
4.  Review my Next Actions
5.  Review my Projects List
6.  Review my tickler file

Doing this everyday gets me ready for the day and makes my weekly review easier.

### Weekly Review

My weekly review checklist is an expanded version of the Morning Review. I basically follow the process outlined in the book to get Clear, Current and Creative. The only variation is the things I check when I'm "getting clear." I've described that below.

#### Getting Clear

- I'll look around my office for any loose paper or things that don't belong and stick them into my inbox.
- I then follow the mental sweep list to get a bunch of things out of my head. I don't like to do this at the computer because I tend to get distracted. Instead, I'll grab my iPad and jot things using the Notes app.
- I then go through my various inboxes and process them.
  - I'll check my various email accounts, and process them.
  - I'll check my physical inbox and clear that out
  - I'll check my OneNote inbox and move things to projects and next actions
  - I'll clean out my Mac Downloads folder
  - I'll also clean out a special folder on Dropbox called 1Scans. This folder contains [content I've scanned in but haven't filed](/posts/finally-never-look-at-another-piece-of-paper-in-your-house-again) in my digital system yet.

## GTD On the Go

I've mentioned that I use Zapier to get my stuff into my system. How do I deal with handling things while I'm on the go? I'm using a [Google Pixel phone](https://amzn.to/2qUrGdI) and the [OneNote application](https://play.google.com/store/apps/details?id=com.microsoft.office.onenote). OneNote for Android lets me create shortcuts to specific pages right on my home screen. I've created a link to my Errands page so I can easily check it whenever I'm out. There's also a link to my Inbox which gives me another way to get things into my system.

{{<imgresize "images/gtd-shortcuts-android-screen.png">}}

## Your Turn

Are you interested in using OneNote for implementing GTD? If so, you can [download my GTD Template](https://1drv.ms/f/s!AvCbt3nzB_snil_K5425hal9fkSr). 

If you want a deep dive into how to setup your own productivity flow, [reach out to me](https://get-in-touch-46.zapier.app).