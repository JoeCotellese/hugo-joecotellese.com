---
title: Summary of Fifty Quick Ideas To Improve Your User Stories
date: Wed, 17 Feb 2021 14:59:19 +0000
draft: true
lastmod: 2023-05-21T20:47:45.797Z
tags:
  - books
---

This is a book outline for the book [Fifty Quick Ideas To Improve Your Users Stories](https://amzn.to/3atkvR3). It's one of the books on my [list of books that every product manager should read](https://www.joecotellese.com/learn-product-management-with-books/).

# Introduction

This book is meant to help you realize the promise of agile. Get the right stuff delivered through productive discussions between delivery team and business stakeholders. Acceptance testing is in a separate book.

# Creating Stories

## Tell stories, don't write them

These aren't lightweight requirements. Don't have a ton of detail. Don't _ever_ hand stories over to team. You won't get benefit of iterative delivery. They are _requirements by collaboration_. Domain expertise and technical knowledge comes together to hash things out. This collaboration leads to questions and prouct ideas. All the important decisions shouldn't be made in advance. Quick iterations requires collaboration b/c there isn't time to write everything down. When one person writes stories then all analysis, understanding and coordination falls on that person. That person then becomes the bottleneck. Physical story cards, Jira are just reminders for conversations. Don't waste time with details upfront. Get business stakeholders and delivery team in a discussion. Explore options together. That's the benefit of user stories.

### Key benefits

1.  This coordination makes sure stakeholders can talk about what they want and the delivery team understands it. Explain the story face to face to prevent problems from falling through the gaps.
2.  Faster analysis.
3.  They produce better solutions. Delivery team can understand business value. Business team can understand technical constraints, new tech, etc.

### Making it work

Commone reasons for writing detailed reqs and how to avoid them.

1.  Regulatory requirements - write all the stories first with the team then get the formal approval of scope.
2.  When there are people that need to approve of plans. - remote teams often need this. Have the discussions with the remote team before starting to implement the story. Coordinate w/ stakeholders. Discuss stories as team, refine them into spec then send to stakeholders. (reverse waterfall?)
3.  You need a specialist who isn't always available.
4.  Investigate dependencies a week or two before starting a story.
5.  review stories twice. A week or two before iteration to collect open questions. Just ahead of the iteration to communicate and agree on the details.
6.  3rd party systems require analysis
7.  See point 3 above

## Don't worry about the story format

Don't worry about the format. Just make sure you've covered these key elements.

- this is just a token for a conversation
- no clear proof that one way is better than another
- avoid syntax wars

Remember stories are just discussion reminders. They've served their purpose after a conversation happens. As a.. I want.. So that... is a good template. It's not the only way to start a discussion. If the story stimulates a conversation it's done it's job. Make sure you cover who wants something and why, don't worry about the template. Try some other formats to see if things stimulate conversations any differently.

- Name stories early, add details later
- don't spell out obvious solutions
- think of how more than one stakeholder might be interested in the item
- Use a picture instead of words
- ask a question

### Key benefit

- It can help shake up the discussions.
- Avoid the [cargo cult](https://en.wikipedia.org/wiki/Cargo_cult)
- unlock creativity in the team

### How to make it work.

Avoid making a story a feature request. You can't have a solution without context. For example Add login support == Bad How will someone be able to access the system == Good Focus on the problem from the users perspective, not the software implementation. One way to avoid trouble - describe behavior change as the summary.

## Describe a behavior change

INVEST has two conflicting forcers. Independent and valuable, hard to reconcile with small. Teams choose size over value. This creates technical stories that don't produce any outcomes. Disconnect between what team delivers and what business cares about. Don't assume that something has value just because business stakeholders are asking for it. Question it! "Valuable initiatives product an observable change in someone's way of working" Note enough to describe behavior. Aim to describe change in that beahvior. This is good for generic stories or stories w/o a value statement. See "We tried to capture the value not just as a behaviour, but as a change in that behaviour, and the discussion suddenly took a much more productive turn. Adzic, Gojko,Evans, David. Fifty Quick Ideas To Improve Your User Stories (Kindle Locations 187-188). Neuri Consulting LLP. Kindle Edition." for a great example.

### Key benefits

Capturing behavior changes makes a story measurable from a business perspective. You can decide if the proposed solution is appropriate, inadequate or over the top. Describing behavior changes:

1.  helps delivery team propose better solutions.
2.  ensures that what is delivered has the intended results
3.  minimizes waste, if there are multiple stories related to the behavior change if one gets you 90% of the way there maybe you don't need the other ones.

Measurable behavior changes makes stories easier to split b/c there is another dimension to discuss. i.e., import contacts 20% faster. What if we do 10% faster is that good enough?

### How to make it work

Quantify expected changes - observable and measurable. if you can't set discreet values aim for ranges See Excel example at Kindle Location 212

## Describe the system change

You can defer commitment until you are clear on the behavior change. Once you are clear, the story description should be clear about how things change between then and now. This applies to system functionality and/or business rules. Not about implementation details, just obserable system outputs The amount of work required by the team depends on how much things are different than the current system. Think about "instead of" clause to follow the "what" See example of the financial trading system at location 233 - 234 Even a complete description doesn't indicate complexity. Need to know how different it is from current behavior.

### Key benefits

When devs and testers are reviewing story having a description of how things have changed helps zero in on scope of the request. Useful when thinking about splitting stories

### How to make it work

Add another clause to the story. _Whereas, today I can access the system with user name and password. As a User, I can access my system with an Apple Id_ Add a verb after _what_ I want to access the reports - what I want to access the reports using a mobile device. You already can. Oh, I want to access the reports using a native application.

## Approach stories as survivable experiments

Common wisdom says a story is a small chunk of work that can be completed in an iteration but still bring value. Teams focus on small, not on value. Business doesn't care about small and waits for larger batch of work. References the book Adapt - linear plans rarely work b/c of things outside of our control. Turn that into a competitive advantage. How? Treat your plans as survivable experiments. Stories are a great fit for this approach if we change our thinking. Don't make stories small b/c they need to fit into iteration. Think if it as keep it small so the cost of being wrong is low. Stories should deliver biz value and you could be wrong. How much do you want to invest in learning? Stories should be small. Fitting into a sprint should be a consequence not a cause. Sell on value, not size.

### Key benefits

Help product managers discover what's really needed w/o getting too far forward under unvalidated assumptions. Help biz sponsors manage their investments in software. **Not a small chunk of work** Stories are survivable experiements. Stop thinking about expected complexity. Think about outcomes and learning. Stop: - technical stories, too small to go live - stories that business sponsors don't care about Biz outcomes help breaking down stories. Too big to fit into a sprint? Cost is high. Look for alternate ways of learning about something. Don't just try to slice story. Manage your investment in software.

### How to make it work

Big chunk of work? What are the underlying assumptions. What's the easiest way to prove or disprove them? Create experiments Author uses example of launching a mobile application. It's very expensive. How can you slim it down and run experiments to see if it's worth doing? See Location 292

## Watch out for generic roles

Big advanthage of user story is that teams can consider how a feature is used. Don't use "as a _user_" it's generic. won't provide useful context for discussion Can't decide if a solution is the right one or just feature creep Generic roles are used to sneak in pet projects.

### Key Benefits

focused user stories kick-start conversations. have clear accurate descriptions of the user roles creates another dimension you can split stories on -- customer groups

### How to make it work

Internal projects? Try using the actual people

- External projects? - Personas. Read \[\[The Inmates are Running the Asylum\]\]
- You can also use the checklist created by Stephen Wendel in Designing for Behavior Change
- Prior experience w/ action
- prior experience w/ simiarl products and channels
- relationship w/ company or organization
- existing motivation
- physical, psych or economic impediments to action
- Group by common behaviors
- market research
- anatomy of a live music fan

## Evalute zone of control and sphere of influence

Book Logical Thinking Process discusses three areas of systems:

1.  zone of control - all things that we can change
2.  sphere of influence - we can impact but can't exercise control
3.  external environment - where we have no influence

User need "So that I..." or "In order to" (sphere of influence) Deliverable "I want to" (zone of control) If a story doesn't match this pattern it should be investigated. If user need is in zone of control this is a red flag. - fake story? - about the needs of the delivery team "As a QA person..." - microstory? - stories broken down to manage risk. - can't measure success of this story w/o looking at whole hierarchy - misleading - describe solution w/o real user need - example on Location 361 of user need being "run reports faster" which should be under control of the delivery team. Story was hiding real objective - find discrepencies. Sometimes deliverable is outside zone of control of the team. 1. Unrealistic expectationss 1. Just reject it 2. Story isn't actionable by the delivery group 1. it might need input from someone outside your team

### Key Benefit

Zone of Control and Sphere of Influence are crucial for finding root causes of problems so you're working on the right thing. System thinking mindset

### How to make it work

Consider system boundaries from the delivery team's perspective Raise the alarm early, consider rewriting the story Micro-stories aren't necessarily bad, going too dep is probably overkill If micro-stories are on long term plans, replace whole group of related stories with one larger item If stories are only partially actionable, split them between actionable piece and piece that needs outside intervention.

## Put a 'best before' date on stories

Stop firefighting. When someone puts a story in the backlog make sure that there is a due date on them if there is a real time constraint Create a view of your backlog that has time constrained stories listed. When you deal with emergencies, you can incur tech-debt

### Key Benefits

You can manage time constrained stories before they become urgent. reduce emergencies reduce tech-debt allows you to handle real emergencies effectively

### How to make it work

1.  Ask about potential deadlines
2.  have a visual indicator on those items
3.  uses classes of service or a separate swimlane for fixed-date items

Ask for some amount of notice on fixed time stories. Best-before date can be one after date story was proposed Don't worry about putting this on every story. Keep it only for things that have a real time constraint. Try limiting the # of time based stories - percentage of backlog - or total number

# Planning with stories

## Set deadlines for addressing major risks

Potential problem with focusing on delivering value sooner - you push risky things off until later. Deliver in small increments, lose the big picture. Good case study for this in Why the FBI Can't Build a Case Management System Further discussed in the book "The Lean Mindset" - due to pressure to demonstrate progress easy tasks were done first, risky stuff pushed off to later. If you track progress by activity you could fall into this trap. You split stories to avoid the risky areas and things can go off the rails. Address this by identifying the major risks, then set deadlines for addressing those risks at a sustainable pace.

### Key benefits

Strike a balance between short-term biz wins and long term sustainability. Plan deadlines that deal with risks as decision points for incremental investment.

### How to make it work

Alistair Cockburn suggest: - Alternate delivery between 'paying to lean' - addressing risks and 'paying to get biz value' - Early stages of a project could focus on addressing risks before building value. - "Develop for business value once the risks are down" New product development use Lean Analytics stages of growth model (citation?) For internal IT intiatives work with stakeholders to make planning decisions. One strategy is have "learning stories" with a "best before" date. Need to agree on definition of done to show risk is addressed. Completed work, proof of concept, conceptual design You can also create a hierarchical backlog. Prioritize at a level higher than stories. Important! Don't use activity metrics like velocity or burn-down to measure progress. Doesn't show that you are working on the right things or deliverying something valuable. Activity metrics good for tracking whether team is working well together. It's the wrong incentive for prioritization. Create a model for expected biz value and report progress toward it.

## Use hierarchical backlogs

Avoid Story Card hell - a big pile of cards that you need to keep track of. You lose the ability to understand what they mean. Creates an illusion that the process is working iteratively but actually has 3 problems.

1.  too much time tracking and managing unnecessary items
2.  hard to adapt to changing circustances
3.  easy to hide scope creep

How does this happen? People want visibility - small user stories provide visibility Useless for big picture though. Lack of big picture visibility is why biz asks for detailed estimates and commitments up front. Use the game asteroids as an example. If you break up all the big rocks first you die. Break up one big rock into medium rocks. Then small rocks. Small ones are most important but you can't take your eyes off the big ones. Don't break up all the big stories into small ones. You need multiple levels of abstraction in your backlog. Tiers and then don't break down higher-level items until you complete relevant low level items. No point in tackling other big items if you haven't finished the ones you are on. Track the big items so the team knows what's coming up. Also helps rest of biz realize that you haven't forgotten about the other stuff. Use 4 levels

1.  Big picture business objective
2.  Smaller scale business change
3.  software deliverables that support those changes
4.  small slices that can ship independently

Until you are going to work on a business goal, why break it down?

### Key benefits

Keep the overall number of items in the backlog small. Provide a big picture view Don't worry about sunk cost if things change. Can change priorities quickly If everyone understands the big picture they can make better decisions to work or not work on things realted to the big picture.

### How to make it work

Figure out the right number of levels for your organization. Keep the number of items in each tier small Visualize this with hierarchical swim lanes, journey map, story or impact maps

## Group stories by impact

Use an impact map to connect user stories to the big picture. Impact maps have four levels

- Goal: The business goal
- Actor: The users or stakeholders that are involved in achieving the goal (the persona)
- Impact: What impact we're trying to drive to hit the goal (the value statement)
- Deliverable: The user stories or epics meant to achieve that goal

Advantages of the impact map? - Visual - Collaborative - Simple

### Key benefits

Impact maps help facilitate decision making and prioritization discussions. the central node forces stakeholders to align on the big business goal for a delivery it eliminates unnecessary scope Facilitates good product management techniques With stories grouped under related impacts it helps stakeholders pick and prioritise on a higher level. Easy to spot scope creep because user stories not part of the current reelase cycle don't have a place to fit on any of the branches of the impact map. Impact maps visualize assumptions It can help the team to design tests to validate or disprove assumptions this supports better product management Eliminate unecessary work. Once an impact is achieved, the remaining deliverables can be discarded.

### How to make it work

When you describe the goal of the milestone - focus on the problem to be solved, not the solution "Create an iPhone app" - not a good goal "improve mobile advertising revenue" - good goal When describing the actors - who is positively or negatively impacted by this. 1. Primary actors who's needs are fulfilled 2. Secondary actors who provide services to help the primary actors 3. off-stage actors who have an interest but don't directly benefit. **Impacts are not product features** The fourth level of the map are options, not commitments Check out the book [Impact Mapping: Making a big impact with software products and projects, Adzic, Gojko, eBook - Amazon.com](https://www.amazon.com/Impact-Mapping-software-products-projects-ebook/dp/B009KWDKVA)

## Create a user story map

User story map invented by Jeff Patton. Popularized in the book User Story Mapping(link) Idea is similar to customer journey map Key idea:

- Horizontal axis represents high level user activity
- Vertical axis represets the software deliver schedule

Way to engage business stakeholders about needs and potential solutions and planning.

### Key benefits

Put stories in the perspective of the journey a user will take through the system or feature.

- Help teams figure out how things fit into the big picture.
- Can help spot missing or unnecessary steps
- create new product ideas.

This also helps you avoid the linear thinking typical in a hierarchical backlog.

- Story maps help with splitting stories
- Provide focus for release planning.
- User stories become options instead of commitments.

Show how you can simplify your deliverable. Can highlight how to get to semi-automated solutions for intial release sooner.

### How to make it work

- create the story map for key activities _not software solutions_
- Examples: purchasing a book, booking a venue, attending a concert
- posting to a social network - too low level don't deserve their own map

1.  Identify the backbone of the map - the horizontal axis
2.  break down activities in to high-level steps
3.  should not imply a specific solution or technology
4.  After backbone, identify options for story.
5.  Backbone - Discover books (horizontal)
6.  Options - Book Recommendations, Book searching
7.  Think about how to do partial solutions
8.  Create them on large whiteboard or wall using sticky notes
9.  Working virtual? Mural is a great solution for this.
10. Keep it out of your backlog as loon as possible. It kills the conversation
11. Optional journeys? Try to keep them on the same map.
12. Complex solutions might still require you to keep an individual map

Do you have multiple user segments? Try using an Impact Map

## Change behaviors using the CREATE funnel

Designing for Behavior Change presents 5 preconditions for action:

1.  Cue - the need to take action crosses someone's mind.
2.  Reaction - person reacts quickly, generating an emotional response.
3.  Evaluation - what are the costs / benefits of doing this?
4.  Ability - can I do this?
5.  Timing - is the time right to do this given the current context.
6.  Execution - doing the action

"Great user stories should be focused on causing behaviour changes, and once we have nailed down a particular behaviour change, we can use the CREATE funnel to come up with good story ideas. "

### Key Benefits

Can help you create a non-linear backlog. Bring about behavior change. Can you link yoru story to one of the steps in the funnel? It supports hierarchical planning and prioritization. It's a good model for splitting stories. If the story attempts to influence multiple parts of the funnel that's a good split point.

### How to make it work

Model key actions then map them to a stage of the funnel. One stage for each action / persona. Write out each stage of the funnel. Idenfity critical features for each of those stages. move the important stories toward the top of the funnel. Then, work horizontally. Another option is pick a stage and work veritcally, trying to incrementally improve one area. Can you identify KPIs that show drop off at each point in the funnel? It will give you a benchmark to decide when you can move on to other work. Some ways to influence different parts of the funnel: \*\*Cue \*\* Change cues to prevent users from ignoring them. create associations between users existing tasks, routines or habits and the product. avoid concurrent cues to seek actions **Reactions** Build up trust, improve initial user experience **Evaluation** Help users build habits to skip the evalution and timing parts of the funnel. Make tasks easier to accomplish or understand automate repetition, remove friction **Ability** Provide clear guides and action plans. provide small wins, reduce risk of failure **Timing** use time sensitive content, encourage early commitments Try to invert the funnel, making inaction work for the users benefit. Shift burden of work from user to the product.

## Set out global concerns at the start of a milestone

Just because agile is about responding to change doesn't mean you should ignore predicatable problems. You should layout those global concerns once per milestone. _Note, if you're doing CI/CD maybe revist them once a quarter_ Try doing this with a mindmap. FURPS+ - Functional - Usability - Reliability - Performance - Supporability - + branch for global items that dont' fall into this category like implementation constraints, resource limitations, interface constraints, ops requirements and licensing requirements go here. Or a pyramid of quality based on Maslow's hierarchy of needs. From the bottom up:

- Does it work? (functionality and deployment)
- Does it work well? (security, performance, capacity)
- Is it usable? (usability, design)
- Is it useful? (behaviour changes, user-level goals)
- Is it successful? (organisational goals)

Adzic, Gojko,Evans, David. Fifty Quick Ideas To Improve Your User Stories (Kindle Locations 715-716). Neuri Consulting LLP. Kindle Edition.

### Key Benefits

Keeps you from having to discuss the same concerns over and over again with each story. Can engage stakeholders even if they are too busy to attend every refinement session.

### How to make it work

Create a checklist of expectations for global concerns. Describe ranges instead of single values i.e., "site is fast" vs page loads are less than <500ms If you are doing story mapping, hold a global concern discussion each time a new map is introduced. Bring in senior decision makers on these discussions. Mix technical and business people so all perspectives are represented.

## Prioritise according to stages of growth

New product development is a juggling act between short and long term needs. You can't ignore long term needs because that could keep you from building a sustainable business. However, you can't ignore short term needs either because that could keep you from even taking off. How do you balance this? You could use the growth model described in the book [Lean Analytics](https://amzn.to/3atkvR3):

- Empathy : figuring out how to solve a real problem in a way that people will pay for
- Stickiness : building the right product to keep users around
- Virality : growing the user base organically and artificially
- Revenue : establishing a sustainable, scalable business model with the right margins in a healthy ecosystem
- Scale : growing the business

Adzic, Gojko,Evans, David. Fifty Quick Ideas To Improve Your User Stories (Kindle Locations 751-754). Neuri Consulting LLP. Kindle Edition. Good framework for prioritizing user stories. I.e., why focus on virality if you haven't figured out customer retention?

### Key Benefits

This framework helps with alignment and focus. Helps with now / later prioritization. Stages of growth are good input for other PM techniques, building a value model and choosing goal for an impact map.

### How to make it work



## Prioritize using purpose alignment

### Key Benefits

### How to make it work

## Make a stakeholder chart

### Key Benefits

### How to make it work

## Name your milestones

### Key Benefits

### How to make it work

## Focus milestones on a limited number of user segments

### Key Benefits

### How to make it work

# Discussing stories

## User low-tech for story conversations

### Key Benefits

### How to make it work

## Imagine the demonstration

### Key Benefits

### How to make it work

## Diverge and merge for story discussions

### Key Benefits

### How to make it work

## Involove all roles in the discussion

## Measure alignment using feedback exercises

### Key Benefits

### How to make it work

## Play the devils advocate

### Key Benefits

### How to make it work

## Divide responsibility for defining stories

### Key Benefits

### How to make it work

## Split business and technical discussions

### Key Benefits

### How to make it work

## Investigate value on multiple levels

### Key Benefits

### How to make it work

## Discuss sliding-scale measurements with QUPER

### Key Benefits

### How to make it work

# Splitting stories

# Managing iterative delivery

# The End
