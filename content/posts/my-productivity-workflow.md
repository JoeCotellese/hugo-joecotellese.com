---
title: My Productivity Workflow
date: Tue, 20 Nov 2018 12:46:30 +0000
description: Working efficently is the hallmark of a great product manager. Here's the workflow I use to get s**t done.
draft: false
lastmod: 2024-02-02T12:32:43.822Z
---

I have a number of tools and techniques for getting stuff done throughout the day. This article originally was written as a note to myself. I'm publishing because it might help others. It's certainly going to help me when I inevitably fall off the wagon.

## Inboxes

The Inboxes are where I capture things that I'm going to work on later. It's easy to end up with too many collection baskets. 

- 1Scans Folder - a Dropbox folder that I use for my [paperless workflow](/posts/finally-never-look-at-another-piece-of-paper-in-your-house-again/)
- Drafts app
- iPad Journal application (see tools below)
- Jira - for work related items
- Omnifocus Inbox
- Personal Email Inbox
- Physical inboxes (I have three)
- Work Email Inbox

## Getting Things Done

Processing the inbox means deciding how I'm going to work on my tasks. I follow the [GTD methodology](/posts/getting-things-done-book-summary) for my workflow. This is my trusted system that I use as the source of truth for everything I'm working on.

Items are either Projects, Tasks or Next Actions, Someday Maybe items or reference material for same.

## Projects

Projects in the GTD world are any “desired result that can be accomplished within a year that requires more than one action step.”

These projects are placed within a project list inside Omnifocus. In addition, if the project seems like it's going to take up a bunch of mental space I also create a project page inside Obsidian with Vision, Outcome, Brainstorming and Notes sections.

### When does a task(s) become a project?

This is really tricky — things tend to become projects out of control in Omnifocus. I end up with a bunch of project like tasks without the associated thought behind them that warrants its own project space.

What does creating a project look like? I think an optimal workflow looks something like this.

- Drop a task into Omnifocus. It may or may not be a project on it’s own.
- When I’m processing the Inbox, make the decisions whether this is a one off task that once completed doesn’t have any other associated work or if it’s a project
  - if it’s a one off task, I move it to a Misceallaneous project. 
  - if it's a project, I create a project in Omnifocus, and if I can think of additional next actions, I'll add that to the project. As I mentioned, if this project is long lived or requires additional support material I will create a project file in Obisidian.

## Tasks

As general rules of thumb when writing tasks:

- Start with a verb when possible.
- Maintain simplicity when possible.
- Avoid combining multiple tasks. (Be wary of the word “and”.)
- If it is a completable task, be as specific as needed, but not more so.
- Write as if you are delegating to your future self.
- If it is an ongoing task without clear boundaries (e.g. “Practice painting”) than a general task may do fine, but know that such general tasks are often developed into many specific completable tasks. This is the one that I have the most trouble with. I have a few of these "Read book", "Practice saxophone" I have to be careful that I don't become blind to them because they are more general tasks.

## Someday Maybe

Things I might want to do later but don’t want to clutter my system with go into a text file “refx - someday maybe list.md”

Since the Someday Maybe list is a text file I can use some cool features in Text Expander and Launch Bar to append ideas to the file.

If a Someday/Maybe idea hits my brain I do the following:

- Search for the file in LaunchBar
- Press SHIFT+SPACE to activate the “Append Text” feature
- use TextExpander to enter the current date and type the idea
- The latest idea is then date stamped and append to the list.

## Project Reference Material

Project reference material is usually referenced in the project file that I create for a given project. Reference material could come from a number of sources.

## Tools

I've moved most of my list of productivity tools to this [list](/posts/2023-09-01-software-i-use-to-get-stuff-done/).
Tools I use as part of my daily workflow

## iThoughts

I use [iThoughts](https://www.toketaware.com/) for mind mapping and free form idea generation. It’s great because it is cross platform. I’ve built a master mindmap of all of the books that I have for personal productivity and growth. It’s useful to see these together to help me link similar ideas.

## Hazel

Hazel acts as my personal assistant to help me file away things into proper places so that I can find them again easily.

Items usually get saved into the 1 Scans folder. Then, they are saved off into other folders.

## Internet Research

I’m using Diigo to collect things I want to read on the web. These get saved via browser extensions. I tag them when I save them.

## Goodnotes (iPad)

I’ve tried a bunch of tablet notebook analogs. I’m currently liking GoodNotes. These notes can export to PDFs directly into Dropbox

I’m using the application GoodNotes on my iPad for note taking.

GoodNotes is the closest analog that I found to paper yet. It has a lot of the features I loved about the old Dan Bricklin Notetaker HD application but with a much more robust user interface.

## Day One

Day one is a journal application. I put in there things that I learn and want to recall later. One could argue that I could just do this all with text files but Day One has a more elegant interface that has a slightly better feature set then plain text but is not heavy like Evernote.

I will put quotes, article snippets, brain dumps in there with appropriate #tags to help me find it again later.

## Delegating Work

When delegating work to others I follow two principles.

1.  Make sure that my intentions are clearly explained so that is no ambiguity in what I’m asking for. Specifically include what the expected outcome is and when it is due.
2.  Make sure that I follow up with the person on a regular basis up until it is due.

I’ve created a project in Omnifocus called “Delegated Work” where I place items that were delegated to others.

I should create an action in this folder for follow up that occurs regularly until the project is finished.

## Your Turn

This is very much a work in progress. I'm constantly evolving how I do my day to day activities in order to fit the most work into the least amount of time. Was this helpful for you? Do you have any daily workflow productivity tips? Let me know in the comments.
