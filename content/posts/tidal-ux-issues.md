---
title: UX Issues with Tidal Playlist Editor
date: 2022-01-04 14:18:56
draft: true
description: This is the meta description for the page
lastmod: 2023-04-10T15:57:49.370Z
---

I just recently started using Tidal. When I setup my account, I moved my content and playlists from Spotify.

For reasons not relevant to this post, I ended up with a bunch of extra playlists. 

I started deleting them one by one but each delete required me to confirm my action. 

I can bulk select multiple playlists but can't delete them.

"Ugh, why don't they just give me a trash can, rather than force me to confirm each action?"
<!--more-->
Well, it turns out Tidal does have a Trash can, of sorts.

If you create a Trash folder, move play lists into the folder, then delete the folder, you delete the play lists.

Problem solved.

"So what's the big deal? You're able to get the job done." 

Sure, but it was a pretty big cognitive lift to do what is essentially a hack. 

There are a few problems worth exploring.

## Confirmation dialogs

I think confirmation dialogs are important if you attempt to perform something destructive that can't be undone. Like formatting a disk.

Often confirmation dialogs are used as a crutch for laziness. In this case, Undo or a Recycle bin is the answer.

## Discoverability

I discovered by accident that I could drag playlists into a folder. 


## Bulk actions

The Tidal UI lets you select multiple things. However, it provides no hints as to what you can do once you've selected them. 

Provide a bulk action prompt or menu to indicate what actions you can perform on multiple items.

