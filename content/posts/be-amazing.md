---
title: Be Amazing
date: Fri, 07 Sep 2012 14:01:25 +0000
lastmod: 2024-03-10T23:57:28.172Z
draft: false
contenttypes:
  - BlogPosting
---

I recently went to a great BYOB restaurant that just opened in Philadelphia. A week later, they sent me this.

![Thank You Note from Will BYOB](/img/will-byob-thankyou-note.jpeg)

When was the last time you did something unexpected and awesome for your customers? Writing this card probably took the waitress 30 seconds to write. The impact for the business is priceless. I’ve already told a number of people about it (including you, the reader) and when I posted it on Facebook generated interest in the restaurant by people on my friend’s list.

Spend some time today thinking about what random act of amazement you can do for your customers and get to it. Your customers will not only appreciate it, they will remember it and talk about your business. In the age of social media their appreciation can affect a lot of people.

## Your Turn

Write a note, send a tweet, do something unexpectedly amazing for your customers.
