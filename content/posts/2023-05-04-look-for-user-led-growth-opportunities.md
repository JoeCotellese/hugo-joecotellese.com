---
title: "Product-Led Growth: Your Competitive Advantage"
description: Product-led growth can help you gain more customers. Using Fantastical as a case study, see how you can leverage this technique to drive users to your product.
slug: product-led-growth-fantastical-case-study
images:
  - /img/Blog%20Product%20Led%20Growth%20Competitive%20Advantage.png
date: 2023-05-04T13:57:18.122Z
preview: ""
draft: false
tags: []
categories: []
lastmod: 2023-05-05T14:33:52.771Z
---

You might think that your product doesn't lend itself well to driving customer acquisition. Typical virality channels just might not apply to your product. But, with a little creativity you can probably find ways to use your product to drive customer acquisition.

Product-led growth, where the product itself is the main means for driving customer acquisition is a great way to compliment your marketing efforts. 

I use the great app Fantastical to manage my calendars on my Mac and iPhones. 

They recently added Openings, a feature that allows others to schedule slots directly into your calendar. Adding this feature allowed me to stop paying for a separate Calendly account. Frugal powerup!

Today, I sent the link over to someone via LinkedIn. 

Here is the preview that was rendered in the message window.

![picture of the author used as an open graph tag in the Fantastical Openings feature](/img/Agilebits-Fantastical-Scheduling-Opengraph.png)

Ugh. I mean, I'm handsome and all but it really seems like I'm all "up in yo face." Plus, the text for the link falls below the fold.

Rather than show my profile picture, this is a great opportunity for Fantastical to educate the audience about the product. Here's one way to do it.

1. Explain what the link is about. How about "Schedule time in Joe's calendar using Fantastical"
2. Change the default image to a stylized version of a calendar and include the Fantastical logo.
3. Fantastical is a Mac / iOS only app. So, if it detects that OS include a call to action such as "Try the award wining calendar app Fantastical"

Make sure you track that call to action in your analytics so you can see how your experiment is performing.

Product-led growth can be a powerful approach to drive customer acquisition and complement your marketing efforts. Even if your product doesn't seem like an obvious fit for virality, there are likely ways to use the product itself to attract new users. As you consider opportunities for product-led growth, challenge yourself to think creatively about how your product can drive customer acquisition, and track the impact of your experiments on key growth metrics. With persistence and experimentation, you may find that product-led growth is a game-changer for your business.

If you want to learn more about how to implement product-led growth strategies in your business, [reach out](/services).