---
title: Great 15 minute overview of the role of Product Owner
date: Mon, 26 Jan 2015 21:53:39 +0000
draft: false
contenttypes:
  - BlogPosting
lastmod: 2023-05-22T12:04:10.895Z
---

Henrik Kniberg,  Agile/Lean coach at Spotify & Lego gives a 15 minutes presentation on being a product owner. I love the way he condenses this information down into a digestable bit. https://youtu.be/502ILHjX9EE

Some key takeaways here.

1. Your agile teams should be cross functional.
2. Don't get bogged down in manual regression testing. Invest in automated testing and continuous integration.
3. Every story needs at least one automated acceptance test and unit tests on the code.
4. Keep team morale high by using the velocity from earlier iterations or "yesterday's weather" as an indicator of how much work a team is capable of taking on in a sprint.
5. Guessing the "value" of a story is hard to do, that's ok though. Even when you are wrong the real benefit is in the conversations with the team.
6. Product ownership is all about communication which fits nicely with the agile manifesto.
7. Reduce risk by trying to acquire knowledge. Use UX prototypes, technical spikes, and experiments to gain that knowledge.
8. There should be a healthy tension between the building the right thing in building the thing right. This tension should be  between the product owner and the development team.
