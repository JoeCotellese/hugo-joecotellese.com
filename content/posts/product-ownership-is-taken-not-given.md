---
title: Product Ownership is Taken, Not Given
date: 2022-01-15 08:31:28
draft: false
noSummary: true
lastmod: 2023-03-09T18:15:38.820Z
---

Ownership is taken, not given. If you are starting out in product
management, don’t wait for your boss to give you responsibility over a
product line. Prove that you deserve it and then take ownership.

**How do you prove you deserve it?**

Understand the company's strategic vision. Be able to articulate that
strategic vision and how the current product fits (or doesn't.)

Understand your product inside and out.

Understand your competitors inside and out and know why they make the choices
that they make.

Understand your customers needs and pain points.

If you wait for the ownership fairy to tell you it’s time you'll be
disappointed.
