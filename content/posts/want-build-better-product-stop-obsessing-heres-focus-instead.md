---
title: Want to build a better product? Stop obsessing about it (here’s what to focus on instead)
date: 2021-11-16 07:59:49
draft: false
lastmod: 2023-05-05T20:58:39.030Z
---

> This post was updated from an earlier piece

Assume for a moment that you just returned from a cross-country trip. You’re sitting at home looking at your credit card statement with all of the trip expenses. Which companies are most likely to show up on your bill? If you flew, maybe it’s USAir or JetBlue. If you drove, you’re likely to see Exxon, Lukoil or some other gas stations.

So let me ask you this: Is Amtrak going to show up on your bill? It’s possible but not likely. How about Reading Railroad or Southern Pacific?

Why no railroads?

It's because railroad companies missed an opportunity. They stopped asking what their customers wanted and needed — efficient, convenient means of getting from point A to point B — and instead focused on building more trains. Railroads were so focused on being in the train business that they failed to realize they were actually in the transportation business. (This was brilliantly illustrated in the article “Marketing Myopia” in the Harvard Business Review back in the ‘60s.)

So today, you’re driving cars built by Honda, Toyota, Ford and not Union Pacific. You're filling your tank at Exxon rather than Reading Fuel.

You might be saying to yourself, "Fine, that's pretty obvious, but how does that impact me today?” Well, history has a way of repeating itself. Let's take a look at Kodak as another example.

Kodak actually created the first digital camera in 1975. Ok, you couldn't fit it in your pocket or purse — or a backpack for that matter — but the company was at the forefront of a revolution. They just didn't realize it. They never marketed the technology. Kodak thought they were in the film business. What they failed to realize is that they were really in the nostalgia and storytelling business. In the meantime, Sony brought the first digital camera to market in 1981.

Where is Kodak today? They [sold off their IP](https://spectrum.ieee.org/the-lowballing-of-kodaks-patent-portfolio) which, by the way, included many innovative patents around digital imaging, in a fire sale. Kodak emerged from Chapter 11 merely a shell of its former self. The company has gone the way of Reading Railroad, Union Pacific, and countless other companies that focused on products rather than customers.

> When technology continually disrupts entire industries your awesome new product of today could be obsolete tomorrow.

We all know this – and it’s a risk we take in any business venture. As product managers we need to ensure that our companies are resilient to changes in the market – and obviously, we want to make sure that our businesses are thriving today.

So how do we protect ourselves while setting the stage for future growth? Hopefully you’re on to me now. We need to change our mindsets to stop hyper-focusing on products, and instead focus on customer needs.

Now, this might be strange hearing from the product guy. It's my job to create products. But, I'm here to tell you that if you want your business to be successful, you need to stop looking at your product as “that thing that I’m selling,” and start asking “Does it meet the needs of my customer?”

How do you shift your thinking? Well, if you find yourself asking the question "What should I build next?” you should stop. Instead start by asking the question "What am I really selling?”

We talk about that more in [this post](/posts/build-better-products-asking-one-question)
