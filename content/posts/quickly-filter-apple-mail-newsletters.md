---
title: How-to quickly clean old newsletters in Apple Mail
date: 2022-02-11 09:25:40
draft: false
lastmod: 2022-05-18T19:36:22.773Z
---

I receive a ton of email newsletters. I try to unsubscribe but let's be honest who really wants to go through and manually click "unsubscribe?" 

I know there are paid services that will manage your inbox for you. I just don't feel like spending the money.

Here's a way to nearly automate batch deleting tons of email in macOS Mail.

So, if you want to follow along, launch your Mail app and let's get going.
<!--more-->
## Create a "newsletters" rule.
Open the Mail rules by:
- clicking the Mail &#x2192; Preferences menu
- selecting the "Rules" tab

Once the rules tab is open:
- Create a new rule. I call mine "Newsletters I can unsubscribe from" 
- Set the condition to "any"
- Now create each of the conditions. Here's what I use:
  - Message contains **Unsubscribe**
  - Message contains **Manage my**
  - Message contains **Opt-out**
  - Message contains **subscription settings**
  - Message contains **no longer wish to receive**

![](/img/apple-mail-filter-rules-newsletters.png)

## Create a Smart Inbox
- Select the "Maibox" menu and choose the "New Smart Mailbox" sub-menu
- Setup your Smart Mailbox rules as follows:
  - Smart Mailbox Name **Newsletter : Inbox : >30d**
  - Contains messages that match **all** of the following conditions:
    - Message has flag "Gray"
    - Date received is not in the last 30 days

Then click Ok.

![](/img/apple-mail-smart-mailbox-newsletters.png)

## Delete the message in the Smart Inbox
What you end up with now is a mailbox that contains old newsletters. Quickly 
skim through the emails and then delete the ones you don't want to keep.

Now that you can see how easy it is to setup your own rules, you can customize
them to your liking.
