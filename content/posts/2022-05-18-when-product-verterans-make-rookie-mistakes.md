---
title: When product veterans make rookie mistakes
description: ""
date: 2022-05-18T17:30:00.911Z
preview: ""
draft: ""
tags: ""
categories: ""
lastmod: 2022-05-18T19:23:51.961Z
---
Sometimes "veterans" make rookie mistakes.

This week was my turn, if you're in #productmanagement come along and learn from my mistake.

When I coach product teams and they ask me to review their user stories or roadmap, I'll always ask them "How will you know if you're successful?" Often times teams will scratch their heads and say something vague like "they'll use it."
<!--more-->
At that point, we’ll spend some time talking about KPIs, funnels, and activation metrics. I'll ask the team some probing questions like "What does using it look like?"

We'll then back into specific, measurable metrics, that we can use to determine whether the thing was successful or if more work is needed.

Well, last week in my rush to put out a new feature in ClipDish, I forgot to ask *myself* that question.

In this last release, I updated the onboarding in order in hopes to improve activation metrics. The flow is updated but I forgot to update the analytics to know how many people are going through it or dropping off.

I'm kicking myself because, now, I have to release an update to correct my mistake and have have lost a week of good data.

My takeaway in all of this is we all make mistakes, this sticky note on my monitor will hopefully help me to avoid this one in the future.

![Note to self](/img/sticky-note-to-self.jpeg)

#kpis #datadrivendecisions