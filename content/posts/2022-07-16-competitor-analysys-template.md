---
title: Competitor Analysys Template
description: ""
date: 2022-07-16T13:33:35.282Z
preview: ""
draft: true
tags: ""
categories: ""
lastmod: 2022-08-10T13:34:24.481Z
---
Competitor Analysis Template: 12 ways to predict your competitors’ behaviors

The best companies in the world remain at the top of their game by keeping a close on eye on their competitors. Honda was able to enter the U.S. motorcycle market with a small motorbike because they saw that the U.S. manufacturers had assumed there was no market for small bikes.

A competitor analysis is a great way to obtain information about important competitors and use that information to predict competitor behaviors whilst making better business decisions.

Using a competitor analysis template you can compare how your company rates against your competitors across a wide range of aspects.
This article will focus on explaining what a competitive analysis is and how you can use the template I have created to make better informed business decisions.

My competitor analysis template has been adapted from Michael Porter’s, Competitive Strategy 1980, with theories and thoughts taken from Jim Riley, Daniel Burtein and Business Lifestyle Solutions.

Before I go straight into showing you the customer analysis template, I wanted to bring to light some of the key reasons why you would want to do a competitor analysis and what benefits you can get out of doing one.

 

Why do a competitor analysis?

The main goals of a competitor analysis are to understand who your competitors are, what strategies they are using and have planned, how competitors might react to your company’s actions, and how to influence competitor behavior to your advantage.
A lot of the time, this data is easily accessible – so you will need to dig deep into annual reports, press releases, product brochures, patent applications and much, much more!

I will take you through the most common places to find each piece of information in the customer analysis template.

So now you know why you should do a competitor analysis, what are the real benefits?

    Can assist your management with developing marketing strategies
    Can identify opportunities in the market that are under-served
    Can help you take advantage of competitors weaknesses to grow market share
    Can allow you to make better informed decisions about your strategy and ensure you can create sustainable competitive advantages
    Can help you with forecasting future investments

 

Now let’s get into the competitor analysis template. I recommend you download and print the template here so you can fill it in while I take you through each section.

I break the template down into 12 key sections:

    Competitor Profile
        Overview
        Competitive advantage

 

    Marketing Profile
        Target market
        Market share
        Marketing strategies

 

    Product/ Service Profile
        Product/Service offerings
        Pricing and costs
        Channels

 

    SWOT Profile
        Strengths
        Weaknesses
        Opportunities
        Threats

 

As I go through each of these sections, make sure analyse your company, then analyse three of your most important competitors.

 
Competitor Profile: Overview

The competitor profile and overview involves analyzing competitors using Michael Porter’s framework based on four key aspects:

    Competitors objectives
    Competitors assumptions
    Competitors strategy
    Competitors capabilities

 

Objectives and assumptions drive the competitor while the strategy and capabilities are what the competitor is capable of doing. See the diagram below to see the components:

Competitor Objectives:  these objectives are not always financial; they can commonly be related to a company’s growth rate and market share. The reason you want to know a competitors objectives is so you can better plan for their strategies.
Let’s say your competitor is focusing on short term revenue – you can likely suspect they will spend most their efforts on holding strong positions for the top performing products as opposed to investing in research and development to roll out new products.

Competitors Assumptions: assumptions are made by management most the time based on past experience, but can also include beliefs about its competitive position, regional factors, industry trends and rules of thumb. The Honda example I brought to light earlier is a classic. U.S. manufacturers had failed in the past with attempts to sell a smaller motorbike and as such had written off the market segment, leaving a gaping opportunity for Honda to grab market share.

Competitors Strategy: this information is often hard to find. A lot can be revealed in annual shareholder reports, 10K reports, interviews with analysts, statements by managers and press releases. Cash flow is also a great indicator of a competitor’s strategy, try looking up their hiring activity, R&D projects, capital investments, promotional campaigns, strategy partnerships, mergers and acquisitions.

Competitors Capabilities: once you have knowledge of the above three aspects, you can make better informed decisions about the capabilities of a competitor. These decisions can help fend off competitor attacks and help you make strategic attacks based on competitor’s abilities to respond quickly and effectively. A company’s capabilities are also linked to its strengths. So completing a SWOT analysis (I go through this later) can provide further interesting intelligence.

 

How to find information for your competitor profile

Davidson (1997) believes the sources of competitor information can be neatly grouped into three categories:

    Recorded data: this is easily available in published form either internally or externally. Good examples include competitor annual reports and product brochures;
    Observable data: this has to be actively sought and often assembled from several sources. A good example is competitor pricing;
    Opportunistic data: to get hold of this kind of data requires a lot of planning and organisation. Much of it is “anecdotal”, coming from discussions with suppliers, customers and, perhaps, previous management of competitors.

 

The table below lists possible sources of competitor data using Davidson’s categorization:

 

Task 1: Competitor Profile
Complete your competitor profile – use the recommended sources to find the data and try get as much info as possible.

 
Competitor Profile: Competitive Advantage

This section is important. Do you know what your competitive advantage is?

I like to define competitive advantage as your point of differentiation – something that makes you different from your competitors. Jim Riley defines it as an advantage over competitors gained by offering consumers greater value, either by means of lower prices or by providing greater benefits and service that justifies higher price.

Jim is spot on, but I like to keep things simple.

 

Task 2: Competitive Advantage
Think about what makes your company different than your competitors. Next, do the same thing for your three competitors. If you’re struggling, pick up the phone and give your competitors a call – just ask them!

Go one step further and actually talk to their customers – ask them why they shopped with the competitor company. You’ll get some lovely responses and more than likely find out their competitive advantages.

 
Marketing Profile: Target Market

Did you know that some of your competitors may not actually be your competitors?

In this step I want you to define your primary target market.

Your primary target market is the group of customers that you service the most. For instance, you are an IT Company that services Accounting firms in New York. That’s a very specific target market, but for demonstration purposes – your competitors are the ones competing with you for a share in that market. You can broaden things out and target a number of different verticals in New York, but I recommend you try keep things as targeted as possible to ensure you find the most relevant competitors to compare against.

 

Task 3: Target Market

Identify who your target market is (if you don’t already know) and then do some research on your competitors to see who their target market is. A good place to start would be their website and marketing communications – see who their existing clients are, have a look at their messaging.
Note: if you find out your actual competitors are different from what you original selected – make sure you change them and redo section one: competitor profile.

 
Marketing Profile: Market Share

Market share can be defined as the percentage of the market you account for. In the previous task you identified your target market – now it’s time to calculate your market share.

In a survey by the Marketing Accountability Standards Board (MASB), 67% of respondents prefer to calculated their market share metric as a dollar value – for instance – you have $300,000 share in a $1.8 million dollar market which equates to a 16.7% market share.
So why is this relevant for your competitive analysis?

Well, you want to know how you stack up against your competitors to ensure you can make better business decisions – read more this in my article on competitive benchmarking. This metric can help you with setting goals and objectives for the future to ensure you grow your business and take a bigger piece of the revenue available.

Looking at these statistical trends overtime can also give you good intelligence on your competitor’s behavior  If their market share has increased steadily over the past couple of years you can safely bet they are looking to gro market share in your market, which means stealing market share from you! If you are aware of this, you can make necessary defensive strategies to combat their attacks.

 

Task 4: Market Share

Complete a market share analysis.

 
Marketing Profile: Marketing Strategies

Your closest competitor is ramping up a new radio campaign – wouldn’t it be nice to know that ahead of time?

Well, it certainly would – but unless you have some overfriendly competitors that love sharing, chances are you probably won’t. That doesn’t mean you can’t strategically predict what your competitors might do.

In this section of the template, make sure you fill in your marketing strategies and predict those of your competitors. If you are not familiar with what they are doing, do some research and observe what they are doing – good sources to be looking at include their advertising campaigns and promotions. The goal here is to try and predict what they might have planned for the future and how that will affect you.

Let me demonstrate this with an example. You might have a competitor in your list that primarily services another particular market but have a couple of clients in your market. They recognise an opportunity for growth and think they can steal a bit of the market share that you hold and as a result, launch a radio campaign targeted at your market!

Now what do you do?

Hopefully you’ve been doing your analysis and have a defensive marketing strategy in place. For example, you can be proactive and reach out to all your clients with a special ‘value adding’ quarterly meeting – giving you an opportunity to strengthen your relationship and retain your clients. By being prepared, you can stay one step ahead of your competitors.

 

Task 5: Marketing Strategies
Write down your current marketing strategies and any you have in the pipeline. Now get moving and see what your competitors are doing – give them a call, do some research on their site – do whatever you can to become more familiar with their strategies. You’d be surprised how many competitors would actually tell you what they are planning.

 
Product/Service Profile:

Your product and/or service mix includes your range of products and services. This section of the competitor analysis template involves comparing your offerings to those of your competitor.

It is important here to take a look at your product range, product quality and brand credibility.

I recommend you firstly take a look at your product range and see where your strengths lie. Next, be honest with yourself and rank your quality standard on scale of 1 to 10. Lastly, rank your brand credibility and if you hold any. Now do the same for your three competitors, using the same judging scale. Look out for areas where your product or service differs from your competitors.

This section gives you the opportunity to identify new viable markets that can be exploited with a new product, or make product variations to fill a gap in an existing market. These opportunities can be explored further with more research and by completing section four of this template which discusses the SWOT.

 

Task 6: Product/Service Profile
Complete the product/service profile – rate your company then your competitors on a consistent performance scale.

 
Product/Service Profile: Pricing and Costs

Pricing can tell a lot about your competitors. Here you want to be looking at what pricing strategies you and your competitors are implementing.

Key questions to be asking about yourself and your competitors include:

    Am I a low-cost or high-cost provider?
    What are my mark-ups? 75% but currently offering 15% to get stock moving?
    Do I work off volume sales or once off purchases?
    Do my prices differentiate depending on the medium – online vs brick n’ mortar?
    Am I using a cost-based, customer-based or competitor-based pricing strategy?

 

Task 7: Pricing and Costs
Answer all these questions about your company and your competitors. Pricing is an observable source so you should be able to get most of the answers by looking at your competitors offerings.

 
Product/Service Profile: Distribution Channels

The internet has disrupted the more traditional distribution channels. This section involves looking at how you distribute your product/service to your customers and how your competitors do it. Jim Riley talks about how each layer of marketing intermediaries that performs some work in bring the product to its final buyer is a ‘channel level’. I recommend you break down your distribution channels down as a percentage.

 

Task 8: Distribution Channels
First, start with your company – how do you get your products or services into your customers hands?
It might be that 50% of the work is done remotely, 30% on site and the remaining 20% done automatically?
Now look at your competitor’s distribution channels – are they automating more aspects? Do they use more middle men? Do they spend more time in front of the clients?

Two questions I love asking are:

    Does there strategy resonate better with customers than yours?
    Do they have better customer satisfaction metrics than you?

 
SWOT Profile: Strengths, Weaknesses, Opportunities, Threats

The final section of the competitor analysis template includes analyzing four aspects from the traditional SWOT analysis (Strengths, Weaknesses, Opportunities, Threats). A SWOT analysis is traditionally used by companies to audit their current business processes and monitor competitors. I like to break the SWOT down to internal and external factors.

Internally, lays your company’s strengths and weaknesses. These are things that you have control over – hence internal. Examples of strengths include your brand and your perceived quality. Weaknesses might be your lack product range.

Externally, lays your companies opportunities and threats. These are aspects which you have limited/no control over – but must be aware of and monitor closely. Examples of opportunities might be government strategy that opens up doors to new markets, while threats might be a new competitor deciding to enter your market.

Understanding your own SWOT is only half the challenge. The really value you get from a SWOT analysis is when you complete one for your competitors – SWOT’s are a great way to predict competitors strategies and their competitive advantages.

In fact, a SWOT analysis links closely to what I talked about in an earlier section regarding competitive advantage. To form a competitive advantage, you need your strengths and opportunities to be matching. By understanding this dynamic, you can put strategies in place that aim to convert your weaknesses and threats into strengths and opportunities. This will allow your company to find new competitive advantages that will help you grow into new markets to defend attacks from competitors.

Furthermore, by understanding your competitors weaknesses and threats, you are able to create strategies that can exploit them and help you towards your objectives.

 

Task 9: SWOT Profile
Complete a SWOT analysis for your company and your competitors – make sure you get your head around internal vs external factors – a lot of people tend to get confused. This is the final step of my competitor analysis template and really aims to wrap up everything and give you a good understanding of where you lie in comparison to your competitors in the market.

 
In conclusion…

Some businesses think it’s best to just go about their own plans and ignore the competitors.

I hope by going through this competitor analysis template you can now appreciate the importance of analysing your competitors. When completed correctly, a competitor analysis can help you your business form strategies that will capitalise on opportunities and minimise the impact of threats from competitors.

If you haven’t already done so, download my customer analysis template and use my resources to help you complete it.

Survey your customers and improve customer loyalty.