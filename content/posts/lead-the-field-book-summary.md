---
title: Lead the Field Book Summary
date: Sun, 23 Mar 2014 09:11:04 +0000
draft: false
lastmod: 2024-02-27T00:22:18.082Z
tags:
  - books
---

The book [Lead the Field](https://amzn.to/2PQZ1RE) is one of those old school self-help books who's advice is still applicable today. It focuses on developing yourself around serving others, developing a positive attitude, personal responsibility and goal setting. I wrote this outline so I could come back and visit the key points in the book. Have you read any good productivity books? I'd love you to take a moment to tell my your favorite in the comments.

## Chapter 1 - The Magic Word

The book is about 12 ideas.

- Our attitude tells the world what we expect in return.
- We live up to our own expectations and others give us what we expect.
- Start each morning establishing our attitude
- Everything we say or do has an effect (cause and effect)
- You are going to get out of your life what you put into it.
- Once you begin to change your attitude your surroundings will change.
  - great attitude / great results
  - poor attitude / poor results
- Most people never think about their attitude at all.
- "Human beings can alter their lives by altering their attitudes of mind"
- For the author, the attitudes of gratitude and expectant work well.
  - grateful for living on earth, yadda yadda
  - expect the best of myself
- If you have a poor attitude you will become a magnet for unpleasant experiences. These experiences then reinforce the poor attitude
- You get what you expect
- Your attitude is a reflection of who you are inside.
- Find someone doing and outstanding job and getting outstanding results and you'll find people with a good attitude.
- Luck is what happens when preparedness meets opportunity, and opportunity is there all the time.
- Successful people come in all sizes, shapes, ages, and colors, and they have widely varying degrees of intelligence and education. But they have one thing in common: They expect more good out of life than bad.
- Our environment, the world in which we live and work, is a mirror of our attitudes and expectations.
- Our attitude toward life doesn’t affect the world and the people in it nearly as much as it affects us
- Create reminders for yourself to have a great attitude.
- If you’ll begin to develop and maintain an attitude that says yes to life and the world, you’ll be astonished at the changes you’ll see.
- have the attitude of the person you wish to become.
- The great German philosopher and writer Goethe put it this way: “Before you can do something, you must be something.”
- Treat every person with whom you come in contact as the most important person on earth. Do that for three excellent reasons:
  - (1) As far as every person is concerned, he is the most important person on earth.
  - (2) That is the way human beings ought to treat each other.
  - (3) By treating everyone this way, we begin to form an important habit.
- Each morning, carry out into the world the kind of attitude you’d have if you were the most successful person on earth.
- Destructive emotions and attitude don't hurt anyone but yourself.
- Forgive everyone that can hurt you and then forgive yourself.
- It really is too short – much too short – to spend any of our valuable time mimicking the attitudes of others – unless their attitudes are good.
- Make each thought you hold constructive and positive.
- Radiate attitutde of well-being and confidence

## Chapter 2 - Acres of Diamonds

- Each of us is standing in the middle of an acres of diamonds
  - explore the work which we are engaged, explore ourselves.
  - don't run off to greener pastures
- Your mind is your richest resource
  - go deep with what you already know before going on to something else.
- Serve the customer; serve the customer better than anyone else is serving the customer.
- There is just as much opportunity in one biz as there is in another.
- Stop begin a copycat and think in new directions
- Develop intelligent objectivity - the faculty to stand back and look at your work objectively.
  - Do you know all about your industry or profession?
  - time for refreshing change?
  - can customer's get a better break?
- How will your work be performed 20 years from now?
- No growth without risk
  - you run risks when you get out of bed in the morning
  - risks are good
- Restate and reaffirm your goal. The thing you want most to do, the place in life you want most to reach.
- Take an hour a day to dissect your work. Take it a part and look at it. There are opportunities in there.

### Thoughts on the chapter

- List opportunities around you now
- How can you increase your knowledge of your current job, trade, industry or profession?
- How can you improve your job performance now?

## Chapter 3 - A Worthy Destination

This chapter focuses on goal setting. The author sets up some success stories and then hits you with the idea that these people, unluck others have set goals for themselves.

- They know what they want, they think about it everyday.
- "Burning desire"
- “The secret of happiness is freedom; and the secret of freedom, courage.”
- “Success is the progressive realization of a worthy goal,” or, in some cases, “…the pursuit of a worthy ideal.”
- Success isn't the achievement of a goal, it's the journey.
- We are happier moving toward our goals than we are when we've achieved them.
- You should feel positive emotions when thinking about your goals.
- Make sure you constantly reevaluate your goals and move forward
- Write down your goals
- Focus your thinking toward your goals. It will help you reach them.
- Work on one goal at a time.
- We become what we think about
  - You are sum total of all your thoughts. Think about the right things and in time your sum total will encompass those thoughts.
- Just thinking about your goal moves your toward it.
- Thirdly, before you can achieve the kind of life you want, you must think, act, talk, and conduct yourself as would the person you wish to become.

In summary: \* First, it’s our attitude at the beginning of a difficult task that, more than anything else, will bring about its successful outcome. \* Secondly, our attitudes toward others determine their attitudes toward us.

### Things to do

- Create a list of the things you want
- Prioritize them
- Make the top one your current goal, save the rest of the list for later.

## Chapter 4 - The Miracle of Your Mind

Everything you have has come to you because you use your mind. This chapter discusses how to use more of it.

- Successful people are not people without problems; they’re simply people who’ve learned to solve their problems.”
- Living successfully, getting the things we want from life, is a matter of solving the problems that stand between where we are now and the point we wish to reach!
- Don't worry about the wrong problems. Worry about the right problems
  - problems we can solve
  - problems we need to solve that are beyond our ability to solve
- Look at a problem as a challenge to be overcome.
- When faced with a problem why not look to your mind to solve it?
  - Do you know how to think?
- Take 1 hour per day and use it to think.
  - Write your primary goal
  - write down ways in which you can improve upon your work each day
  - Most ideas will suck
  - This isn't easy
- When you start your day thinking you will think all day long.

### Thoughts on this chapter

- List your worries and concerns
- Take the list and determine what is actually legitimate
- Figure out the strategy for solving legitimate problems
- Spend 1 hour / day exercising mind by writing down ideas for achieveing goals
  - Try to get 20 ideas / session
- Test the ideas that you think are good

## Chapter 5 - Destiny in Balance

- You need to understand the law of cause and effect
- Our rewards will be determined by the extend to which we serve
- Unhappy with your rewards? Look at your device.
- Constructive discontent is what is responsilbe for our continuing upward spiral of civilization
- What do people need or want that you can supply them?
- George Bernard Shaw once commented, “I have become rich and famous by thinking a couple of times a week. Most people never think at all.”
- Your life is yours to do what you do with it, make it grow or fallow.
- Figure out how to increase your service
  - read books
  - read about and emulate other successful people
  - think of original wasy to increase your service
- Ask yourself everyday - “How can I increase my service today, knowing that my rewards in life must be in exact proportion to my service?"
- Being together is understanding how things work. Working hard won’t do it. That isn’t enough. We have to work intelligently.
- Succeeding takes time. It takes dedication, 100 percent commitment, and creative thought.
- If you’re worried about your income or your future, you’re concentrating on the wrong end of the scale.
  - instead focus on increasing your service, income and future will take care of itself.

Thoughts on this chapter: \* Evaluate how you are serving others now \* Note one way in which you can increase your service today \* Assess whether you are working not only hard but intelligently.

## Chapter 6 - Seed for Achievement

- People Love and value someone with integrety
- thine own self be true
- Integrity means to try, as best we can, to know ourselves, to examine ourselves as Socrates advised, and to make a true assessment of ourselves – an inventory of our abilities, our talents, our goals.
- What is important is to find what one can do best and then to do it with all one’s might.
- But trying to live up to \[making the world better\] involves constantly looking forward to the consequences of one’s actions, choosing those that are likely to be fruitful, and inhibiting action from impulse.
- The richness of a life depends not on the amount of happiness it achieves, but on finding out who one is – i.e., about one’s unique combination of powers – and then discovering through experiment and reflection what course of life will fulfill those powers most completely.”
- We have our underutilized minds, our abilities, our talents, and time.
- We are the only ones from whom we can steal time, talent, ability, and the use of our minds.

Thoughts on this chapter:

1.  Reflect on your typical day. Can you spot an area where you are throwing an “unfailing boomerang” — where, for example, you are cutting corners you should not cut, or doing something you should not do? (Integrity, the “Seed for Achievement,” is a deeply personal subject. Although space is provided for notes under this question, you may not want to write down a response. You may want to simply think it out, and make a mental commitment to take any corrective action that may be called for.)
2.  Do you always look for the truth, wherever it leads? Do you check things out for yourself, weigh what others tell you, and make your own judgments?
3.  Write down a task that you consider difficult and/or unpleasant. Resolve to do it to the best of your ability, just for the joy of accomplishment.

## Chapter 7 - It's Easier to Win

- What motivates people to live like they do?
- The people in this world who are willing to go after the hard ones are the people who achieve greatness. They are motivated to give the last ounce of themselves in the achievement of their goals.
- Bill Cosby earned $12 million in 1986. Do you know why? It’s because of the people he serves. There’s an investment banker in Los Angeles who earns about $40 million a year. Do you know that, as far as is known, there is no limit on earnings?
- Discontent is the greatest motivator
- Successfult people follow independent paths
- breakaway from crowd
- start path of their own
- And it’s almost never too late, for with a purpose – a worthy goal – and the motivation to reach those upper layers on the pyramid, a person can travel farther in a few years than he might otherwise travel in a lifetime.

### Thoughts on this chapter:

1.  Our environmental conditioning teaches us to conform, to play it safe. It’s an easy trap to fall into — one that all of us fall into to some extent. In adulthood, do you ever find yourself playing the game Earl Nightingale calls “follow the follower”?
2.  Analyze the key people in your environment, those who have an important effect on you, in your family, in your community, at work, and socially. Who possesses qualities you want to emulate?
3.  How can you improve your “human environment”?

## Chapter 8 - How Much Are You Worth?

- Begin to see yourself for what you really are: an amazing, infinitely valuable creature.
- Now, while the operations of a corporation are multitudinous and complex, they can be reduced to four basic functions: finance, production, sales, and research.
- Treat yourself like a business
- research, production, and sales. They are the head, hands, and legs of a company.
- A company growing at the rate of 10 percent a year will double in size in about eight years. What attention are you giving to the production of your personal corporation? Can you grow and improve as a person at least 10 percent a year?
- An ongoing education is vital, if we are to stay vital.
- Tomorrow is a brand new day. Time is the great equalizer – you have no more, or no less, than anyone else on earth can have. Right now, begin to think of ways in which you can begin to increase your effectiveness, raise your production, knowing that by so doing, you’re automatically presetting your rewards.
- If we waste an hour of productive time every day, it adds up to about 250 hours a year that our corporation, our plant, is shut down. We can earn nothing with the doors closed.
- Learn to enjoy every minute of your life
- A human life is really nothing more than a collection of minutes, hours, and days. These are the building materials. And it’s left strictly up to us to determine the kind and size of structure we build.
- How have you been handling the four vital functions of your business: finance, research, production, sales?
- How much time and effort are you giving to finance?
- To research – to the study of your work, your career? Can this be improved?
- What about production? How can you vastly improve the way in which you conduct your work?
- And how can your sales be improved? Sales entails more than selling a product or service; it includes the way in which we sell ourselves to everyone with whom we come in contact. It includes the way we get along with our associates, our spouse, our kids, our neighbors.

### Thoughts on this chapter:

Analyze your worth as a one-person corporation in three of the four critical areas of operation: research, production, and sales. Then write down ways in which you can improve in each of these areas. 1. How are you doing in research? (For example, are you holding a regular career study hour?) 2. Plans for improvement: 3. How is your production? (Are you spending an hour a day brainstorming? Are you putting the best of these ideas to work to increase your productivity? Are you more productive, more effective, on the job this year than you were last year? Are you growing and improving every year?)

## Chapter 9 - Let's Talk About Money

- I think it’s important that we remind ourselves just exactly what money is, how much of it is enough, and how to earn the amount of money we need to live the way we want to live now, as well as in the important future years.
- What is money? Money is the harvest of our production. Money is what we receive for our production and service as persons, which we can then use to obtain the production and services of others.
- The amount of money we receive will always be in direct ratio to the demand for what we do, to our ability to do it, and to the difficulty of replacing us.
- Most people think they want more money than they really do, and they settle for a lot less than they could earn if only they went about it in the right way.
- Our rewards will always be in exact proportion to our service. If you don’t like your income, you must devise ways and means of increasing your service. Your service must come out of you – your mind, your abilities, and your energy.
- People who refuse to do more than they’re being paid for will seldom be paid for more than they’re doing. You may have heard someone say, “Why should I knock myself out for the money I’m getting?”
- People who refuse to do more than they’re being paid for will seldom be paid for more than they’re doing. You may have heard someone say, “Why should I knock myself out for the money I’m getting?”
- Two distinct steps to take:
  - decide exactly how much money we really want.
  - forget the money and to concentrate on improving what we now do, until we’ve grown to the size that will fit and naturally earn the income we seek.
- Goals
  - the trouble with people is not that they can’t achieve their goals
  - they never set them
- Ben Franklin gave us the secret to wealth. He said that the road to wealth lies in augmenting our means or diminishing our wants. Either will do.
- It’s not your present circumstances that count; it’s the circumstances you make up your mind to achieve that are important.
- With the income that you intend to earn written down on a card, spend a part of each day thinking of ways in which you can increase your service.
- realize that money cannot be sought directly. Money, like happiness, is an effect.
- money in its proper place. It’s a servant – nothing more. It’s a tool with which we can live better,
- A person may be worth more than he’s getting – for a while – but the two will match up. They have to. In fact, unless a person is worth more than he’s receiving, he cannot move ahead. He’s receiving all he’s worth.

### Thoughts on this chapter:

1.  How much money do you want? How much do you need to live in the way you want to live? There are three amounts of money that you should decide upon: the yearly income you want to earn now or in the near future; the amount of money you want to have in a savings and/or investment account; and the amount you want as retirement income.
    - Set your financial goals. i. Yearly income: ii. Financial reserves: iii. Retirement income:
2.  Who in your line of work is now earning that amount of money? (If you know, you’ll have a good idea of what you’ll have to do to earn it.)
3.  Continue to make plans for increasing your service to others, for making yourself more valuable. The money will follow!

## Chapter 10: One Thing You Can't Hide

- The degree of a person’s ignorance will determine his place in the world.
- A person should begin with the study of his language, and then study his general area of interest.
- Our use of our language is the one thing we can’t hide.
- Children with the best vocabularies get the best grades in school.
- Make it a point to acquire books that will help you improve your vocabulary.
- Build a personal library

### Thoughts on this chapter:

1.  Rate your knowledge of the English language. Is it “excellent,” “fair,” or “poor”?
2.  Set goals for improving your vocabulary and usage. (Include books to read, audio programs to listen to, courses to take, a number of new words to learn each week.) Make a list of them here.
3.  Set goals for increasing your knowledge of your area of interest, for upgrading your job skills. (Include books and periodicals to read, audio programs to listen to, courses and seminars to attend.) Write them down. At the end of the book, there is a list of audio programs that will enrich your life in many areas.

## Chapther 11 - Today's Greatest Adventure

- We’ve defined success as the progressive realization of a worthy goal.
- Seems like reaching goal is slow and takes forever
  - beat that feeling by living successfully one day at a time
- Successful life == many successful days back to back
- Keep your goals in mind as often as possible live life one day at a time
- Do everything you can each day
- not quantity of things but quality of things
- To get the habit of success (and that’s why successful people go from one success to another – because it’s a habit with them), you need only to succeed in the small tasks of each day.
- To advance to the place you’ve chosen, two things are necessary: that you keep your eye on your goal and that you continue to grow in competence and effectiveness.
- If you do most of your tasks successfully than your life will be successful. It has to happen.
- Remind yourself at this time that people become exactly what they make up their minds to become.
- The 25k plan
  - write down six things you need to do
  - order them by importance
  - work on each until they are finished
  - don't waste your time being successful at unimportant things
  - work on the important stuff that moves you towards goal
  - don't get sidetracked by people or things
- “Work as though you would live forever; but live as though you would die tomorrow.”

### Thoughts on this chapter:

1.  Try the $25,000 idea in your life: Write the six most important things you have to do each day, then number them in their order of importance. Print out this sheet. Work on item number one until it is successfully completed. Then move on to number two, and so on. When you’ve finished with all six, repeat the process.
2.  Make certain that the tasks you spend your time on are important ones — tasks that will move you ahead, steadily, toward your goal. List any tasks that you might be able to delegate to others or even eliminate from your agenda — tasks that, over time, have become unnecessary or obsolete.
3.  How well do you deal with interruptions and distractions? If you are often sidetracked during your day – by non-urgent telephone calls, drop-in visitors, etc. — plan a strategy for handling those diversions.

## Chapter 12 - The person on the white horse

- The best way for you to develop the security that lasts a lifetime is to become outstanding at one particular line of work.
- When he was asked the formula for success, the great steel magnate, Andrew Carnegie, answered, “Put all your eggs in one basket, and then watch the basket.”
- Keep you eye on the goal
- be happy about everything - you'll achieve your goal, why not be happy
- become a sponge of information that will help you on the way
  - learn from other people's mistakes
- don't forget success comes at the service of others

### Thoughts on this chapter:

1.  Evaluate your leadership skills, and note any ideas for improvement below:
2.  How can you develop what Earl Nightingale calls “the security that lasts a lifetime”?
3.  Resolve to, each day, do more than you are paid to do.

## Lead the Field Book Frequently Asked questions

Sure, here's the content formatted as requested:

### What is the main theme of "Lead the Field"?

The book emphasizes personal development, positive attitude, serving others, and goal setting as keys to success.

### Can you summarize the key points of each chapter in "Lead the Field"?

Each chapter covers different aspects of personal development, from the importance of attitude and goal setting to the significance of integrity and understanding the value of money.

### How does "Lead the Field" suggest we change our attitude?

It recommends starting each day with a positive attitude, being grateful, expecting the best from oneself, and treating others as important.

### What does "Acres of Diamonds" mean in the context of the book?

It suggests that opportunities for success are already present in one’s current situation, emphasizing the value of exploring and maximizing one's current resources and position.

### How does the book define success?

Success is described as the progressive realization of a worthy goal, highlighting the journey towards achieving goals as more fulfilling than the actual achievement.

### What is the significance of goal setting according to the book?

Goal setting is crucial for giving direction and purpose to one’s efforts, and it’s important to write down and prioritize goals.

### How can one increase their service according to "Lead the Field"?

By continuously seeking ways to serve others better and focusing on increasing one’s value through service.

### What role does integrity play in achievement according to the book?

Integrity is foundational for true success, involving self-examination, honesty, and striving to make the world better through one’s actions.

### How does the book suggest handling financial goals?

By determining how much money one truly needs, focusing on increasing service to others, and enhancing one’s skills and value.

### What is the $25,000 idea mentioned in the book?

It’s a productivity method involving listing six most important tasks each day, prioritizing them, and working on them in order of importance.

### How does "Lead the Field" suggest improving one's vocabulary and knowledge?

Through continuous learning, building a personal library, and setting goals for acquiring new knowledge and skills.

### What is today's greatest adventure according to the book?

Living successfully one day at a time, focusing on quality of actions rather than quantity, and making daily progress towards one’s goals.

## Your Turn

I hope you enjoyed my book summary and outline of Lead the Field. If you want to be notified when I've created a new book summary, [join our email list](https://mailsrv.getclipdish.com/subscription?f=EeZoV6892LXhcY892763exC763cYdoDgoAf0bUVbz1mDs4XmCTDHGoP7l8EIVsHzShlA8FUG).

## More Book Summaries

You can find more of my book outlines & summaries by [visiting this page.](/tags/books/)