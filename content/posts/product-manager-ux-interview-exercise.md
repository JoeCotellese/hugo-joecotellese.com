---
title: "Product Manager & UX Interview Exercise"
date: 2022-01-20T07:36:08-05:00
draft: false
tags: []
contentTypes: ["BlogPosting"]
---

When I interview Product Managers or UX designers I usually block off time for 
an interactive exercise to see how a candidate tackles product problems. 

This involves designing a feature for app or website. 

Not to design the "right" solution but to observe thinking.

I don't want "free work" from the candidate so it's never about my company's 
products. Rather, I'll take an application that I've used recently that I find
particularly irksome.

My new targets are the CVS COVID scheduler their website or the Roku 
Peacock app.

It usually breaks down like this. 

I'll show the candidate the work, give them five to 10 minutes to play with it
then, we go about updating it. 

I'll act as proxy for the cutstomer in the event they want to mock interview me.

What I'm holding a candidate accountable for is different depending on the role. 

Broadly speaking though, I'm paying attention to the following things.

1. How does the candidate approach the problem?
2. What is the candidates process for making improvements?
3. Are they asking questions about the "job" the app is supposed to perform?
4. Are they thinking about the personas using the app?
5. How do they approach measuring success?

The best candidates love this process. 

Am I unique here? I'd love to know if you follow a process like this or have 
gone through a process like this.

