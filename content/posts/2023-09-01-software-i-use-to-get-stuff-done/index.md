---
title: Software I use to Get Stuff Done
description: Here is a list of software I run on my devices to help me manage my life.
date: 2023-09-01T14:02:34.897Z
preview: ""
draft: false
tags: []
categories: []
lastmod: 2023-09-03T12:36:44.238Z
---

I love finding great software to help me get stuff done. Here is a collection of applications that I install whenever I'm setting up a new Mac.

These are all really exceptional applications that I can't live without.

- [Alfred](https://alfredapp.com) - control your Mac, launch applications and workflows without leaving the keyboard.
- [AnyBox](https://anybox.app) - manage your bookmarks outside of your browser. Useful if you run multiple browsers.
- [AudioWrangler](https://audiowrangler.app) - the missing audio manager, let's you prioritize the order of your Input & Output audio devices in macOS
- [Choosy](https://choosy.app) - a browser chooser which lets you control which browser opens when you click a link. Good if you like to run multiple browsers or Chrome profiles.
- [Darkroom](https://darkroom.co) - macOS photo editor focused on ease of use. Great UI.
- [Drafts](https://getdrafts.com)
- [Fantastical](https://flexibits.com/fantastical) - excellent Calendar replacement
- [Hammerspoon](https://www.hammerspoon.org) - macOS window manager
- [Homebrew](https://brew.sh) - easily install and maintain terminal packages
- [iTerm](https://iterm2.com) - macOS terminal replacement. If you're in the terminal a lot, you'll want to install this
- [Obsidian](https://obsidian.md)
- [OmniFocus](https://www.omnigroup.com/omnifocus)
- [Parsec](https://parsec.app) - great application for remotly accessing your PC & Mac
- [PopClip](https://www.popclip.app) - define instant actions to run on selected text
- [Typinator](https://ergonis.com/typinator) - text expansion saves you typing
- [Visual Studio Code](https://code.visualstudio.com)
- [Vivaldi](https://vivaldi.com) - a Chrome based browser with better privacy controls