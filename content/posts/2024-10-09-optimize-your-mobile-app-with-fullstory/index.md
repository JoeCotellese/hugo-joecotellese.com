---
title: Qualitiative Analytics Case Study - AWeber
description: Learn how I used quantiative and qualitative analytics tools to improve mobile application adoption and engagement.
date: 2020-10-09T15:26:04.995Z
preview: ""
draft: false
tags: []
categories: []
lastmod: 2024-10-09T15:33:59.976Z
---

---

*Note - this is a rewrite of an interview that I conducted with the team at FullStory. The original page no longer exists but can be found in the [Internet Archive](https://web.archive.org/web/20200404233602/https://blog.fullstory.com/aweber-uses-session-replay-and-click-maps-to-improve-online-experience/)*

---

As the Director of Product at AWeber, an email marketing platform serving over 100,000 small businesses, email marketers, and entrepreneurs, I’ve always been keen on understanding our customers’ interactions with our product without intruding on their experience. In 2014, I discovered [FullStory](https://fullstory.com), and I was immediately blown away by its session replay technology. It offered us a unique lens into our users’ experiences—one that usability studies, with their artificial settings, simply couldn’t provide.

Before FullStory, our approach to analytics was grounded in high-level metrics, thanks to a comprehensive suite of tools. However, I quickly realized the limitations of relying solely on quantitative data. FullStory changed that by allowing us to delve into the finer details of user behavior.

Our workflow involves selecting a cohort of users and tracking their actions in AWeber with KISSmetrics. This macro-level insight is complemented by FullStory’s micro-level session replays, enabling us to piece together a holistic view of user interactions.

One pivotal discovery came from observing users on mobile devices. We noticed an unexpected trend: many were accessing AWeber via a browser rather than our app. By segmenting these users and employing FullStory to watch their behavior, we identified the missing functions in our mobile app that were present on the website. This insight guided us to enhance our app, ultimately boosting its usage.

Page Insights, a feature of FullStory, has been instrumental in our decision-making process. Unlike traditional heatmaps, it aggregates user sessions to reveal patterns at a glance. This was particularly useful when we identified a broken link on our user control panel. Observing repeated clicks on the non-functional link was a revelation that analytics alone wouldn’t have uncovered.

FullStory has been a game-changer for us, providing deep user insights without the need for an extensive research team. It simplifies what would otherwise be a cumbersome process of scheduling and conducting user interviews. This tool has bridged the empathy gap, helping us make informed product decisions grounded in real user behavior.

In product development, understanding your users is crucial. Without research, decisions are based on assumptions, which can lead to costly mistakes. FullStory has been a vital partner in our journey to enhance the AWeber customer experience over the past three years, ensuring our product evolves in line with our users’ needs.

To read another case study on qualitative analytics check out [3 Things I Learned About Usability Testing By Looking at Customers in Their Underwear](/posts/3-things-learned-usability-testing-looking-customers-underwear/)