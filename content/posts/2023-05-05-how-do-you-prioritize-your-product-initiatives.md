---
title: "Mastering the Art of Prioritization: Tools for Product Managers"
description: Learn how to prioritize product requirements with tips and tools you can use to balance user needs, feedback, and technical feasibility for success.
date: 2023-05-05T21:07:36.564Z
preview: ""
draft: false
slug: product-management-prioritization-guide
tags: []
categories: []
lastmod: 2023-05-06T12:11:22.353Z
---
---
**Key Ideas**

* Product management frameworks are useful tools for organizing and optimizing product development processes.
* However, blindly adhering to a framework without considering its context can lead to negative outcomes.
* Prioritizing the right things, such as understanding customer needs and feedback, is crucial for successful product development, regardless of the framework used.

---

As a former boy scout, I know the importance of packing light for a camping trip. On my first overnight hiking trip in sixth grade, I was determined to be prepared for anything. I packed everything I could think of, including a gallon jug of water. It wasn't until a few hours into the hike that I realized my mistake – that gallon of water was heavy, and it was slowing me down. The same is true for your product backlog – if you try to pack in everything, you'll end up weighed down by the sheer volume of features and ideas. By carefully prioritizing your backlog, you can make sure you're focusing on the most important and impactful features, and leaving behind the ones that aren't as crucial. Just like on a hiking trip, a lighter load can make all the difference in the success of your product.

![picking up oversized back pack and falling over](/img/giphy.webp)

Just like packing for a camping trip, prioritizing your product backlog can be challenging. You have limited space, and you need to decide what to bring with you. In the case of your product backlog, you have limited resources and time, and you need to decide which features to prioritize. The challenge comes in trying to balance competing priorities and conflicting opinions from stakeholders. Some features may seem critical to one person, but less important to another. Additionally, you may receive feedback from customers or users that contradicts what you thought was the right priority. In the end, it's important to remember that prioritizing is an ongoing process, and you need to be open to adjusting your priorities as new information becomes available. It's not always easy, but by focusing on the most important features and ideas, you can ensure that your product is delivering the most value to your users.

This is where prioritization frameworks can come to the rescue. A well-defined prioritization framework offers a common language and transparent process for evaluating and prioritizing product ideas and features. It provides a structured way to objectively compare and rank features based on their potential impact, reach, effort, and confidence. By using a prioritization framework, you can ensure that all stakeholders are on the same page and have a clear understanding of how you go about prioritizing things. It also helps to avoid subjective arguments or discussions, as you can point to the specific criteria used to make decisions. Additionally, a prioritization framework ensures that you're making informed decisions based on data and feedback, rather than just going with your gut. 

In this post, we explore some common prioritization frameworks that product managers can use to make informed decisions and justify their choices to management or clients.

The first step in prioritizing requirements is to understand the goals and objectives of the product. This involves defining the product vision, mission, and strategy, and aligning the requirements with these elements. Once you have a clear understanding of the product goals, you can use a variety of prioritization frameworks to evaluate and rank requirements.

## Prioritization matrix

This is usually the first type of prioritization framework that I start with when working with product teams.

A prioritization matrix to used to evaluate requirements based on two or more factors, such as importance and difficulty or value and feasibility. This matrix involves plotting each requirement on a two-dimensional grid, with each axis representing a different factor.

You can also just make one factor the numerator and the other the denominoator and, using those two values come up with a score. 

This framework is useful for prioritizing requirements based on multiple criteria and for identifying requirements that may be both high value and low effort.

[You can start with something simple like plotting value and effort](/posts/value-effort-ratio-prioritization). Then add in additional factors. RICE is a great example of a prioritization matrix.

### Reach, Impact, Confidence, and Effort (RICE)

The [RICE framework](/posts/rice-framework-product-prioritization-how-to/) is a popular prioritization method used by product managers to assess feature ideas and determine which ones are most important to pursue. RICE stands for Reach, Impact, Confidence, and Effort. Reach measures the number of people who will be affected by the feature, while Impact measures how much the feature will improve key metrics, such as revenue or customer satisfaction. Confidence measures how sure the team is about the estimates for reach and impact, and Effort measures the amount of time and resources required to build the feature. By scoring each feature on these four dimensions and calculating the overall RICE score, product managers can prioritize features and make data-driven decisions about what to build next.
## Must have, Should have, Could have, Won't have (MoSCoW)
The MoSCoW method, which stands for Must have, Should have, Could have, and Won't have is another popular framework to keep in your product toolbox. 

This method involves categorizing requirements into four groups based on their importance and urgency. Must have requirements are critical and essential to the success of the product, while Should have requirements are important but not critical. Could have requirements are desirable but not essential, and Won't have requirements are those that can be deferred or discarded. This framework is useful for prioritizing requirements based on their importance and urgency, and for communicating these priorities to stakeholders.

## Kano model

Another commonly used framework is the Kano model, which helps identify the features that will delight users and create competitive differentiation. The Kano model categorizes features into three groups: basic, performance, and excitement. Basic features are those that users expect as a minimum requirement, such as reliable performance or easy usability. Performance features are those that improve the product's performance or functionality, such as faster processing or increased storage. Excitement features are those that surprise and delight users, such as innovative design or unique features. This framework is useful for prioritizing features based on their potential to create user satisfaction and loyalty, and for identifying areas where the product can differentiate itself from competitors.

Read this [Kano model case study](/posts/kano-model-clipdish-case-study) for the mobile recipe app ClipDish.

## Cost of delay (CoD)

Finally, you can use the cost of delay (CoD) framework, which helps quantify the cost of delaying a requirement. The CoD framework involves evaluating the potential revenue, savings, or other benefits that would result from implementing a requirement, and then subtracting the cost of delay, such as the opportunity cost of not implementing the requirement or the potential revenue lost by delaying it. This framework is useful for prioritizing requirements based on their potential to generate revenue or savings, and for identifying requirements that may have a higher cost of delay.

## How do I pick a prioritization framework?

It's worth noting that these frameworks are not mutually exclusive and can be used in combination or modified to suit specific product management needs. As a product manager, it's important to evaluate and choose the most appropriate framework for your specific situation and to continually refine and adjust the framework based on feedback and results.

Even with a well-prioritized backpack, you might still get blisters if your shoes shoes don't fit well. Similarly, in product management, prioritizing features and tasks can help ensure that you're building the most valuable and impactful product, but it won't necessarily address all the challenges you might face.

It's important to be flexible and adaptable (and pack extra socks), and to keep your end goal in mind while navigating the challenges along the way.

If your prioritzation process feels weighted down like my old backpack [reach out](/services) and let me help ligten the load.
## References 
These frameworks are widely used and have been discussed in various books, articles, and online resources on product management. If you want to take a deeper dive on each, here's some recommendations for good jumping off points.

Prioritization matrix: The prioritization matrix is a general framework that can be customized to suit different product management needs. You can find more information on prioritization matrices in articles and online resources such as the "[Product Plan](https://www.productplan.com/learn/prioritization-matrix-example/)" blog.

MoSCoW method: The MoSCoW method was first introduced by Dai Clegg in his book "[Case Method Fast-Track: A RAD Approach](https://amzn.to/3pjyxhZ)" and has since been adopted by many organizations and product management frameworks. 

Kano model: The Kano model was developed by Japanese researcher Noriaki Kano in the 1980s and has since become a popular framework for product managers to evaluate customer needs and requirements. 

Cost of delay: The concept of cost of delay was first introduced by Donald Reinertsen in his book "[The Principles of Product Development Flow](/posts/principles-of-product-development-flow-book-summary)" and has since become a popular framework for product managers to evaluate the potential impact of delaying a requirement. You can find more information on the cost of delay in the excellent book "[Escaping the Build Trap](https://amzn.to/3NNvtVE)" by Melissa Perri.

