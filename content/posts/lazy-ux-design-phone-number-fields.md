---
title: Lazy UX Design - Phone Number Fields
date: Sun, 15 May 2016 17:08:31 +0000
draft: false
lastmod: 2023-03-09T18:17:45.228Z
---

Grrr, I hate poorly designed forms. I'm especially annoyed by forms that want humans to think like computers. Case in point, take a look at this phone number field from Bed Bath and Beyond. ![Bad UX Patterns - Phone number field](/img/2016-05-15-Bad-UX-Patterns-Phone-number-field.png)

Phone numbers in the US can take many forms - parens around the area and dash separated are the most common. What isn't common is a form typed out without any separators. However, lazy design forces that on people.

One side effect of this design is that the auto form completion built into browsers and password managers break when used here.

When you are thinking about your input fields, think about how your customers think about the information they are providing and provide support for the most common patterns.
