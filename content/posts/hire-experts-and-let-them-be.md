---
title: Stop doing, start delegating
description: Reflect on the value of trusting experts with a personal story of learning to let go and empower professionals in their field of expertise.
date: 2023-03-08T15:44:20
preview: ""
draft: false
tags: ""
categories: ""
lastmod: 2024-02-04T15:06:16.743Z
---

"Yeah, everyone thinks they're a designer."

Years ago, my company hired a branding agency. During one of our sessions I made a suggestion about a creative direction the firm could take.

I was quickly shut down by the owner of the firm with those words that have stuck with me all of those years later.

My initial reaction was "We're paying them, why shouldn't I be able to offer my input?"

However, reflecting on it later I realized "We're paying them. They're the experts."

I've thought a lot about that over the years and it's helped shape my thinking as a manager. It is easy to fall into the trap of telling your team exactly what to do. Probably because you became a manager because you're good at "doing the thing."

It takes a lot more work to let go of things and let the experts be experts. Your job is defined less by doing the thing and more by hiring the right team, giving them outcome based goals and working to eliminate roadblocks. Start thinking about when to [coach and when to direct your team](/posts/product-management-leadership-coaching-directing-teams/).

Her words were a little harsh but her message was clear -- When you hire people for their experience and expertise, trust that they can do the work.
