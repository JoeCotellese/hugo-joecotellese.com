---
title: "Reframing the Approach: Prioritizing Problems over Solutions in Product Management"
date: 2018-11-27T13:22:05+0000
draft: false
lastmod: 2024-01-09T14:06:01.742Z
featuredImage: /img/Product-Tunnel-vision-featured-fs8.png
description: Explore how product managers can enhance their strategies by focusing on understanding and solving problems rather than jumping straight to solutions.
slug: problem-focused-product-management-strategy
---

## Focus on the problem, not the solution

I just spent about a half an hour looking online for replacement photo sleeves for a baby album that sits on a shelf in my office. I needed to do this because the sleeve was torn.

![](/img/product-tunnelvision-fs8.png)

After coming up empty and thinking I'd have to toss the whole thing in the trash I realized something pretty obvious...

There were about two dozen unused sleeves in the back of the album.

_How often do you get fixated on the solution to a problem that you stop actually thinking about the problem you're trying to solve?_

As product managers, our natural inclination is often to jump straight into solution mode. We're fixers by nature, eager to provide value and resolve issues. However, this solution-first mindset can sometimes lead us astray, causing us to overlook simpler or more effective paths. Reframing our thinking to be problem-oriented rather than solution-oriented can lead to more innovative and efficient outcomes. Here are three ways a product manager can make this shift:

### 1. Embrace Deep Understanding Before Action

- **Ask 'Why' Repeatedly**: Instead of immediately searching for solutions, delve deeper into understanding the 'why' behind the problem. The Five Whys technique, a method encouraged in Agile methodologies, is a great tool for this. It involves asking "why" successively until you reach the root cause of a problem. This process helps in comprehending the problem at a fundamental level, thereby ensuring that the solution addresses the core issue, not just the symptoms.

- **Customer-Centric Approach**: According to the Agile Handbook, the highest priority is to satisfy the customer through the early and continuous delivery of valuable software. Applying this principle, focus on understanding the customer’s needs and pain points in detail. Engage in customer interviews, surveys, and observe their interactions with your product. This insight can redefine your perception of what the problem actually is.

### 2. Cultivate a Mindset of Flexibility and Adaptability

- **Welcome Changing Requirements**: As Agile methodology suggests, being open to changing requirements, even late in development, harnesses change for the customer’s competitive advantage. This approach allows product managers to shift from a rigid solution-based mindset to a flexible problem-solving attitude. Embrace the idea that your initial solution might not be the final one and be open to iterating based on new information and feedback.

- **Encourage Cross-Functional Collaboration**: Agile promotes collaboration between business people and developers throughout the project. Use this collaborative environment to gather diverse perspectives on the problem. This can lead to uncovering hidden aspects of the problem and consequently, more innovative solutions.

### 3. Focus on Value and Outcome Over Output

- **Prioritize Value Over Features**: Instead of measuring success by the number of features released, [focus on the value these features bring](/posts/2023-06-07-stop-fighting-your-product-roadmaps/) to the end-user. Inspired by Lean Startup principles, validate your problem understanding and solutions through continuous testing and customer feedback. Measure success through outcomes like user satisfaction and problem resolution rather than just output.

- **Avoid Over-Engineering**: Drawing from the Agile principle of simplicity – maximizing the amount of work not done – avoid the temptation to over-engineer solutions. Sometimes, the most straightforward solution is the most effective. Keeping solutions simple and focused on the problem can lead to more sustainable and user-friendly products.

By shifting focus from solution-first to problem-first, product managers can foster a more innovative, customer-centric, and efficient approach to product development. This mindset prioritizes understanding and flexibility, allowing for solutions that truly resonate with users' needs and contribute to the product's success.
