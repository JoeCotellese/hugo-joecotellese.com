---
title: When to Use Which User-Experience Research Methods
date: Fri, 18 Mar 2016 17:29:14 +0000
draft: false
contenttypes:
  - BlogPosting
lastmod: 2023-12-22T18:57:21.200Z
---

There are many different ways to research your users. So many in fact that it's hard to know when to use a particular method. In this post, the author lays out framework you can follow to help you determine which UX research method to follow. In [When to Use Which User-Experience Research Methods](https://www.nngroup.com/articles/which-ux-research-methods/) the author describes a framework for choosing which research method to use based on where you are in the lifecycle of a project.

While it's not realistic to use the full set of methods on a given project, nearly all projects would benefit from multiple research methods and from combining insights. Unfortunately many design teams only use one or two methods that they are familiar with.  The key question is what to do when. To better understand when to use which method, it is helpful to view them along a 3-dimensional framework with the following axes:

{{<imgresize "images/user-research-methods.png">}}
