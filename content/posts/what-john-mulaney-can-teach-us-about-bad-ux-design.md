---
title: What John Mulaney can teach us about bad UX design
description: Learn how a John Mulaney show experience reveals key lessons in UX design, emphasizing the importance of clear communication and user expectations on websites.
date: Mon, 04 Oct 2021 13:14:08 +0000
draft: false
contenttypes:
  - BlogPosting
lastmod: 2024-03-10T17:49:10.131Z
---

Last night we went to see John Mulaney at the Academy of Music in Philadelphia. We arrived at 6:45 to find two lines wrapped around the block. I expected a line, COVID screening takes time. 

What I didn’t expect to find was a second line inside the theater where people were taking our phones and watches and putting them into security bags. I was really looking forward to this evening. It was the first time I’ve seen a popular comedian in a venue like this. Having someone tell me I needed to lock up my phone and watch(!) when I walked in the door did not prime me for a night of hilarity that I thought I was paying for. In fact, it was so far out of what I would consider normal behavior that my wife and I didn’t even think to text the kids to let them know we would be unavailable by phone. So, rather than settle into our seats, excited to see a really funny show, we sat down pissed off that John Mulaney felt the need to treat us like children. “He’s not that funny”, I thought before the show opened. Most of the people around me were cranky about it too. Now, some of you might say “It was on the website.” Sure, it was. Did I see it? No. I bought the tickets on my phone. Take a look at the screen shot and tell me if you can spot the information. This was so far out of what is normal behavior I wouldn’t even consider to look for a cell phone policy before the transaction. 

_If you have critical information that you need to put in front of your users, make sure it’s given prominence on your page. Better still, put it into a confirmation box that someone needs to accept before continuing._ 

I can only think of two reasons that this policy is at the bottom. It's there to intentionally mislead the customer or it's a bad decision by the web designer. 

Either way, a year from now, when the jokes are long forgotten I’m going to remember how annoyed he made me.
{{< imgresize "images/John-Mulaney-Tickets-Website.jpg" >}}
