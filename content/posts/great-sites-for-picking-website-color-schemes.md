---
title: Great sites for picking website color schemes
date: Fri, 26 Oct 2018 11:55:49 +0000
draft: false
contenttypes:
  - BlogPosting
lastmod: 2023-03-09T18:05:15.892Z
---

I have a hard time with colors. I don't know if I'm actually color blind but I will admit that from time to time I've left the house with two different color socks. I know my inability to ascertain color can hamper my ability to create visually pleasing web sites but I don't want to stick to black and white. However, finding color designs to steal (ahem, borrow) is extremely tedious. If you're color scheme challenged like I am then check out some of these websites to help you create eye pleasing color palettes.

## COLOURLovers

[COLOURlovers](https://www.colourlovers.com/) is a community driven website which provides color palettes which you can use for your website. The content is nicely laid out, and if you have a palette theme in mind (tans, blues, etc) you can drill down into dozens of different pallettes.

## Material Design Palette

If you are building a site and you want it to follow Google's Material Design guidelines check out [Material Design Palette](https://www.materialpalette.com/). This site will generate really great looking palettes for you. All you have to do is pick one or two colors and let it do its thing.

## Your Turn

Picking a great looking color palette for your website shouldn't be hard even if you are color-impaired. Check out those sites and be inspired. Have you come across another color scheme site that you like? Tell me in the comments.
