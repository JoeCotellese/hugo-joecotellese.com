# JoeCotellese.com

This is the public site for [JoeCotellese.com](https://www.joecotellese.com)

## Dependencies

The site is built on the static site generator Hugo. To build it you need to have Hugo installed.

`brew install hugo`

The site is also built with [Bootstrap](https://getbootstrap.com/)

## Building

When hugo builds the website it will mount the bootstrap scss files into the assets/scss folder in a sub-folder called Bootstrap.

In that folder there is also a custom scss file that is used to override the default Bootstrap to customize my site.

That custom file references the base bootstrap files under the mounted bootstrap folder.

After the build process, I'm left with the bootstrap combined CSS file in the output folder.

## Deployment

The site is deployed to Netlify using a CI/CD job running on Gitlab.

## Adding content

There are two priamry content types, projects and articles.

Projects are meant to be case studies of things that I've worked in.

Articles are blog posts.

Content is adding to the following folders:

- /content/articles - blog posts
- /content/projects - projects and case studies
- /content - top level pages

## Frontmatter

*Note: I'll get all of this documented someday.*

I am separating out title and blogTitle as two different front matter params.

- title: represents an `<title>` tag
- blogTitle: An optional param that represents the post title placed inside an `<h1>` tag.

If blogTitle is omitted, title is used for both.

## Adding images

Some images are stored in /static/img. They are referenced in markdown content from the path /img/IMAGENAME.png

I'm gradually moving over to use [leaf bundles](https://gohugo.io/content-management/page-bundles/#leaf-bundles). New content with images should follow that format.

## Various helpers

In the shortcodes folder you'll find various helpers.

`{{%airtable%}}` will embed a linked Airtable into a document
`{{<quote>}}TEXT{{< /quote >}}` will create a stylized quote in a document
`{{<imgresize "images/AlwaysBeTesting.jpg" >}}` to create images that work across media breakpoints

## Other folders
